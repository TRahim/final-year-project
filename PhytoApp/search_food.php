<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 15/03/13
 * Time: 14:16
 * To change this template use File | Settings | File Templates.
 */

include "phenol_connect.php";

if (isset($_POST['kw']) && $_POST['kw'] != "") {
    try {
        $input = $_POST['kw'];
        $search_term = '%' . $input . '%';
        $sql = "SELECT FOOD_NAME, FOOD_ID FROM FOODS WHERE FOOD_NAME like ?";
        $statement = $phenol_db->prepare($sql);
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $statement->execute(array($search_term));

        $result_set = $statement->fetchAll();
        $totalrows = count($result_set);

        if ($totalrows > 0) {
            echo json_encode($result_set);
        } else {
            echo("no_result_found");
        }
    } catch (PDOException $e) {
        echo($e->getMessage());
    }

}