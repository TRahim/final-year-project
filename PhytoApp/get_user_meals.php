<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 12/04/13
 * Time: 16:07
 * Description:
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];

try{
    $sql = "SELECT meal_id, meal_name, polyphenol_count FROM user_meals WHERE user_id = ?";
    $statement = $db_handle->prepare($sql);
    $statement->setFetchMode(PDO::FETCH_ASSOC);
    $statement->execute(array($user_id));

    $result_set = $statement->fetchAll();
    $totalrows = count($result_set);

    if($totalrows > 0){
        echo json_encode($result_set);
    } else {
        echo('no data returned');
    }
} catch(PDOException $e){
    echo($e->getMessage());
}
