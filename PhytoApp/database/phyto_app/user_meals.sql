USE phyto_app;

CREATE TABLE `user_meals` (
  `meal_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `meal_name` varchar(255) NOT NULL,
  `isoflavones_count` double NOT NULL DEFAULT '0',
  `lignans_count` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`meal_id`),
  KEY `fk_user_id` (`user_id`),
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
