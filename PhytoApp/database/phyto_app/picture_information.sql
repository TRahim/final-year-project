CREATE TABLE `picture_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phenol_ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `weight` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;


INSERT INTO `picture_information` (`id`, `phenol_ID`, `name`, `weight`) VALUES
(1, 686, 'Oranges', 154),
(4, 707, 'Apples', 150),
(5, 885, 'Banana', 126),
(6, 780, 'Blueberry', 100),
(7, 681, 'orange_juice', 250),
(8, 881, 'kiwi', 50),
(9, 698, 'lemon', 75);
