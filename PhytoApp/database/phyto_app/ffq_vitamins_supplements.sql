USE phyto_app;

CREATE TABLE `ffq_vitamins_supplements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ffq_entry_id` int(11) NOT NULL,
  `supplement_name` varchar(255) NOT NULL,
  `supplement_brand` varchar(255) NOT NULL,
  `supplement_strength` varchar(255) NOT NULL,
  `supplement_dose` int(11) NOT NULL,
  `supplement_frequency` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
