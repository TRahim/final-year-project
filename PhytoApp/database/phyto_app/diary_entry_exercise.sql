USE phyto_app;

CREATE TABLE `diary_entry_exercise` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `duration` double NOT NULL,
  `intensity` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB CHARSET=utf8;
