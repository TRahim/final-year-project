USE phyto_app;

CREATE TABLE `ffq_frequency_lookup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_number_value` int(11) NOT NULL,
  `option_string_value` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (1,0,'Never/less than once a month');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (2,1,'1-3 Per Month');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (3,2,'Once A Week');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (4,3,'2-4 Per Week');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (5,4,'5-6 Per Week');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (6,5,'Once A Day');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (7,6,'2-3 Per Day');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (8,7,'4-5 Per Day');
INSERT INTO `phyto_app`.`ffq_frequency_lookup`(`id`,`option_number_value`,`option_string_value`) VALUES (9,8,'6+ Per Day');
