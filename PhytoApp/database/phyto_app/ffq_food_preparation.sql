USE phyto_app;

CREATE TABLE `ffq_food_preparation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ffq_entry_id` int(11) NOT NULL,
  `which_fat_frying` varchar(50) NOT NULL,
  `which_fat_baking` varchar(50) NOT NULL,
  `how_often_fried_home` varchar(50) NOT NULL,
  `how_often_fried_away` varchar(50) NOT NULL,
  `visible_fat` varchar(50) NOT NULL,
  `how_often_grilled_roast_meat` int(11) NOT NULL,
  `grilled_roast_meat_how_well_cooked` varchar(50) NOT NULL,
  `how_often_add_salt_cooking` varchar(50) NOT NULL,
  `how_often_add_salt_table` varchar(50) NOT NULL,
  `salt_sub_yes_no` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

