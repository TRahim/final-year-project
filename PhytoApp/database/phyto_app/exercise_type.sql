USE phyto_app;

CREATE TABLE `exercise_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exercise_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;


INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (1,'Walking');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (2,'Running');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (3,'Cycling');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (4,'Rowing');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (5,'Yoga');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (6,'Martial Arts');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (7,'Football');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (8,'Rugby');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (9,'Tennis');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (10,'Cricket');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (11,'Gym (weights)');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (12,'Gym (cardio)');
INSERT INTO `phyto_app`.`exercise_type`(`id`,`exercise_name`) VALUES (13,'Other');
