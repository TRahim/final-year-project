USE phyto_app;

CREATE TABLE `diary_entry_meal_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_type_string` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


INSERT INTO `phyto_app`.`diary_entry_meal_type`(`id`,`meal_type_string`) VALUES (1,'breakfast');
INSERT INTO `phyto_app`.`diary_entry_meal_type`(`id`,`meal_type_string`) VALUES (2,'lunch');
INSERT INTO `phyto_app`.`diary_entry_meal_type`(`id`,`meal_type_string`) VALUES (3,'dinner');
INSERT INTO `phyto_app`.`diary_entry_meal_type`(`id`,`meal_type_string`) VALUES (4,'snacks_other');
