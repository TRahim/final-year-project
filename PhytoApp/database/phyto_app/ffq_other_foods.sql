USE phyto_app;

CREATE TABLE `ffq_other_foods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ffq_entry_id` int(11) NOT NULL,
  `food_name` varchar(255) NOT NULL,
  `serving_size` varchar(255) NOT NULL,
  `frequency` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
