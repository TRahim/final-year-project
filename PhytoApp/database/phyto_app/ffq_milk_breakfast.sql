USE phyto_app;

CREATE TABLE `ffq_milk_breakfast` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ffq_entry_id` int(11) NOT NULL,
  `which_milk` varchar(50) NOT NULL,
  `how_much_milk` varchar(50) NOT NULL,
  `cereal_brand_1` varchar(50) DEFAULT NULL,
  `cereal_type_1` varchar(50) DEFAULT NULL,
  `cereal_brand_2` varchar(50) DEFAULT NULL,
  `cereal_type_2` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
