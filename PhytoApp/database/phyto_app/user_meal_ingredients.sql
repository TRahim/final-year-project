USE phyto_app;

CREATE TABLE `user_meal_ingredients` (
  `ingredient_id` int(11) NOT NULL AUTO_INCREMENT,
  `meal_id` int(11) NOT NULL,
  `ingredient_name` varchar(255) NOT NULL,
  `ingredient_phenoldb_id` int(11) NOT NULL,
  `ingredient_weight` double NOT NULL DEFAULT '0',
  `isoflavones_count` double NOT NULL DEFAULT '0',
  `lignans_count` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`ingredient_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;