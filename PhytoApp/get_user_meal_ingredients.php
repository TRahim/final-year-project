<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 15/04/13
 * Time: 16:47
 * Description:
 */

include "connect.php";

$meal_id = $_REQUEST['meal_id'];

try{
    $sql = "SELECT * FROM user_meal_ingredients WHERE meal_id = ?";
    $statement = $db_handle->prepare($sql);
    $statement->setFetchMode(PDO::FETCH_ASSOC);
    $statement->execute(array($meal_id));

    $result_set = $statement->fetchAll();
    $totalrows = count($result_set);

    if($totalrows > 0){
        $success_response = array('success' => true, 'ingredients' => $result_set);
        echo json_encode($success_response);
    } else {
        $fail_response = array('success' => false, 'error' => 'no data returned');
        echo($fail_response);
    }
} catch(PDOException $e){
    $fail_response = array('success' => false, 'error' => $e->getMessage());
    echo($fail_response);
}