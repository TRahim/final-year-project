var user_data = []; //Global variable for user information such as name etc
var user_meal_ingredients = {}; //Holds all ingredients while user creates a new meal.
var user_meal_ingredients_to_delete_from_db = [];
var user_meal_poly_count = { isoflavones_count: 0, lignans_count: 0}; //initialize values to 0
var diary_entry_poly_count = {isoflavones_count: 0, lignans_count: 0}; //initialize values to 0
var diary_meal_type; //determines which type of meal is being added to the diary entry
var current_diary_date; //this is used to store diary entries
var user_meal_current_name;
var diary_entry_ingredients = {};
var current_user_isoflavone_level;
var current_user_lignans_level;
var ffq_other_foods = {}; //Holds all the custom foods entered by user for ffq
var ffq_supplements = {}; //holds all supplements entered by the user for ffq
var ffq_all_data = {}; //Holds everything from the ffq
var ffq13_data = {};
var ffq14_data = {};
var all_ffq_valid;
var invalid_ffq_pages = [];
var ffq1_valid = false;
var ffq2_valid = false;
var ffq3_valid = false;
var ffq4_valid = false;
var ffq5_valid = false;
var ffq6_valid = false;
var ffq7_valid = false;
var ffq8_valid = false;
var ffq9_valid = false;
var ffq10_valid = false;
var ffq11_valid = false;
var ffq12_valid = false;
var ffq13_valid = false;
var ffq14_valid = false;
var ffq15_valid = false;
var other_food_to_delete = ""; //used to identify which food in the list of "other foods" in the ffq to remove if user wishes to delete it
var supplement_to_delete = ""; //used to identify which supplement in the list in the ffq to remove if the user wants to delete it
var recommendTotalLig = 0;
var recommendTotalIso = 0;
var recommendData = {};
var recommendDataMain = {};

function validateEmail(email){
    var email_regEx = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;
    return email_regEx.test(email);
}

$('#register_submit').click(function () {
    var name = $('#name').val();
    var surname = $('#surname').val();
    var dob_day = $('#dob_day').val();
    var dob_month = $('#dob_month').val();
    var dob_year = $('#dob_year').val();
    var email = $('#email').val();
    var password = $('#password').val();
    var password2 = $('#password2').val();

    $('#register_validation_error').html('');
    removeValidationErrorClass('label[for="name"], label[for="surname"], label[for="dob_day"], label[for="dob_month"], label[for="dob_year"], label[for="email"], label[for="password"], label[for="password2"]');

    var error_html = "";
    var inputs_valid = true;

    dob_day = parseInt(dob_day);
    dob_month = parseInt(dob_month);
    dob_year = parseInt(dob_year);

    //validate the inputs
    if(name === ""){
        addValidationErrorClass('label[for="name"]');
        error_html += "<p>Please enter your name</p>";
        inputs_valid = false;
    }
    if(surname === ""){
        addValidationErrorClass('label[for="surname"]');
        error_html += "<p>Please enter your surname</p>";
        inputs_valid = false;
    }
    if(dob_day === ""){
        addValidationErrorClass('label[for="dob_day"]');
        error_html += "<p>Please enter your day of birth (eg. 01)</p>";
        inputs_valid = false;
    }
    if(isNaN(dob_day)){
        addValidationErrorClass('label[for="dob_day"]');
        error_html += "<p>Your day of birth must be an integer number (eg. 01)</p>";
        inputs_valid = false;
    }
    if(dob_month === ""){
        addValidationErrorClass('label[for="dob_month"]');
        error_html += "<p>Please enter your month of birth (eg. 01)</p>";
        inputs_valid = false;
    }
    if(isNaN(dob_month)){
        addValidationErrorClass('label[for="dob_month"]');
        error_html += "<p>Your month of birth must be an integer number (eg. 01)</p>";
        inputs_valid = false;
    }
    if(dob_year === ""){
        addValidationErrorClass('label[for="dob_year"]');
        error_html += "<p>Please enter your year of birth (eg. 1970)</p>";
        inputs_valid = false;
    }
    if(isNaN(dob_year)){
        addValidationErrorClass('label[for="dob_year"]');
        error_html += "<p>Your year of birth must be an integer number (eg. 1970)</p>";
        inputs_valid = false;
    }
    if(email === ""){
        addValidationErrorClass('label[for="email"]');
        error_html += "<p>Please enter your email address</p>";
        inputs_valid = false;
    } else {
        if(!validateEmail(email)){
            addValidationErrorClass('label[for="email"]');
            error_html += "<p>The email address you entered is not valid, please try again.</p>";
            inputs_valid = false;
        }
    }
    if(password === ""){
        addValidationErrorClass('label[for="password"]');
        error_html += "<p>Please enter a password.</p>";
        inputs_valid = false;
    }
    if(password2 === ""){
        addValidationErrorClass('label[for="password2"]');
        error_html += "<p>Please confirm your password.</p>";
        inputs_valid = false;
    }
    if(password != password2){
        addValidationErrorClass('label[for="password"], label[for="password2"]');
        error_html += "<p>The passwords you entered did not match, please try again.</p>";
        inputs_valid = false;
    }

    $('#register_validation_error').append(error_html);

    if(inputs_valid){
        var dob_string = dob_year + '-' + dob_month + '-' + dob_day; //put string in format database expects

        var register_form_data = {
            'name': name,
            'surname': surname,
            'dob': dob_string,
            'email': email,
            'password': password
        };

        $.post('new_user.php', register_form_data, function (response) {
            var response_data = $.parseJSON(response);
            if (response_data.success === true) {
                user_data = response_data.user_data;
                $.mobile.changePage('#questionnaire_dialog');
            } else if (response_data.success === false) {
                if(response_data.error == "user_exists"){
                    $('#register_validation_error').html('').append('<p>A user with that email address already exists. If you have forgotten your password please reset it from the login screen.</p>');
                    addValidationErrorClass('label[for="email"]');
                    $('#email').focus();
                } else {
                    //There was an error.
                    $('#register_validation_error').html('').append('<p>Sorry something went wrong, please try again.</p>');
                }
            }
        });
    }

});

$('#password_reset_btn').click(function(){
    var email = $('#login_email').val();
    $('#password_reset_error_message').html('');
    removeValidationErrorClass('label[for="login_email"]');

    if(email === ""){
        $('#password_reset_error_message').html('<p class="validation_error">Please enter your account email address and then press reset again.</p>');
        addValidationErrorClass('label[for="login_email"]');
        $('#login_email').focus();
    } else {
        $.post('reset_password.php', {email : email}, function(response){
            if(response == "success"){
                $('#password_reset_error_message').html('<p class="validation_error">Please check your email for further instructions.</p>');
            } else {
                //something went wrong
            }
        });
    }
});

/* Login Function */
$('#login_submit').click(function () {
    var email = $('#login_email').val();
    var password = $('#login_password').val();
    var form_valid = true;
    $('#login_error_message').html('');
    removeValidationErrorClass('label[for="login_email"], label[for="login_password"]');

    var error_html = "";

    if(email === ""){
        addValidationErrorClass('label[for="login_email"]');
        error_html += '<p>Please enter your email.</p>';
        form_valid = false;
    } else {
        if(!validateEmail(email)){
            addValidationErrorClass('label[for="login_email"]');
            error_html += '<p>The email you\'ve entered is not valid.</p>';
            form_valid = false;
        }
    }

    if(password === ""){
        addValidationErrorClass('label[for="login_password"]');
        error_html += '<p>Please enter your password.</p>';
        form_valid = false;
    }

    $('#login_error_message').append(error_html);


    if(form_valid){
        var login_form_data = {
            'login_email': email,
            'login_password': password
        };

        $.post('login.php', login_form_data, function (response) {
            var response_data = $.parseJSON(response);
            if (response_data.success === true) {
                user_data = response_data.user_data;
                if(response_data.change_pass === true){
                    $.mobile.changePage('#change_password_page');
                } else {
                    getPolyphenolLevelsForToday();
                    createUserMealTable("diet_page");
                    $('#diet_questionnaire_message').html("");
                    if (user_data.questionnaire_complete == "0"){
                        $('#diet_questionnaire_message').append('<p>You have not yet taken the Food Frequency Questionnaire, this will give us a sense of your diet '
                            + 'over the past year and allow us to recommend meals to you which better suit your current eating habits.</p>'
                            + '<p>It is not necessary to complete the questionnaire but if you wish to do so press <a href="#questionnaire_page">here</a>.</p>');
                    } else if (user_data.questionnaire_complete == "1") {
                        $('#diet_questionnaire_message').append('<p>If you would like to retake the Food Frequency Questionnaire please press <a href="#questionnaire_page" id="retake_ffq">here</a>.</p>');
                    } else {
                        //something went wrong
                        alert('something went wrong');
                    }
                    $('#dashboard_greeting').html('<p class="centre">Welcome back ' + user_data.user_name + '</p>');
                    $('#login_email, #login_password').val('');
                    $.mobile.changePage('#dashboard_page');
                }
            } else if (response_data.success === false) {
                if(response_data.error == "no_user" || response_data.error == 'wrong_password'){
                    $('#login_error_message').html('<p class="validation_error">Sorry either your username or password was incorrect. Please check and try again.</p>');
                    addValidationErrorClass('label[for="login_email"], label[for="login_password"]');
                }
                if(response_data.error == "something_went_wrong"){
                    $('#login_error_message').html('<p class="validation_error">Sorry something went wrong, please try again.</p>');
                }
                if(response_data.error == "PDO_exception"){
                    console.log(response_data.message);
                }
            } else {
                $('#login_error_message').html('<p class="validation_error">Sorry something went wrong, please try again.</p>');
            }
        });
    }

});

$('#new_password_submit').click(function(){
    var new_pass = $('#new_password').val();
    var new_pass_confirm = $('#new_password_confirm').val();

    removeValidationErrorClass('label[for="new_pass_confirm"]');
    $('#new_password_error_message').html('');

    if (new_pass != new_pass_confirm){
        addValidationErrorClass('label[for="new_pass_confirm"]');
        $('#new_password_error_message').html('<p class="validation_error">Your passwords did not match, please try again.</p>');
    } else {
        $.post('create_new_user_password.php', {user_id : user_data.user_id, new_pass : new_pass}, function(response){
            if(response == "success"){
                $.mobile.changePage('#dashboard_page');
                getPolyphenolLevelsForToday();
                createUserMealTable("diet_page");
                $('#diet_questionnaire_message').html("");
                if (user_data.questionnaire_complete == "0"){
                    $('#diet_questionnaire_message').append('<p>You have not yet taken the Food Frequency Questionnaire, this will give us a sense of your diet '
                        + 'over the past year and allow us to recommend meals to you which better suit your current eating habits.</p>'
                        + '<p>It is not necessary to complete the questionnaire but if you wish to do so press <a href="#questionnaire_page">here</a>.</p>');
                } else if (user_data.questionnaire_complete == "1") {
                    $('#diet_questionnaire_message').append('<p>If you would like to retake the Food Frequency Questionnaire please press <a href="#questionnaire_page" id="retake_ffq">here</a>.</p>');
                } else {
                    //something went wrong
                    alert('something went wrong');
                }
                $('#dashboard_greeting').html('<p class="centre">Welcome back ' + user_data.user_name + '</p>');
                $('#new_password, #new_password_confirm').val('');
               // $.mobile.changePage('#dashboard_page');
            }
        });
    }
});

$('.logout').click(function(){
    $.post('logout.php', function(response){
        if(response == 'success'){
            user_data = [];
            $.mobile.changePage('#home_page');
        } else {
            //something went wrong
        }
    });
});

$('#dashboard_page').live('pageshow', function(){
    getPolyphenolLevelsForToday();
});

//This runs first time the user logs in so that it initialize the dashboard
function getPolyphenolLevelsForToday(){
    // get all diary entries for today and then calculate the level of polyphenol (isoflavone) content and update progressbar
    var dateString = getTodaysDate();
    var total_isoflavones = 0; //this will be in mg -- need to multiply by 1000 to convert to micrograms
    var total_lignans = 0;
    $.post('get_user_diary_entry.php', {user_id : user_data.user_id, date : dateString}, function(response){
        var responseData = $.parseJSON(response);
        if (responseData.success === true){
            var meal_data = responseData.meals;
            var individual_ingredients = responseData.individual_ingredients;
            if(meal_data != "none"){
                $.each(meal_data, function(key, val){
                    total_isoflavones += parseFloat(val.isoflavones_count);
                    total_lignans += parseFloat(val.lignans_count);
                });
            }
            if(individual_ingredients != "none"){
                $.each(individual_ingredients, function(key,val){
                    total_isoflavones += parseFloat(val.isoflavones_count);
                    total_lignans += parseFloat(val.lignans_count);
                });
            }
            total_isoflavones.toFixed(2);
            total_lignans.toFixed(2);
        }

        updateDashboardPolyphenolTotal(total_isoflavones, total_lignans );
    });
}
/*
function calculateIsoflavonePercentage(isoflavones_count){
    isoflavones_count = isoflavones_count*1000; //convert to micrograms
    //highest isoflavone value in sample data was ~50,000 micrograms so using 50,000 as upper limit of the percentage calculation.
    var percentage = ((isoflavones_count/50000)*100);
    if(percentage > 100){
        percentage = 100;
    }
    return percentage;
}

function calculateLignansPercentage(lignans_count){
    //according to Phyto-App research abstract low lignans consumption is < 150 micrograms and high consumption is > 300 micrograms
    //data for an example value for very high consumption at the time of writing this is unavailable therefore a top end value of 600 micrograms will be used for the calculation.
    lignans_count = lignans_count*1000; //convert to micrograms
    var percentage = ((lignans_count/600)*100);
    if(percentage > 100){
        percentage = 100;
    }
    return percentage;
}*/

/*function updateIsoProgressBar(isoflavones_level){
    if(isoflavones_level < 34){
        $('#isoflavones_progress_bar_dashboard div, #isoflavones_progress_bar_diary div').css('background-color', 'red');
        $('#iso_progress_message').html('<p>Your Isoflavone levels for today are low.</p>');
    } else if (isoflavones_level >= 34 && isoflavones_level < 67) {
        $('#isoflavones_progress_bar_dashboard div, #isoflavones_progress_bar_diary div').css('background-color', 'orange');
        $('#iso_progress_message').html('<p>Your Isoflavone levels for today are medium.</p>');
    } else {
        $('#isoflavones_progress_bar_dashboard div, #isoflavones_progress_bar_diary div').css('background-color', 'green');
        $('#iso_progress_message').html('<p>Your Isoflavone levels for today are high.</p>');
    }
    $('#isoflavones_progress_bar_dashboard div, #isoflavones_progress_bar_diary div').animate({width: isoflavones_level + '%'}, 500);
}

function updateLignansProgressBar(lignans_level){
    if(lignans_level < 34){
        $('#lignans_progress_bar_dashboard div, #lignans_progress_bar_diary div').css('background-color', 'red');
        $('#lignans_progress_message').html('<p>Your Lignans levels for today are low.</p>');
    } else if (lignans_level >= 34 && lignans_level < 67) {
        $('#lignans_progress_bar_dashboard div, #lignans_progress_bar_diary div').css('background-color', 'orange');
        $('#lignans_progress_message').html('<p>Your Lignans levels for today are medium.</p>');
    } else {
        $('#lignans_progress_bar_dashboard div, #lignans_progress_bar_diary div').css('background-color', 'green');
        $('#lignans_progress_message').html('<p>Your Lignans levels for today are high.</p>');
    }
    $('#lignans_progress_bar_dashboard div, #lignans_progress_bar_diary div').animate({width: lignans_level + '%'}, 500);
}*/

function createUserMealTable(where){
    var user_meals = user_data.user_meals;

    if(where == "diet_page"){
        if($(user_meals).size() !== 0){
            $('#user_meal_list_empty').css('display', 'none');
            $.each(user_meals, function(key, val){
                $('#user_meal_list').append('<li id="diet_meal_id_' + val.meal_id + '"><a href="#" data-isoflavones-count="' + val.isoflavones_count + '" data-lignans-count="' + val.lignans_count + '" data-meal-name="' + val.meal_name + '" data-meal-id="' + val.meal_id + '" class="edit_user_meal_btn"><p><h3>' + val.meal_name + '</h3></p><p><strong>Isoflavones content: </strong>' + val.isoflavones_count + '</p><p><strong>Lignans content: </strong>' + val.lignans_count + '</p></a></li>');
            });
            refreshListView('user_meal_list');
        }
    } else if (where == "diary_page"){
        if($(user_meals).size() !== 0){
            $('#diary_meal_list_empty').css('display', 'none');
            $.each(user_meals, function(key, val){
                $('#diary_meal_list').append('<li id="diary_meal_id_' + val.meal_id + '"><a href="#" data-isoflavones-count="' + val.isoflavones_count + '" data-lignans-count="' + val.lignans_count + '"  data-meal-name="' + val.meal_name + '" data-meal-id="' + val.meal_id + '" class="diary_select_meal_btn"><p><h3>' + val.meal_name + '</h3></p><p><strong>Isoflavones content: </strong>' + val.isoflavones_count + '</p><p><strong>Lignans content: </strong>' + val.lignans_count + '</p></a></li>');
            });
            refreshListView('diary_meal_list');
        }
    }


}

//makes sure that any list views which are being dynamically generated are refreshed or initiliazed correctly so they look ok.
function refreshListView(selector){
    if ($('#' + selector).hasClass('ui-listview')){
        $('#' + selector).listview('refresh');
    } else {
        $('#' + selector).trigger('create');
    }
}

function addNewUserMealToTable(meal_name, poly_count, meal_id){
    var list_size = $('#user_meal_list li').size();

    if(list_size == 2){
        $('#user_meal_list_empty').css('display', 'none');
    }
    $('#user_meal_list').append('<li id="diet_meal_id_' + meal_id + '"><a href="#" class="edit_user_meal_btn" data-isoflavones-count="' + poly_count.isoflavones_count + '" data-lignans-count="' + poly_count.lignans_count + '" data-meal-name="' + meal_name + '" data-meal-id="' + meal_id + '"><p><h3>' + meal_name + '</h3></p><p><strong>Isoflavones content: </strong>' + poly_count.isoflavones_count + '</p><p><strong>Lignans content: </strong>' + poly_count.lignans_count + '</p></a></li>');
    refreshListView('user_meal_list');
    var user_meals = user_data.user_meals;
    if (user_meals !== 0){
        user_meals.push({ meal_id : meal_id, meal_name : meal_name, isoflavones_count : poly_count.isoflavones_count, lignans_count : poly_count.lignans_count});
    } else {
        user_meals = [{meal_id : meal_id, meal_name : meal_name, isoflavones_count : poly_count.isoflavones_count, lignans_count : poly_count.lignans_count}];
    }

    user_data['user_meals'] = user_meals;
}

function removeUserMealFromTable(meal_id){
    var list_size = $('#user_meal_list li').size();
    $('#diet_meal_id_' + meal_id).remove();
    list_size--;
    if(list_size == 2){
        $('#user_meal_list_empty').css('display', 'block');
    }
}

function clearUserMealTable(where){
    if(where == "diet_page"){
        var list_size = $('#user_meal_list li').size();
        if(list_size != 2){
            while(list_size != 2){
                $('#user_meal_list li:last-child').remove();
                list_size = $('#user_meal_list li').size();
            }
            $('#user_meal_list_empty').css('display', 'block');
        }
    } else if(where == "diary_page") {
        var list_size = $('#diary_meal_list li').size();
        if(list_size != 2){
            while(list_size != 2){
                $('#diary_meal_list li:last-child').remove();
                list_size = $('#diary_meal_list li').size();
            }
            $('#diary_meal_list_empty').css('display', 'block');
        }
    }

}

function removeIngredientsFromList(){
    var list_size = $('#custom_meal_ingredients ul li').size();
    if(list_size != 2){
        while(list_size != 2){
            $('#custom_meal_ingredients ul li:last-child').remove();
            list_size = $('#custom_meal_ingredients ul li').size();
        }
        $('#add_meal_list_empty').css('display', 'block');
    }
}

$('#add_meal_btn').click(function(){
    $('#delete_meal_btn').css('display', 'none');
    $('#update_meal_btn').css('display', 'none');
    $('#create_meal_save_btn').css('display', 'block');
    $.mobile.changePage('#create_meal_page');
});

/* Create meal search functionality */
$('#create_meal_search_btn').click(function () {
    $.mobile.loading("show");
    var keywords = $('#create_meal_search').val();
    if (keywords !== '') {
        $('#meal_search_results').html('');
        $.post('search_food.php', {kw: keywords}, function (response) {
            if(response != "no_result_found"){
                var data = $.parseJSON(response);
                var items = [];
                $('#meal_search_results').append('<ul id="food_search_list" data-role="listview" data-inset="true">');
                $.each(data, function (key, val) {
                    items.push('<li data-icon="plus" class="meal_search_result" data-food-id="' + val.FOOD_ID + '" data-food-name="' + val.FOOD_NAME +'"><a href="#">' + val.FOOD_NAME + '</a></li>');
                });
                items.push('</ul>');
                $.mobile.loading("hide");
                $('#food_search_list').append(items).listview().listview('refresh');
            } else {
                $('#meal_search_results').append('<p>Sorry, that item was not found in the database.</p>');
                $.mobile.loading("hide");
            }
        });
    } else {
        $('#meal_search_results').html("");
        $.mobile.loading("hide");
    }
});

/*Get all food data from database and create collapsibles for food selection first time the create meal page is shown */
$('#create_meal_page').one('pageshow', function(){
    $.mobile.loading("show");
    $.getJSON('get_food_groups.php', function(data){
        //create collapsible html for each group
        $.each(data, function(key, val){
            var current_group_id = "food_group_" + val.group_id;
            var sub_groups = val.subgroups;
            //Create a collapsible out of each group
            $('#food_groups').append('<div data-role="collapsible" data-content-theme="d" data-theme="b" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u" data-iconpos="right" class="food_group" id="' + current_group_id + '">'
                + '<h1>' + val.group_name + '</h1><div data-role="listview" id="'+current_group_id+'_subgroups"></div>');
            $.each(sub_groups, function(key, val){
                var current_subgroup_id = "subgroup_" + val.subgroup_id;
                var foods = val.subgroup_foods;
                $('#' + current_group_id + '_subgroups').append('<div data-role="collapsible" data-content-theme="d" data-theme="b" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u" data-iconpos="right" class="food_subgroup" id="' + current_subgroup_id + '">'
                    + '<h1>' + val.subgroup_name + '</h1><ul data-role="listview" class="food_items_list" id="' + current_subgroup_id + '_foods"></ul></div>').collapsibleset();
                $.each(foods, function(key,val){
                    $('#' + current_subgroup_id + '_foods').append('<li data-icon="plus"  class="meal_food_item" data-food-name="'+ val.food_name + '" data-food-id="' + val.food_id + '"><a href="#"><div>'+ val.food_name + '</div></a></li>');
                });
            });
            $('#food_groups').collapsibleset('refresh');
            $('.food_items_list').listview();
        });
        $.mobile.loading("hide");
    });
});

//Make sure that each time you navigate away from create meal page that ingredients are removed from the list
//text input fields are reset to blank, polyphenol count reset & collapsibles are closed.
$('#diet_page').live('pageshow', function(){
    removeIngredientsFromList();
    $('#create_meal_page input[type="text"]').val('');
    $('#create_meal_page div[data-role="collapsible"]').trigger('collapse');
    $('#meal_polyphenol_count').html('');
});

//// Add food item from search results or list of types to the table on screen. /////
$('.meal_food_item, .meal_search_result').live('click',function(){
    var food_name = $(this).data('food-name');
    var food_id = $(this).data('food-id');
    var weight = $('#create_meal_ingredient_weight').val();
    removeValidationErrorClass('label[for="create_meal_ingredient_weight"]');
    $('#add_meal_weight_error_message').html('');
    var list_size = $('#custom_meal_ingredients ul li').size();

    if(weight === ""){
        addValidationErrorClass('label[for="create_meal_ingredient_weight"]');
        $('#add_meal_weight_error_message').html('<p class="validation_error">Please state a weight for the ingredient.</p>');
        $('#create_meal_select_quantities').trigger('expand');
        $('#create_meal_ingredient_weight').focus();

    } else if (isNaN(weight)){
        addValidationErrorClass('label[for="create_meal_ingredient_weight"]');
        $('#add_meal_weight_error_message').html('<p class="validation_error">Weight must be a decimal.</p>');
        $('#create_meal_select_quantities').trigger('expand');
        $('#create_meal_ingredient_weight').focus();
    } else {
        $.post('get_food_polyphenol_count.php', {food_id: food_id }, function(response){
            var response_data = $.parseJSON(response);
            var isoflavones_count = 0;
            var lignans_count = 0;

            if(response_data.success === true){
                if(weight !== ''){
                    var isosflavones = response_data.isoflavones_total;
                    var lignans = response_data.lignans_total;

                    // responses will be figures in mg/100g so divide by 100 and multiply by weight
                    // to get total content in mg rounded to 2 decimal places.
                    isoflavones_count = ((isosflavones/100)*weight).toFixed(2);
                    lignans_count = ((lignans/100)*weight).toFixed(2);

                    user_meal_poly_count.isoflavones_count += parseFloat(isoflavones_count);
                    user_meal_poly_count.lignans_count += parseFloat(lignans_count);
                }
            }
            list_size++;
            var html = "";
            var ingredient_list_id = "ingredient_list_id_" + list_size;
            $('#add_meal_list_empty').css('display', 'none');

            html = '<li id="'+ingredient_list_id+'"><a href="#" class="ingredients_delete_item" data-iso-count="' + isoflavones_count + '" data-lignans-count="' + lignans_count +'"><p><h4>' + food_name + '</h4></p>';
            html += '<p><strong>Weight: </strong>' + weight + 'g</p>';
            html += '<p><strong>Total Isoflavones: </strong>' + isoflavones_count + 'mg</p>';
            html += '<p><strong>Total Lignans: </strong>' + lignans_count + 'mg</p></a></li>';

            $('#custom_meal_ingredients ul').append(html).listview('refresh');
            $('#create_meal_ingredient_weight, #create_meal_search').val('');
            $('#meal_search_results').html('');
            updateUserMealPolyphenolTotal(user_meal_poly_count.isoflavones_count, user_meal_poly_count.lignans_count);
            //$('#meal_polyphenol_count').html('<p>Total meal Isoflavones content: <span id="meal_iso_count_display" class="' + iso_level_colour_class + '">' + user_meal_poly_count.isoflavones_count.toFixed(2) + 'mg</span></p><p>Total meal Lignans content: <span id="meal_lignans_count_display" class="' + lignans_level_colour_class + '">' + user_meal_poly_count.lignans_count.toFixed(2) + 'mg</span></p>');
            $('#'+ ingredient_list_id + ' span').addClass('ui-icon-red-cross').removeClass('ui-icon-arrow-r'); //add delete icon
            $.mobile.silentScroll(0);

            user_meal_ingredients[ingredient_list_id] = { food_name : food_name, food_id : food_id, food_weight : weight, isoflavones_count : isoflavones_count, lignans_count: lignans_count };
        });

    }
});

function updateUserMealPolyphenolTotal(isoflavones_count, lignans_count){
    var iso_level_colour_class;
    var lignans_level_colour_class;

    if(isoflavones_count >= 16.3){
        iso_level_colour_class = "high_polyphenols";
    } else if (isoflavones_count < 16.3 && isoflavones_count > 1){
        iso_level_colour_class = "medium_polyphenols";
    } else if (isoflavones_count <= 1){
        iso_level_colour_class = "low_polyphenols";
    } else {
        iso_level_colour_class = "no_polyphenols";

    }

    if(lignans_count >= 0.3){
        lignans_level_colour_class = "high_polyphenols";
    } else if (lignans_count < 0.3 && lignans_count > 0.15){
        lignans_level_colour_class = "medium_polyphenols";
    } else if (lignans_count <= 0.15){
        lignans_level_colour_class = "low_polyphenols";
    }else {
        lignans_level_colour_class = "no_polyphenols";
    }

    $('#meal_isoflavone_count span').text(isoflavones_count.toFixed(2)).addClass(iso_level_colour_class);
    $('#meal_lignans_count span').text(lignans_count.toFixed(2)).addClass(lignans_level_colour_class);
}

function updateDiaryEntryPolyphenolTotal(isoflavones_count, lignans_count){
    var iso_level_colour_class;
    var lignans_level_colour_class;

    if(isoflavones_count === 0){
        iso_level_colour_class = "no_polyphenols";
        console.log(iso_level_colour_class);
    } else {
        if(isoflavones_count >= 16.3){
            iso_level_colour_class = "high_polyphenols";
            console.log(iso_level_colour_class);
        } else if (isoflavones_count < 16.3 && isoflavones_count > 1){
            iso_level_colour_class = "medium_polyphenols";
            console.log(iso_level_colour_class);
        } else if (isoflavones_count <= 1){
            iso_level_colour_class = "low_polyphenols";
            console.log(iso_level_colour_class);
        }
    }
    if(lignans_count === 0){
        lignans_level_colour_class = "no_polyphenols";
        console.log(lignans_level_colour_class);
    } else {
        if(lignans_count >= 0.3){
            lignans_level_colour_class = "high_polyphenols";
            console.log(lignans_level_colour_class);
        } else if (lignans_count < 0.3 && lignans_count > 0.15){
            lignans_level_colour_class = "medium_polyphenols";
            console.log(lignans_level_colour_class);
        } else if (lignans_count <= 0.15){
            lignans_level_colour_class = "low_polyphenols";
            console.log(lignans_level_colour_class);
        }
    }

    // console.log(isoflavones_count);
    // console.log(lignans_count);
    $('#diary_total_isoflavone_count span').text(isoflavones_count.toFixed(2));
    $('#diary_total_isoflavone_count span').addClass(iso_level_colour_class);
    $('#diary_total_lignans_count span').text(lignans_count.toFixed(2));
    $('#diary_total_lignans_count span').addClass(lignans_level_colour_class);
}

function updateDashboardPolyphenolTotal(isoflavones_count, lignans_count){
    var iso_level_colour_class;
    var lignans_level_colour_class;

    if(isoflavones_count === 0){
        iso_level_colour_class = "no_polyphenols";
    } else {
        if(isoflavones_count >= 16.3){
            iso_level_colour_class = "high_polyphenols";
        } else if (isoflavones_count < 16.3 && isoflavones_count > 1){
            iso_level_colour_class = "medium_polyphenols";
        } else if (isoflavones_count <= 1){
            iso_level_colour_class = "low_polyphenols";
        }
    }
    if(lignans_count === 0){
        lignans_level_colour_class = "no_polyphenols";
    } else {
        if(lignans_count >= 0.3){
            lignans_level_colour_class = "high_polyphenols";
        } else if (lignans_count < 0.3 && lignans_count > 0.15){
            lignans_level_colour_class = "medium_polyphenols";
        } else if (lignans_count <= 0.15){
            lignans_level_colour_class = "low_polyphenols";
        }
    }

    $('#dashboard_total_isoflavone_count span').text(isoflavones_count.toFixed(2)).addClass(iso_level_colour_class);
    $('#dashboard_total_lignans_count span').text(lignans_count.toFixed(2)).addClass(lignans_level_colour_class);
}

$('.edit_user_meal_btn').live('click', function(){
    var meal_id = $(this).data('meal-id');
    var meal_name = $(this).data('meal-name');
    var meal_isoflavones_count = $(this).data('isoflavones-count');
    var meal_lignans_count = $(this).data('lignans-count');
    $.post('get_user_meal_ingredients.php', {meal_id : meal_id}, function(response){
        var data = $.parseJSON(response);
        if(data.success === true){
            var ingredients = data.ingredients;
            var list_size = $('#custom_meal_ingredients ul li').size();
            $('#add_meal_list_empty').css('display', 'none');
            $.each(ingredients, function(key, val){
                list_size++;
                var html = "";
                var ingredient_list_id = "ingredient_list_id_" + list_size;
                var ingredient_name = val.ingredient_name;
                var weight = val.ingredient_weight;
                var isoflavones_content = val.isoflavones_count;
                var lignans_content = val.lignans_count;
                var ingredient_db_id = val.ingredient_id;
                $('#custom_meal_ingredients ul').append('<li id="' + ingredient_list_id + '"><a href="#" class="edit_meal_ingredients_delete_item" data-ingredient-id="' + ingredient_db_id + '"><p><h4>' + ingredient_name + '</h4></p>');
                $('#' + ingredient_list_id + ' a').append('<p><strong>Weight: </strong>' + weight + 'g</p>');
                $('#' + ingredient_list_id + ' a').append('<p><strong>Total Isoflavone content: </strong>' + isoflavones_content + 'mg</p>');
                $('#' + ingredient_list_id + ' a').append('<p><strong>Total Lignans content: </strong>' + lignans_content + 'mg</p></a>');
                $('#custom_meal_ingredients ul').append('</li>');
                refreshListView('custom_meal_ingredients ul');
            });
            $('#create_meal_name_input').val(meal_name);
            user_meal_current_name = meal_name;
            $('#meal_polyphenol_count').html('<p>Total meal Isoflavone content: ' + meal_isoflavones_count + 'mg</p><p>Total meal Lignans content: ' + meal_lignans_count + 'mg</p>');
            user_meal_poly_count.isoflavones_count = meal_isoflavones_count;
            user_meal_poly_count.lignans_count = meal_lignans_count;
            $('#create_meal_save_btn').css('display', 'none');
            $('#delete_meal_btn').css('display', 'block').data('meal-to-delete', meal_id);
            $('#update_meal_btn').css('display', 'block').data('meal-to-update', meal_id);
            $.mobile.changePage('#create_meal_page');
        } else {
            alert(data.error);
        }
    });
});

$('#delete_meal_btn').click(function(){
    var meal_id = $(this).data('meal-to-delete');
    $.post('delete_user_meal.php', {meal_id : meal_id}, function(response){
        if(response == "success"){
            removeUserMealFromTable(meal_id);
            removeIngredientsFromList();
            var user_meals = user_data.user_meals;
            $.each(user_meals, function(key, val){
               if (val.meal_id == meal_id){
                   delete user_meals[key];
                   return false;
               }
            });
            $.mobile.changePage('#diet_page');
        }
    });
});

$('.edit_meal_ingredients_delete_item').live('click', function(){
    var ingredient_db_id = $(this).data('ingredient-id');
    var ingredient_to_delete = $(this).parents('li').attr('id');
    user_meal_ingredients_to_delete_from_db.push(ingredient_db_id);
    $('#' + ingredient_to_delete).remove();

    var list_size = $('#custom_meal_ingredients ul li').size();
    if(list_size == 2){
        $('#add_meal_list_empty').css('display','block');
    }
    $('#meal_search_results').html('');
    $('#create_meal_search').val('');
    $('#create_meal_search_collapsible, #food_selection_groups').trigger('collapse');
});

$('.ingredients_delete_item').live('click', function(){
    var ingredient_to_delete = $(this).parents('li').attr('id');
    var iso_count = $(this).data('iso-count');
    var lignans_count = $(this).data('lignans-count');
    user_meal_poly_count.isoflavones_count -= iso_count;
    user_meal_poly_count.lignans_count -= lignans_count;
    updateUserMealPolyphenolTotal(user_meal_poly_count.isoflavones_count,  user_meal_poly_count.lignans_count);
    $('#' + ingredient_to_delete).remove();
    delete user_meal_ingredients[ingredient_to_delete];
    //ingredient_to_delete = "";

    var list_size = $('#custom_meal_ingredients ul li').size();
    if(list_size == 2){
        $('#add_meal_list_empty').css('display','block');
    }
    $('#meal_search_results').html('');
    $('#create_meal_search').val('');
    $('#create_meal_search_collapsible, #food_selection_groups').trigger('collapse');
});

$('#create_meal_save_btn').click(function(){
    var data = {};
    var meal_name = $('#create_meal_name_input').val();
    removeValidationErrorClass('label[for="create_meal_name_input"]');

    if(meal_name === ""){
        addValidationErrorClass('label[for="create_meal_name_input"]');
    } else {
        data['user_id'] = user_data.user_id;
        data['meal_name'] = meal_name;
        data['ingredients'] = user_meal_ingredients;
        data['polyphenol_count'] = user_meal_poly_count;
    }

    $.post('add_user_meal.php', data, function(response){
        var data = $.parseJSON(response);
        if(data.success === true){
            $.each(user_meal_ingredients, function(key, val){
                $('#' + key).remove(); //remove each ingredient from the ingredient list.
            });
            user_meal_ingredients = {};
            $('#add_meal_list_empty').css('display', 'block');
            $('#create_meal_name_input').val('');
            addNewUserMealToTable(meal_name, user_meal_poly_count, data.meal_id);
            user_meal_poly_count = {};
            $.mobile.changePage('#diet_page');
            $('#meal_polyphenol_count').html('');
            $('#create_meal_search_collapsible, #food_selection_groups').trigger('collapse');
        } else {
            alert(data.error);
        }
    });
});

$('#update_meal_btn').click(function(){
    var data = {};
    var meal_id = $(this).data('meal-to-update');
    var edit_meal_name = $('#create_meal_name_input').val();
    var update_meal_name = false;
    data['meal_id'] = meal_id;
    if(edit_meal_name != user_meal_current_name){
        data['meal_name'] = edit_meal_name;
        update_meal_name = true;
    } else {
        data['meal_name'] = "no_change";
    }
    data['new_ingredients'] = user_meal_ingredients;
    data['new_poly_count'] = user_meal_poly_count;

    $.post('update_user_meal.php', data, function(response){
        var data = $.parseJSON(response);
        if(data.success === true){
            var user_meals = user_data.user_meals;
            if(update_meal_name === true){
                $.each(user_meals, function(key,val){
                    if(val.meal_id == meal_id){
                        val.meal_name = edit_meal_name;
                        val.isoflavones_count = user_meal_poly_count.isoflavones_count;
                        val.lignans_count = user_meal_poly_count.lignans_count;
                    }
                });
                user_data.user_meals = user_meals;
            } else {
                $.each(user_meals, function(key,val){
                    if(val.meal_id == meal_id){
                        val.isoflavones_count = user_meal_poly_count.isoflavones_count;
                        val.lignans_count = user_meal_poly_count.lignans_count;
                    }
                });
                user_data.user_meals = user_meals;
            }
            user_meal_ingredients = {};
            user_meal_poly_count = {};
            user_meal_current_name = "";
            clearUserMealTable("diet_page");
            createUserMealTable("diet_page");
            $.mobile.changePage('#diet_page');
        } else {
            alert(data.error);
        }
    });
});

// Date box functionality for diary
$('#open_calendar_btn').click(function(){
    $('#date_box_input').datebox('open');
});
//every time date changes update the text on the calendar button & query database to check if there is an entry to display
$('#date_box_input').bind('change', function(e,p) {
    $('#open_calendar_btn').find('.ui-btn-text').text($(this).val());
    getDiaryEntry($(this).val());
    workLevels();
});

$('#diary_page').live('pageshow', function(){
    var dateString = getTodaysDate();
    updateCalendarButtonText(dateString);
    getDiaryEntry(dateString);
    current_diary_date = dateString;
    clearUserMealTable("diary_page");
    workLevels();
});

function getTodaysDate(){
    //have to do all of this because the 'getTheDate' method of the dateBox plugin returns a super long string which I don't know how to format properly
    var currentDate = new Date();
    var day = currentDate.getDate(); //returns current day from 1-31
    var month = currentDate.getMonth() + 1; //returns current month from 0-11 (!?!?) so we must add 1
    var year = currentDate.getFullYear(); //returns the year
    var dateString = "";
    if (month < 10){
        dateString = year + '-0' + month + '-' + day;
    } else {
        dateString = year + '-' + month + '-' + day;
    }
    return dateString;
}

function updateCalendarButtonText(text){
    $('#open_calendar_btn').find('.ui-btn-text').text(text);
}
//change date to previous day on button click
$('#calendar_back_btn').click(function(){
    $('#date_box_input').trigger('datebox', {'method':'dooffset', 'type':'d', 'amount':-1}).trigger('datebox', {'method':'doset'});
    current_diary_date = $('#date_box_input').val();
    workLevels();
});
// change date to next day on button click
$('#calendar_forward_btn').click(function(){
    $('#date_box_input').trigger('datebox', {'method':'dooffset', 'type':'d', 'amount':1}).trigger('datebox', {'method':'doset'});
    current_diary_date = $('#date_box_input').val();
    workLevels();
});

function getDiaryEntry(date){
    var user_id = user_data.user_id;
    $.mobile.loading("show");
    $.post('get_user_diary_entry.php', {user_id : user_id, date : date}, function(response){
        var responseData = $.parseJSON(response);
        if (responseData.success === true){
            var meal_data = responseData.meals;
            var individual_ingredients = responseData.individual_ingredients;
            var exercise = responseData.exercise;
            var diary_total_isoflavones_count = 0;
            var diary_total_lignans_count = 0;
            //index list selectors by meal type so can just loop over the response data and grab the appropriate selector based on which type of meal it is.
            var listSelectors = { '1' : '#diary_breakfast ul', '2' : '#diary_lunch ul', '3' : '#diary_dinner ul', '4' : '#diary_snacks_other ul'};

            resetDiaryScreen();
            //add meals to screen
            if(meal_data != "none"){
                $.each(meal_data, function(key, val){
                    var selector = listSelectors[val.meal_type];
                    diary_total_isoflavones_count += parseFloat(val.isoflavones_count);
                    diary_total_lignans_count += parseFloat(val.lignans_count);
                    $(selector + ' .diary_nothing_selected').css('display', 'none');
                    var html = '<li><h3>' + val.meal_name + '</h3><p><strong>Total Isoflavone Content: </strong>' + val.isoflavones_count + 'mg</p><p><strong>Total Lignans Content: </strong>' + val.lignans_count + 'mg</p><h4>Ingredients: </h4>';
                    var ingredients = val.ingredients;
                    $.each(ingredients, function(key, val){
                        html += '<p><strong>' + val.ingredient_name + '</strong></p><p>Isoflavone Content: ' + val.isoflavones_count + 'mg</p>';
                        html += '<p>Lignans Content: ' + val.lignans_count + 'mg</p>';
                    });
                    html += '</li>';
                    $(selector).append(html);
                });
            }
            //add any individual ingredients to screen
            if(individual_ingredients != "none"){
                $.each(individual_ingredients, function(key,val){
                    var selector = listSelectors[val.meal_type];
                    diary_total_isoflavones_count += parseFloat(val.isoflavones_count);
                    diary_total_lignans_count += parseFloat(val.lignans_count);
                    $(selector + ' .diary_nothing_selected').css('display', 'none');
                    var html = '<li><h3>' + val.ingredient_name + '</h3>';
                    html += '<p><strong>Weight: </strong>' + val.ingredient_weight + '</p>';
                    html += '<p><strong>Isoflavones Content: </strong>' + val.isoflavones_count + 'mg</p>';
                    html += '<p><strong>Lignans Content: </strong>' + val.lignans_count + 'mg</p></li>';
                    $(selector).append(html);
                });
            }
            if(exercise != "none"){
                $('#diary_exercise .diary_nothing_selected').css('display', 'none');
                $.each(exercise, function(key, val){
                    var html = '<li><h3>' + val.exercise_name + '</h3><p><strong>Duration: </strong>' + val.duration + ' minutes</p><p><strong>Intensity: </strong>' + val.intensity + '</p></li>'
                    $('#diary_exercise ul').append(html);
                });
            }
            $('#diary_food_list ul[data-role="listview"]').listview('refresh');
            diary_total_isoflavones_count.toFixed(2);
            diary_total_lignans_count.toFixed(2);
          //   var iso_percentage = calculateIsoflavonePercentage(diary_total_isoflavones_count);
          //   var lignans_percentage = calculateLignansPercentage(diary_total_lignans_count);
          //  // updateIsoProgressBar(iso_percentage);
          // //  updateLignansProgressBar(lignans_percentage);
          //   current_user_isoflavone_level = iso_percentage;
          //   current_user_lignans_level = lignans_percentage;
          //   updateDiaryEntryPolyphenolTotal(diary_total_isoflavones_count, diary_total_lignans_count);
           // $('#diary_total_iso_count').html('<p>Total Isoflavone consumption on this day: ' + diary_total_isoflavones_count + 'mg</p>');
            //$('#diary_total_lignans_count').html('<p>Total Lignans consumption on this day: ' + diary_total_lignans_count + 'mg</p>');
        } else {
            resetDiaryScreen();
            updateDiaryEntryPolyphenolTotal(0,0);
           // updateIsoProgressBar(0);
          //  updateLignansProgressBar(0);
        }
        $.mobile.loading("hide");
    });
}

$('#add_diary_entry_page').one('pageshow', function(){
    $.mobile.loading("show");
    $.getJSON('get_food_groups.php', function(data){
        //create collapsible html for each group
        $.each(data, function(key, val){
            var current_group_id = "diary_food_group_" + val.group_id;
            var sub_groups = val.subgroups;
            //Create a collapsible out of each group
            $('#diary_food_groups').append('<div data-role="collapsible" data-content-theme="d" data-theme="b" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u" data-iconpos="right" class="food_group" id="' + current_group_id + '">'
                + '<h1>' + val.group_name + '</h1><div data-role="listview" id="'+ current_group_id + '_subgroups"></div>');
            $.each(sub_groups, function(key, val){
                var current_subgroup_id = "diary_subgroup_" + val.subgroup_id;
                var foods = val.subgroup_foods;
                $('#' + current_group_id + '_subgroups').append('<div data-role="collapsible" data-content-theme="d" data-theme="b" data-collapsed-icon="arrow-d" data-expanded-icon="arrow-u" data-iconpos="right" class="food_subgroup" id="' + current_subgroup_id + '">'
                    + '<h1>' + val.subgroup_name + '</h1><ul data-role="listview" class="diary_food_items_list" id="' + current_subgroup_id + '_foods"></ul></div>').collapsibleset();
                $.each(foods, function(key,val){
                    $('#' + current_subgroup_id + '_foods').append('<li data-icon="plus" class="diary_food_item" data-food-name="'+ val.food_name + '" data-food-id="' + val.food_id + '"><a href="#"><div>'+ val.food_name + '</div></a></li>');
                });
            });
            $('#diary_food_groups').collapsibleset('refresh');
            $('.diary_food_items_list').listview();
        });
        $.mobile.loading("hide");
    });
});

/* Diary search functionality -- For some reason when trying to create this as a separate function to avoid code duplication it always returns undefined so must repeat code :-/   */
$('#diary_entry_search_btn').click(function () {
    $.mobile.loading("show");
    var keywords = $('#diary_entry_search').val();
    if (keywords !== '') {
        $('#diary_search_results').html('');
        $.post('search_food.php', {kw: keywords}, function (response) {
            var data = $.parseJSON(response);
            var items = [];
            $('#diary_search_results').append('<ul id="diary_food_search_list" data-role="listview" data-inset="true">');
            $.each(data, function (key, val) {
                items.push('<li data-icon="plus" class="diary_search_result" data-food-id="' + val.FOOD_ID + '" data-food-name="' + val.FOOD_NAME +'"><a href="#">' + val.FOOD_NAME + '</a></li>');
            });
            items.push('</ul>');
            $.mobile.loading("hide");
            $('#diary_food_search_list').append(items).listview().listview('refresh');
        });
    } else {
        $('#diary_search_results').html("");
        $.mobile.loading("hide");
    }
});

$('.diary_search_result, .diary_food_item').live('click',function(){
    var weight = $('#diary_entry_ingredient_weight').val();
    var food_name = $(this).data('food-name');
    var food_id = $(this).data('food-id');

    removeValidationErrorClass('label[for="diary_entry_ingredient_weight"]');
    $('#diary_ingredients_weight_error_message').html('');
    var list_size = $('#diary_ingredients_to_add ul li').size();

    if(weight === ""){
        addValidationErrorClass('label[for="diary_entry_ingredient_weight"]');
        $('#diary_ingredients_weight_error_message').html('<p class="validation_error">Please state a weight for the ingredient.</p>');
        $('#diary_entry_select_quantities').trigger('expand');
        $('#diary_entry_ingredient_weight').focus();
    } else {
        $.post('get_food_polyphenol_count.php', {food_id: food_id }, function(response){
            var isoflavones_count = 0;
            var lignans_count = 0;
            var response_data = $.parseJSON(response);
            if(response_data != 'no data returned'){
                if(response_data.success == true){
                    // response will be a figure in mg/100g so divide by 100 and multiply by weight
                    // to get total polyphenol content rounded to 2 decimal places.
                    isoflavones_count = ((response_data.isoflavones_total/100)*weight).toFixed(2);
                    lignans_count = ((response_data.lignans_total/100)*weight).toFixed(2);
                    diary_entry_poly_count.isoflavones_count += parseFloat(isoflavones_count);
                    diary_entry_poly_count.isoflavones_count.toFixed(2);
                    diary_entry_poly_count.lignans_count += parseFloat(lignans_count);
                    diary_entry_poly_count.lignans_count.toFixed(2);
                }
            }
            list_size++;
            var html = "";
            var ingredient_list_id = "diary_ingredient_list_id_" + list_size;
            $('#diary_ingredients_list_empty').css('display', 'none');
            html = '<li id="'+ingredient_list_id+'"><a href="#" class="diary_ingredients_delete_item" data-iso-count="' + isoflavones_count + '" data-lignans-count="' + lignans_count +'"><p><h4>' + food_name + '</h4></p>';
            html += '<p><strong>Weight: </strong>' + weight + 'g</p>';
            html += '<p><strong>Total Isoflavones: </strong>' + isoflavones_count + 'mg</p>';
            html += '<p><strong>Total Lignans: </strong>' +  lignans_count + 'mg</p></a></li>';
            $('#diary_ingredients_to_add ul').append(html).listview('refresh');
            $('#diary_entry_ingredient_weight, #diary_entry_search').val('');
            $('#diary_search_results').html('');
           // $('#diary_entry_polyphenol_count').html('<p>Total meal Isoflavones content: ' + diary_entry_poly_count.isoflavones_count + '</p>');
            $('#'+ ingredient_list_id + ' span').addClass('ui-icon-red-cross').removeClass('ui-icon-arrow-r'); //add delete icon
            $.mobile.silentScroll(0);
            diary_entry_ingredients[ingredient_list_id] = { food_name : food_name, food_id : food_id, food_weight : weight, isoflavones_count : isoflavones_count, lignans_count: lignans_count };
        });
    }
});

$('.diary_add_item').click(function(){
    diary_meal_type = $(this).data('entry-type');
    createUserMealTable("diary_page");
    $.mobile.changePage('#add_diary_entry_page');
});

$('.diary_ingredients_delete_item').live('click', function(){
    var ingredient_to_delete = $(this).parents('li').attr('id');
    var iso_count = $(this).data('iso-count');
    var lignans_count = $(this).data('lignans-count');
    diary_entry_poly_count.isoflavones_count -= iso_count;
    diary_entry_poly_count.lignans_count -= lignans_count;
    updateDiaryEntryPolyphenolTotal(diary_entry_poly_count.isoflavones_count,  diary_entry_poly_count.lignans_count);
    $('#' + ingredient_to_delete).remove();
    delete diary_entry_ingredients[ingredient_to_delete];
    //ingredient_to_delete = "";

    var list_size = $('#diary_ingredients_to_add ul li').size();
    if(list_size == 2){
        $('#diary_ingredients_list_empty').css('display','block');
    }
    $('#diary_search_results').html('');
    $('#diary_entry_search, #diary_entry_ingredient_weight').val('');
    $('#create_meal_search_collapsible, #food_selection_groups').trigger('collapse');
});


$('#diary_add_exercise_page').one('pagebeforeshow',function(){
    $.post('get_exercise_types.php', function(response){
        var response_data = $.parseJSON(response);
        if(response_data.success === true){
            var html = '<option value="no_choice"></option>';
            $.each(response_data.data, function(key, val){
                html += '<option value="' + val.id + '">' + val.exercise_name + '</option>';
            });
            $('#select_exercise_list').append(html).selectmenu('refresh');
        } else {
            alert(response_data.error);
        }
    });
});

$('#diary_exercise_add_item').click(function(){
    $.mobile.changePage('#diary_add_exercise_page');
});

$('#diary_add_exercise_confirm_btn').click(function(){
    var exercise_type = $('#select_exercise_list').selectmenu('refresh').val();
    var intensity = $('#exercise_intensity').val();
    var duration = $('#exercise_duration').val();
    var valid = true;

    removeValidationErrorClass('label[for="select_exercise_list"], label[for="exercise_intensity"]', 'label[for="exercise_duration"]');
    $('#exercise_validation_message').html('');

    if(exercise_type == "no_choice"){
        addValidationErrorClass('label[for="select_exercise_list"]');
        valid = false;
        $('#exercise_validation_message').append('<p class="validation_error">Please choose an exercise type</p>');
    }
    if (intensity == "no_choice"){
        addValidationErrorClass('label[for="exercise_intensity"]');
        valid = false;
        $('#exercise_validation_message').append('<p class="validation_error">Please choose an exercise intensity</p>');
    }
    if (duration === "" ){
        addValidationErrorClass('label[for="exercise_duration"]');
        valid = false;
        $('#exercise_validation_message').append('<p class="validation_error">Please input a duration in minutes</p>');
    }
    if(duration == "0"){
        addValidationErrorClass('label[for="exercise_duration"]');
        valid = false;
        $('#exercise_validation_message').append('<p class="validation_error">Duration cannot be 0</p>');
    }
    if (isNaN(duration)){
        addValidationErrorClass('label[for="exercise_duration"]');
        valid = false;
        $('#exercise_validation_message').append('<p class="validation_error">Duration in minutes must be an integer or decimal value (eg. 30, 45.3)</p>');
    }

    if(valid === true){
        $.post('add_diary_entry_exercise.php', {type : exercise_type, intensity : intensity, duration : duration, user_id : user_data.user_id, date : current_diary_date}, function(response){
            if(response == "success"){
                resetExerciseForm();
                $.mobile.changePage('#diary_page');
            }
        });
    }
});

$('#diary_add_exercise_cancel_btn').click(function(){
    $.mobile.changePage('#diary_page');
    resetExerciseForm();
});

function resetExerciseForm(){
   // $('div[data-role="content"] select').val('no_choice').selectmenu('refresh');
    $('div[data-role="content"] input[type="text"]').val('');
}

$('.diary_select_meal_btn').live('click', function(){
    var meal_id = $(this).data('meal-id');
    var user_id = user_data.user_id;
    $.post('add_diary_entry_existing_meal.php', {meal_id : meal_id, user_id : user_id, date : current_diary_date, meal_type : diary_meal_type}, function(response){
        if(response == "success"){
            $.mobile.changePage('#diary_page');
        } else {
            alert(response);
        }
    });
});

$('#diary_save_meal_yes_no').live('change', function(){
    var val = $(this).val();
    if(val == "yes"){
        $('#diary_save_meal_name').textinput('enable');
    } else {
        $('#diary_save_meal_name').textinput('disable');
    }
});

$('#diary_add_ingredients_btn').click(function(){
    var user_id = user_data.user_id;
    var save_meal = $('#diary_save_meal_yes_no').val();
    var meal_name = $('#diary_save_meal_name').val();

    removeValidationErrorClass('label[for="diary_save_meal_name"]');
    $('#diary_save_meal_error_message').html('');

    if(save_meal == "yes"){
        if(meal_name === ""){
            addValidationErrorClass('label[for="diary_save_meal_name"]');
            $('#diary_save_meal_error_message').html('<p class="validation_error">Please enter a name to store the meal as.</p>');
            $('#diary_save_meal_name').focus();
        }
    }
    $.post('add_diary_entry_individual_ingredients.php', {user_id : user_id, save_meal : save_meal, meal_name : meal_name, ingredients : diary_entry_ingredients, date : current_diary_date, meal_type : diary_meal_type}, function(response){
        var response_data = $.parseJSON(response);
        if(response_data.success === true){
            if (response_data.meal_saved === true){
                var meal_id = response_data.meal_id;
                var poly_count = {isoflavones_count : response_data.isoflavones_count, lignans_count : response_data.lignans_count};
                addNewUserMealToTable(meal_name, poly_count, meal_id);
            }
            $.mobile.changePage('#diary_page');
        } else {
            alert(response_data.error);
        }
    });
});

function resetDiaryScreen(){
    var listSelectors = [];
    listSelectors.push('#diary_breakfast ul');
    listSelectors.push('#diary_lunch ul');
    listSelectors.push('#diary_dinner ul');
    listSelectors.push('#diary_snacks_other ul');
    listSelectors.push('#diary_exercise ul');
    $.each(listSelectors, function(key, val){
        var list_size = $(val + ' li').size();
        if(list_size > 2){
            while(list_size != 2){
                $(val + ' li:last-child').remove();
                list_size = $(val + ' li').size();
            }
            $(val + ' .diary_nothing_selected').css('display', 'block');
        }
        $(val + ' .diary_nothing_selected').css('display', 'block');
    });
}

//////////////// Functionality to add "other food" and "supplements & vitamins" to the FFQ ///////////////////

$('#add_supplement_btn').click(function(){
    var supplements_name = $('#supplements_name').val();
    var supplements_brand = $('#supplements_brand').val();
    var supplements_strength = $('#supplements_strength').val();
    var supplements_dose = $('#supplements_dose').val();
    var supplements_frequency = $('#supplements_frequency').val();
    var inputs_valid = false;
    var list_size = $('#supplements_list li').size();

    removeValidationErrorClass('label[for="supplements_name"], label[for="supplements_brand"], label[for="supplements_strength"], label[for="supplements_dose"], label[for="supplements_frequency"]');
    $('#supplements_validation_message').html("");

    validateSupplementInput();

    if(inputs_valid === false){
        $('#supplements_validation_message').html('<p class="validation_error">The fields marked in red must be complete.</p>');
    } else {
        list_size++;
        var supplements_list_id = "supplement_id_" + list_size;
        $('#supplements_list_empty').css('display', 'none');
        $('#supplements_list').append('<li id="' + supplements_list_id + '" ><a href="#"><p><h3>' + supplements_name + '</h3></p>'
                + '<p><strong>Brand: </strong>' + supplements_brand + '</p>'
                + '<p><strong>Dose: </strong>' + supplements_dose + '</p>'
                + '<p><strong>Strength: </strong>' + supplements_strength + '</p>'
                + '<p><strong>Average Frequency: </strong>' + supplements_frequency + '</p></a>'
                + '<a href="#confirm_remove_supplement_dialog" data-role="button" class="supplements_delete_item" data-rel="dialog" data-transition="slidedown" data-supplements-list-id="' + supplements_list_id + '">Remove</a></li>')
           .listview('refresh');

        $('#supplements_name, #supplements_brand, #supplements_strength').val('');
        $('#supplements_dose, #supplements_frequency').val('no_choice').selectmenu('refresh', true);
        var supplement_info = {
            supplement_list_id : supplements_list_id,
            supplement_name : supplements_name,
            supplement_brand : supplements_brand,
            supplement_dose : supplements_dose,
            supplement_strength : supplements_strength,
            supplement_frequency : supplements_frequency
        };

        ffq_supplements[supplements_list_id] = supplement_info;
        //$('#supplements_delete_item').button().button('refresh');
    }

    function validateSupplementInput(){
        inputs_valid = true;

        if(supplements_name === ""){
            addValidationErrorClass('label[for="supplements_name"]');
            inputs_valid = false;
        }
        if(supplements_brand === ""){
            addValidationErrorClass('label[for="supplements_brand"]');
            inputs_valid = false;
        }
        if(supplements_strength === ""){
            addValidationErrorClass('label[for="supplements_strength"]');
            inputs_valid = false;
        }
        if(supplements_dose == "no_choice"){
            addValidationErrorClass('label[for="supplements_dose"]');
            inputs_valid = false;
        }
        if(supplements_frequency == "no_choice"){
            addValidationErrorClass('label[for="supplements_frequency"]');
            inputs_valid = false;
        }
    }
});

$('#other_foods_add_btn').click(function(){
    var food_name = $('#other_foods_food_name').val();
    var serving_size = $('#other_foods_serving_size').val();
    var frequency = $('#other_foods_frequency').val();
    var inputs_valid;
    var list_size = $('#other_foods_list li').size();

    //Reset labels & error message text
    $('label[for="other_foods_food_name"], label[for="other_foods_serving_size"], label[for="other_foods_frequency"] ').removeClass('validation_error');
    $('#other_foods_validation_message').html('');

    validateOtherFoodInput();
    if(inputs_valid === false){
        $('#other_foods_validation_message').html('<p class="validation_error">The fields marked in red must be complete.</p>');
    } else {
        list_size++;
        var food_list_id = "other_food_id_" + list_size;
        $('#other_foods_list_empty').css('display', 'none');
        $('#other_foods_list').append('<li id="' + food_list_id + '" ><a href="#"><p><h3>' + food_name + '</h3></p>'
            + '<p><strong>Usual serving size: </strong>' + serving_size + '</p>'
            + '<p><strong>Number of times eaten each week: </strong>' + frequency + '</p></a>'
            + '<a href="#confirm_remove_food_dialog" data-role="button" class="other_foods_delete_item" data-rel="dialog" data-transition="slidedown" data-food-list-id="' + food_list_id + '">Remove</a></li>')
            .listview('refresh');

        $('#other_foods_food_name, #other_foods_serving_size').val("");
        $('#other_foods_frequency').val('no_choice').selectmenu('refresh', true);
        var food_info = {
            food_list_id : food_list_id,
            food_name : food_name,
            serving_size : serving_size,
            frequency : frequency
        };

        ffq_other_foods[food_list_id] = food_info;
        //$('.other_foods_delete_item').button().button('refresh');
    }

    function validateOtherFoodInput(){
        inputs_valid = true;
        if(food_name === ""){
            $('label[for="other_foods_food_name"]').addClass('validation_error');
            inputs_valid = false;
        }
        if (serving_size === "") {
            $('label[for="other_foods_serving_size"]').addClass('validation_error');
            inputs_valid = false;
        }
        if (frequency == "no_choice") {
            $('label[for="other_foods_frequency"]').addClass('validation_error');
            inputs_valid = false;
        }
    }
});

$('.supplements_delete_item').live('click', function(){
    supplement_to_delete = $(this).data('supplements-list-id');
});

$('#supplements_confirm_delete').click(function(){
    $('#' + supplement_to_delete).remove(); //remove supplement from list on screen
    delete ffq_supplements[supplement_to_delete];
    supplement_to_delete = "";

    var list_size = $('#supplements_list li').size();
    if(list_size == 2){
        $('#supplements_list_empty').css('display', 'block');
    }
    $('#supplements_name, #supplements_brand, #supplements_strength').val('');
    $('#supplements_dose, #supplements_frequency').val('no_choice').selectmenu('refresh', true);
    $.mobile.changePage('#ffq_page15');

});

$('#supplements_cancel_delete').click(function(){
    supplement_to_delete = "";
    $.mobile.changePage('#ffq_page15');
});

$('.other_foods_delete_item').live('click',function(){
    other_food_to_delete = $(this).data('food-list-id');
});


$('#other_foods_confirm_delete').click(function(){
    $('#' + other_food_to_delete).remove(); //remove food from the on screen list
    delete ffq_other_foods[other_food_to_delete]; //remove the food from the data object
    other_food_to_delete = "";
    var list_size = $('#other_foods_list li').size();
    if(list_size == 2){
        $('#other_foods_list_empty').css('display','block');
    }
    $('#other_foods_food_name, #other_foods_serving_size').val("");
    $('#other_foods_frequency').val('no_choice').selectmenu('refresh', true);
    $.mobile.changePage('#ffq_page12');
});

$('#other_foods_cancel_delete').click(function(){
    other_food_to_delete = "";
    $.mobile.changePage('#ffq_page12');
});

//enable other food input fields when user selects yes from list.
$('#other_foods_yes_no').change(function(){
    var val = $(this).val();
    if(val == "yes"){
        $('#other_foods_food_name, #other_foods_serving_size').textinput('enable');
        $('#other_foods_frequency').selectmenu('enable');
        $('#other_foods_add_btn').button('enable');
    } else if (val == "no" || val == "no_choice"){
        $('#other_foods_food_name, #other_foods_serving_size').textinput('disable');
        $('#other_foods_frequency').selectmenu('disable');
        $('#other_foods_add_btn').button('disable');
    }
});


//enable vitamin and supplement input fields when user selects yes from the list
$('#vitamins_yes_no').change(function(){
    var val = $(this).val();
    if(val == "yes"){
        $('#supplements_name, #supplements_brand, #supplements_strength').textinput('enable');
        $('#supplements_dose, #supplements_frequency').selectmenu('enable');
        $('#add_supplement_btn').button('enable');
    } else if (val == "no" || val == "no_choice"){
        $('#supplements_name, #supplements_brand, #supplements_strength').textinput('disable');
        $('#supplements_dose, #supplements_frequency').selectmenu('disable');
        $('#add_supplement_btn').button('disable');
    }
});


//////////// FFQ Form Validation ///////////////

function addValidationErrorClass(selector){
    $(selector).addClass('validation_error');
}

function removeValidationErrorClass(selector){
    $(selector).removeClass('validation_error');
}

//ffq pages 1 - 11 all have confirmation button of the same class which has data attribute of the page id they refer to
// the page id on the data attribute is passed into the validateSelectInputs function which will return true or false
$('.ffq_confirm').click(function(){
    var page_id = $(this).data('page-id');
    if(validateSelectInputs(page_id)){
        if(page_id == "ffq_page1"){
            ffq1_valid = true;
        } else if (page_id == "ffq_page2"){
            ffq2_valid = true;
        }  else if (page_id == "ffq_page3"){
            ffq3_valid = true;
        }  else if (page_id == "ffq_page4"){
            ffq4_valid = true;
        }  else if (page_id == "ffq_page5"){
            ffq5_valid = true;
        }  else if (page_id == "ffq_page6"){
            ffq6_valid = true;
        }  else if (page_id == "ffq_page7"){
            ffq7_valid = true;
        }  else if (page_id == "ffq_page8"){
            ffq8_valid = true;
        }  else if (page_id == "ffq_page9"){
            ffq9_valid = true;
        }  else if (page_id == "ffq_page10"){
            ffq10_valid = true;
        }  else if (page_id == "ffq_page11"){
            ffq11_valid = true;
        }
        $('#questionnaire_page a[href="#' + page_id + '"]').parents('li').attr('data-icon', 'green-check')
            .find('.ui-icon').addClass('ui-icon-green-check').removeClass('ui-icon-arrow-r').removeClass('ui-icon-red-cross');//change icon to be a tick
        //$('#questionnaire_page #ffq_section_list ul').listview('refresh');
        $.mobile.changePage('#questionnaire_page');
    } else {
        if(page_id == "ffq_page1"){
            ffq1_valid = false;
        } else if (page_id == "ffq_page2"){
            ffq2_valid = false;
        }  else if (page_id == "ffq_page3"){
            ffq3_valid = false;
        }  else if (page_id == "ffq_page4"){
            ffq4_valid = false;
        }  else if (page_id == "ffq_page5"){
            ffq5_valid = false;
        }  else if (page_id == "ffq_page6"){
            ffq6_valid = false;
        }  else if (page_id == "ffq_page7"){
            ffq7_valid = false;
        }  else if (page_id == "ffq_page8"){
            ffq8_valid = false;
        }  else if (page_id == "ffq_page9"){
            ffq9_valid = false;
        }  else if (page_id == "ffq_page10"){
            ffq10_valid = false;
        }  else if (page_id == "ffq_page11"){
            ffq11_valid = false;
        }
    }
});

$('.ffq_cancel').click(function(){
    var page_id = $(this).data('page-id');
    resetSelectElements(page_id);
});

//This function used for ffq pages 1 - 11 which only have select inputs and require no other user input.
function validateSelectInputs(page_id) {
    var data = {};
    var current_page_valid = true;

    var page_select_elements = '#' + page_id + ' select';
    var page_label_elements = '#' + page_id + ' label';
    var validation_message_selector = '#' + page_id + ' .ffq_validation_message';

    $(page_label_elements).each(function(){
        $(this).removeClass('validation_error');
    });

    $(validation_message_selector).html("");

    $(page_select_elements).each(function(){
        var value = $(this).val();
        var element_id = $(this).attr('id');
        if(value == "no_choice"){
            addValidationErrorClass('label[for="' + $(this).attr('id') + '"]');
            current_page_valid = false;
        } else {
            data[element_id] = value;
        }
    });

    if(current_page_valid === true) {
        ffq_all_data[page_id] = data;
        return true;
    } else {
        $(validation_message_selector).html('<p class="validation_error">All fields marked in red must be complete.</p>');
        return false;
    }
}


//validates ffq page 12
$('#other_foods_submit').click(function(){
    removeValidationErrorClass('label[for="other_foods_yes_no"], label[for="other_foods_food_name"], label[for="other_foods_serving_size"], label[for="other_foods_frequency"]');
    $('#other_foods_validation_message').html("");
    var yes_no_val = $('#other_foods_yes_no').val();
    ffq12_valid = false;

    if(yes_no_val == "no_choice"){
        addValidationErrorClass('label[for="other_foods_yes_no"]');
    } else if (yes_no_val == "yes"){
        validateOtherFoodList();
    } else if (yes_no_val == "no"){
        ffq12_valid = true;
    }

    if(ffq12_valid){
        $('#questionnaire_page a[href="#ffq_page12"]').parents('li').attr('data-icon', 'green-check')
            .find('.ui-icon').addClass('ui-icon-green-check').removeClass('ui-icon-arrow-r').removeClass('ui-icon-red-cross');//change icon to be a tick
        ffq_all_data.ffq_page_12 = ffq_other_foods;
        $.mobile.changePage('#questionnaire_page');
    }
});

//validates if at least 1 item of food has been added to the list.
function validateOtherFoodList(){
    var list_size = $('#other_foods_list li').size();

    if(list_size == 2) {
        $('#other_foods_validation_message').html('<p class="validation_error">You have selected yes but not added any foods to the list.</p>');
        addValidationErrorClass('label[for="other_foods_food_name"], label[for="other_foods_serving_size"], label[for="other_foods_frequency"]');
    } else if (list_size > 2) {
        ffq12_valid = true;
    }
}

//validates ffq page 13
$('#cereal_breakfast_submit').click(function(){
    removeValidationErrorClass('label[for="which_milk"], label[for="how_much_milk"], label[for="cereal_yes_no"], label[for="milk_specify_other"], label[for="cereal_brand_1"], label[for="cereal_type_1"]');
    var which_milk = $('#which_milk').val();
    var how_much_milk = $('#how_much_milk').val();
    var cereal_yes_no = $('#cereal_yes_no').val();
    ffq13_valid = false;
    var which_milk_valid = false;
    var how_much_milk_valid = false;
    var cereal_valid = false;

    if(which_milk == "no_choice"){
        addValidationErrorClass('label[for="which_milk"]');
    } else if (which_milk == "other"){
        var milk_other = $('#milk_specify_other').val();
        if(milk_other === "") {
            addValidationErrorClass('label[for="milk_specify_other"]');
        } else {
            which_milk_valid = true;
            ffq13_data.which_milk = which_milk;
            ffq13_data.which_milk_other = milk_other;
        }
    } else {
        which_milk_valid = true;
        ffq13_data.which_milk = which_milk;
        //TODO: Maybe need to sort this out.
    }

    if(how_much_milk == "no_choice"){
        addValidationErrorClass('label[for="how_much_milk"]');
    } else {
        how_much_milk_valid = true;
        ffq13_data.how_much_milk = how_much_milk;
    }

    if(cereal_yes_no == "no_choice"){
        addValidationErrorClass('label[for="cereal_yes_no"]');
    } else if (cereal_yes_no == "yes") {
        cereal_valid = true;
        var brand1 = $('#cereal_brand_1').val();
        var type1 = $('#cereal_type_1').val();
        var brand2 = $('#cereal_brand_2').val();
        var type2 = $('#cereal_type_2').val();

        if(brand1 === ""){
            addValidationErrorClass('label[for="cereal_brand_1"]');
            cereal_valid = false;
        } else {
            ffq13_data.cereal_brand_1 = brand1;
        }
        if(type1 === ""){
            addValidationErrorClass('label[for="cereal_type_1"]');
            cereal_valid = false;
        } else {
            ffq13_data.cereal_type_1 = type1;
        }
        if(brand2 !== ""){
            ffq13_data.cereal_brand_2 = brand2;
        }
        if(type2 !== ""){
            ffq13_data.cereal_type_2 = type2;
        }
    } else if (cereal_yes_no == "no"){
        cereal_valid = true;
    }

    if(which_milk_valid === true && how_much_milk_valid === true && cereal_valid === true){
        ffq13_valid = true;
    } else {
        ffq13_valid = false;
    }

    if (ffq13_valid === true) {
        $('#questionnaire_page a[href="#ffq_page13"]').parents('li').attr('data-icon', 'green-check')
            .find('.ui-icon').addClass('ui-icon-green-check').removeClass('ui-icon-arrow-r').removeClass('ui-icon-red-cross');//change icon to be a tick
        ffq_all_data['ffq_page13'] = ffq13_data;
        $.mobile.changePage('#questionnaire_page');
    }
});

//validate ffq page 14
$('#food_preparation_confirm').click(function(){
    removeValidationErrorClass('label[for="which_fat_frying"], label[for="which_veg_oil"], label[for="which_fat_baking"], label[for="which_margarine_type"]');
    removeValidationErrorClass('label[for="how_often_fried_home"], label[for="how_often_fried_away"], label[for="visible_fat"], label[for="how_often_grilled_roast_meat"]');
    removeValidationErrorClass('label[for="grilled_roast_meat_how_well_cooked"], label[for="how_often_add_salt_cooking"]');
    removeValidationErrorClass('label[for="how_often_add_salt_table"], label[for="salt_sub_yes_no"], label[for="which_salt_sub"]');
    var which_fat_frying = $('#which_fat_frying').val();
    var which_fat_baking = $('#which_fat_baking').val();
    var how_often_fried_home = $('#how_often_fried_home').val();
    var how_often_fried_away = $('#how_often_fried_away').val();
    var visible_fat = $('#visible_fat').val();
    var how_often_grilled_roast_meat = $('#how_often_grilled_roast_meat').val();
    var grilled_roast_meat_how_well_cooked = $('#grilled_roast_meat_how_well_cooked').val();
    var how_often_add_salt_cooking = $('#how_often_add_salt_cooking').val();
    var how_often_add_salt_table = $('#how_often_add_salt_table').val();
    var salt_sub_yes_no = $('#salt_sub_yes_no').val();

    ffq14_valid = true;

    if (which_fat_frying == "no_choice"){
        addValidationErrorClass('label[for="which_fat_frying"]');
        ffq14_valid = false;
    } else if (which_fat_frying == "vegetable_oil"){
        var veg_oil = $('#which_veg_oil').val();
        if(veg_oil === ""){
            addValidationErrorClass('label[for="which_veg_oil"]');
            ffq14_valid = false;
        } else {
            ffq14_data.which_fat_frying = which_fat_frying;
            ffq14_data.which_veg_oil = veg_oil;
        }
    } else {
        ffq14_data.which_fat_frying = which_fat_frying;
    }

    if (which_fat_baking == "no_choice"){
        addValidationErrorClass('label[for="which_fat_baking"]');
        ffq14_valid = false;
    } else if (which_fat_baking == "margarine"){
        var margarine = $('#which_margarine_type').val();
        if(margarine === ""){
            addValidationErrorClass('label[for="which_margarine_type"]');
            ffq14_valid = false;
        } else {
            ffq14_data.which_fat_baking = which_fat_baking;
            ffq14_data.which_margarine_type = margarine;
        }
    } else {
        ffq14_data.which_fat_baking = which_fat_baking;
    }

    if(how_often_fried_home == "no_choice"){
        addValidationErrorClass('label[for="how_often_fried_home"]');
        ffq14_valid = false;
    } else {
        ffq14_data.how_often_fried_home = how_often_fried_home;
    }

    if(how_often_fried_away == "no_choice"){
        addValidationErrorClass('label[for="how_often_fried_away"]');
        ffq14_valid = false;
    } else {
        ffq14_data.how_often_fried_away = how_often_fried_away;
    }

    if(visible_fat == "no_choice"){
        addValidationErrorClass('label[for="visible_fat"]');
        ffq14_valid = false;
    } else {
        ffq14_data.visible_fat = visible_fat;
    }

    if(how_often_grilled_roast_meat == "no_choice"){
        addValidationErrorClass('label[for="how_often_grilled_roast_meat"]');
        ffq14_valid = false;
    } else {
        ffq14_data.how_often_grilled_roast_meat = how_often_grilled_roast_meat;
    }

    if(grilled_roast_meat_how_well_cooked == "no_choice"){
        addValidationErrorClass('label[for="grilled_roast_meat_how_well_cooked"]');
        ffq14_valid = false;
    } else {
        ffq14_data.grilled_roast_meat_how_well_cooked = grilled_roast_meat_how_well_cooked;
    }

    if(how_often_add_salt_cooking == "no_choice"){
        addValidationErrorClass('label[for="how_often_add_salt_cooking"]');
        ffq14_valid = false;
    } else {
        ffq14_data.how_often_add_salt_cooking = how_often_add_salt_cooking;
    }

    if (how_often_add_salt_table == "no_choice"){
        addValidationErrorClass('label[for="how_often_add_salt_table"]');
        ffq14_valid = false;
    } else {
        ffq14_data.how_often_add_salt_table = how_often_add_salt_table;
    }

    if(salt_sub_yes_no == "no_choice"){
        addValidationErrorClass('label[for="salt_sub_yes_no"]');
        ffq14_valid = false;
    } else if (salt_sub_yes_no == "yes"){
        var val = $('#which_salt_sub').val();
        if(val === ""){
            addValidationErrorClass('label[for="which_salt_sub"]');
            ffq14_valid = false;
        } else {
            ffq14_data.salt_sub_yes_no = salt_sub_yes_no;
            ffq14_data.which_salt_sub = val;
        }
    } else {
        ffq14_data.salt_sub_yes_no = salt_sub_yes_no;
    }

    if(ffq14_valid){
        $('#questionnaire_page a[href="#ffq_page14"]').parents('li').attr('data-icon', 'green-check')
            .find('.ui-icon').addClass('ui-icon-green-check').removeClass('ui-icon-arrow-r').removeClass('ui-icon-red-cross');//change icon to be a tick
        ffq_all_data.ffq_page14 = ffq14_data;
        $.mobile.changePage('#questionnaire_page');
    }
});

//validate ffq page 15 (supplements)
$('#supplements_confirm_btn').click(function(){
    removeValidationErrorClass('label[for="vitamins_yes_no"], label[for="supplements_name"], label[for="supplements_brand"], label[for="supplements_strength"], label[for="supplements_dose"], label[for="supplements_frequency"]');
    $('#supplements_validation_message').html("");
    var yes_no_val = $('#vitamins_yes_no').val();
    ffq15_valid = false;

    if(yes_no_val == "no_choice"){
        addValidationErrorClass('label[for="vitamins_yes_no"]');
    } else if (yes_no_val == "yes"){
        validateSupplementsList();
    } else if (yes_no_val == "no"){
        ffq15_valid = true;
    }

    if(ffq15_valid){
        $('#questionnaire_page a[href="#ffq_page15"]').parents('li').attr('data-icon', 'green-check')
            .find('.ui-icon').addClass('ui-icon-green-check').removeClass('ui-icon-arrow-r').removeClass('ui-icon-red-cross');//change icon to be a tick
        ffq_all_data['ffq_page15'] = ffq_supplements;
        $.mobile.changePage('#questionnaire_page');
    }
});

function validateSupplementsList(){
    var list_size = $('#supplements_list li').size();

    if(list_size == 2){
        $('#supplements_validation_message').html('<p class="validation_error">You have selected yes but not added any supplements or vitamins to the list.</p>');
        addValidationErrorClass('label[for="supplements_name"], label[for="supplements_brand"], label[for="supplements_strength"], label[for="supplements_dose"], label[for="supplements_frequency"]');
    } else if (list_size > 2){
        ffq15_valid = true;
    }
}

function resetSelectElements(page_id){
    var page_select_elements = '#' + page_id + ' select';
    var page_label_elements = '#' + page_id + ' label';

    $(page_select_elements).each(function(){
       $(this).val("no_choice").selectmenu('refresh', true);
    });

    $(page_label_elements).each(function(){
        $(this).removeClass('validation_error');
    });

}

//Submit the whole ffq form
$('#ffq_complete_submit').click(function(){
    invalid_ffq_pages = [];
    all_ffq_valid = true;
    $('#questionnaire_page .ffq_validation_message').html('');
    if(!ffq1_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page1', title : 'Meat & Fish'});
    }
    if (!ffq2_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page2', title : 'Bread & Savoury Biscuits'});
    }
    if (!ffq3_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page3', title : 'Cereals'});
    }
    if (!ffq4_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page4', title : 'Potatoes, Rice & Pasta'});
    }
    if (!ffq5_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page5', title : 'Dairy Products & Fats'});
    }
    if (!ffq6_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page6', title : 'Butter & Margarine'});
    }
    if (!ffq7_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page7', title : 'Sweets & Snacks'});
    }
    if (!ffq8_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page8', title : 'Soups, Sauces & Spreads'});
    }
    if (!ffq9_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page9', title : 'Drinks'});
    }
    if (!ffq10_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page10', title : 'Fruit'});
    }
    if (!ffq11_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page11', title : 'Vegetables'});
    }
    if (!ffq12_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page12', title : 'Other Foods'});
    }
    if (!ffq13_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page13', title : 'Milk & Breakfast'});
    }
    if (!ffq14_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page14', title : 'Food Preparation'});
    }
    if (!ffq15_valid){
        all_ffq_valid = false;
        invalid_ffq_pages.push({page_id: 'ffq_page15', title : 'Vitamins & Supplements'});
    }

    if(all_ffq_valid) {
        //ffq_all_data;
        $.post('process_ffq.php', { data : ffq_all_data, user_id : user_data.user_id }, function(response){
            alert(response);
        });
    } else {
        $('#questionnaire_page .ffq_validation_message').html('<p class="validation_error">The following sections marked with a cross must be complete: </p>');
        //$('#ffq_section_list ul li').attr('data-theme', 'c').trigger('mouseout');
        $(invalid_ffq_pages).each(function(){
            var target_page = $(this)[0].page_id;
            $('#questionnaire_page a[href="#' + target_page + '"]').parents('li').attr('data-icon', 'red-cross')
                .find('.ui-icon').addClass('ui-icon-red-cross').removeClass('ui-icon-arrow-r');
        });

    }
});


//This function executes a food search as the user is typing -- uncomment if necessary
/*$('#create_meal_search').keyup(function(){
 $.mobile.loading("show");
 delay(function(){
 var keywords = $('#create_meal_search').val();
 if(keywords != ''){
 $('#search_results').html('');
 $.post('search_food.php', {kw:keywords}, function(response){
 var data = $.parseJSON(response);
 var items = [];
 $('#search_results').append('<ul id="food_search_list" data-role="listview" data-inset="true">');
 $.each(data, function(key, val){
 items.push('<li class="search_result" data-food-id="' + val.FOOD_ID + '">' + val.FOOD_NAME + '</li>');
 });
 items.push('</ul>');
 $.mobile.loading("hide");
 $('#food_search_list').append(items);
 $('#food_search_list').listview().listview('refresh');
 });
 } else {
 $('#search_results').html("");
 $.mobile.loading("hide");
 }
 }, 250);
 });*/


/* Generate and embed the google map for the contact page */
$('#contact_page').one('pageshow', function () {
    generateMap('map_canvas', '51.520707', '-0.13999'); //latitude and longitude coordinates of the Cavendish Campus
});

/* Function takes a div selector id (must be an ID, not a class), latitude & longitude and generates google map
 in the given div. Once map is created it creates a marget for the given location.
 */
function generateMap(selector_name, lat, long) {
    var selector_id = "#" + selector_name;
    var latlong = lat + "," + long;
    $(selector_id).gmap({zoom: 16, 'center': latlong}).bind('init', function (ev, map) {
        $(selector_id).gmap('addMarker', { 'position': map.getCenter(), 'bounds': false});
    });
}

/* Delay the execution of the given callback function */
var delay = (function () {
    var timer = 0;
    return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
    };
})();


//when the popup button is clicked the graphData method is called with 
$('#isoPopUp').live('click', function(e) {
  graphData(0);
});

$('#ligPopUp').live('click', function(e){
    graphData(1);
});

//set the diary values
function setValues(iso,lignans){

    $('#diary_total_isoflavone_count span').text(iso);
    $('#diary_total_lignans_count span').text(lignans);
}

//function used to get the graph data for the application, by sending the user id and the current diary date
function graphData(iso_lig){
    $.post('get_user_week_diary.php',{user_id:user_data.user_id, date:current_diary_date},function(response){
        //console.log(response);
        var responseData = $.parseJSON(response);
        var a = 0;
        var dayLigArray = [];
        var dayIsoArray = [];
        //if(responseData.success === true){
            //for every single data in the response push to the isoflavone and lignan arrays
            for ( a in responseData[0])
            {
                dayLigArray.push({date:responseData[0][a]['date'] , lignans_count:responseData[0][a]['lignans_count']});
                dayIsoArray.push({date:responseData[0][a]['date'] , isoflavones_count:responseData[0][a]['isoflavones_count']});
            }
            console.log(dayIsoArray);
            console.log(dayLigArray);

            //send data according to which graph was needed
            if(iso_lig === 0)
            {
                isoChart(dayIsoArray);
            }
            else
            {
                ligChart(dayLigArray);
            }

        //}     
    });
}
function workLevels(){
    //var diaryDate = getDiaryDate();
    var total_isoflavones = 0; //this will be in mg -- need to multiply by 1000 to convert to micrograms
    var total_lignans = 0;
    $.post('get_user_diary_entry.php',{user_id:user_data.user_id, date:current_diary_date},function(response){
        var responseData = $.parseJSON(response);
        //console.log(responseData);
        if (responseData.success === true){
            var meal_data = responseData.meals;
            var individual_ingredients = responseData.individual_ingredients;
            if(meal_data != "none"){
                $.each(meal_data, function(key, val){
                    total_isoflavones += parseFloat(val.isoflavones_count);
                    total_lignans += parseFloat(val.lignans_count);
                });
            }
            if(individual_ingredients != "none"){
                $.each(individual_ingredients, function(key,val){
                    total_isoflavones += parseFloat(val.isoflavones_count);
                    total_lignans += parseFloat(val.lignans_count);
                });
            }
            total_isoflavones.toFixed(2);
            total_lignans.toFixed(2);
        }
        //setValues(total_isoflavones,total_lignans);
        //graphData();
        updateDiaryEntryPolyphenolTotal(total_isoflavones,total_lignans);
        recommendTotalIso = total_isoflavones;
        recommendTotalLig = total_lignans;
    });
}

function isoChart(dayIsoArray){
    //console.log(dayIsoArray[0][]);
    var max = 0;
    // create loop to get highest isoflavone to get max value for graph
    for(a in dayIsoArray){
        //console.log(dayIsoArray[a]['isoflavones_count']);
        if(max < dayIsoArray[a]['isoflavones_count']){
            max = dayIsoArray[a]['isoflavones_count'];
            //console.log(dayIsoArray[a]['isoflavones_count']);
        }
    }

    //for the isochart div create a chart with the data
    $('#isoChart').highcharts({
                chart: {
            type: 'column'
        },
        title: {
            text: 'Weekly Isoflavones Chart'
        },
        xAxis: {
            categories: [dayIsoArray[6]['date'], dayIsoArray[5]['date'], dayIsoArray[4]['date'], dayIsoArray[3]['date'], dayIsoArray[2]['date'], dayIsoArray[1]['date'], dayIsoArray[0]['date']]
        },
        yAxis: {
            min: 0,
            max: max + 10,
            title: {
                text: 'Isoflavones'
            }
        },
        series: [{
            name: "Isoflavones",
            data: [dayIsoArray[6]['isoflavones_count'], dayIsoArray[5]['isoflavones_count'], dayIsoArray[4]['isoflavones_count'], dayIsoArray[3]['isoflavones_count'], dayIsoArray[2]['isoflavones_count'], dayIsoArray[1]['isoflavones_count'], dayIsoArray[0]['isoflavones_count']]
        }]
    });
}
 
//same as isoChart function   
function ligChart(dayLigArray){
    //console.log(dayIsoArray[0][]);
    var max = 0;
    // create loop to get highest isoflavone to get max value for graph
    for(a in dayLigArray){
        //console.log(dayIsoArray[a]['isoflavones_count']);
        if(max < dayLigArray[a]['lignans_count']){
            max = dayLigArray[a]['lignans_count'];
            console.log(dayLigArray[a]['lignans_count']);
        }
    }

    $('#ligChart').highcharts({
                chart: {
            type: 'column'
        },
        title: {
            text: 'Weekly Lignans Chart'
        },
        xAxis: {
            categories:[
                        dayLigArray[6]['date'], 
                        dayLigArray[5]['date'], 
                        dayLigArray[4]['date'], 
                        dayLigArray[3]['date'], 
                        dayLigArray[2]['date'], 
                        dayLigArray[1]['date'], 
                        dayLigArray[0]['date']
                       ]
        },
        yAxis: {
            min: 0,
            max: max + 1,
            title: {
                text: 'Lignans'
            }
        },
        series: [{
            name: "Lignans",
            data: [
                    dayLigArray[6]['lignans_count'], 
                    dayLigArray[5]['lignans_count'], 
                    dayLigArray[4]['lignans_count'], 
                    dayLigArray[3]['lignans_count'], 
                    dayLigArray[2]['lignans_count'], 
                    dayLigArray[1]['lignans_count'], 
                    dayLigArray[0]['lignans_count']
                  ]
        }]
    });
}

//function for picture submit
$('#upload_submit').click(function(){

    var name;
    var weight;
    var picID;
    //get the file information and call an AJAX request
    var file = $('#file').get(0).files[0];
    var fd = new FormData();
    fd.append("file", file);
    
    //script an AJAX request
    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'compare_images.php', true);
  
    xhr.upload.onprogress = function(e) {
      if (e.lengthComputable) {
        var percentComplete = (e.loaded / e.total) * 100;
        console.log(percentComplete + '% uploaded');
        $.mobile.loading("show");
      }
    };
 
    xhr.onload = function() {
        if (this.status == 200) {
            $.mobile.loading("hide");
            var resp = JSON.parse(this.response);
            if(resp.success === true){
                foodName = resp.name;
                weight = resp.weight; 
                picID = resp.id;

                //append data to the popup 
                $("#picContent").empty();
                $("#picContent").append('<p> Meal name: <input type="text" id="picMealName" /> <br /> Food name: ' + foodName + '<br /> Weight: <input type="text" id="picWeight" onkeypress="return isNumber(event)" value=' + weight + ' /> <input type="submit" id="pictureConfirm" value="Yes"/> <input type="button" value="No" id="picClose"/> <input type="hidden" id="picID" value=' + picID + ' </p>');
                $("#confirmFoodPopUp").popup("open");
                $('#file').val("");
            }
            else
            {
                $("#failPicPopup").popup("open");
            }
        };
    };
 
    xhr.send(fd);
    }, false);

//function to confirm the food
$(document).on('click', '#pictureConfirm', function(){
    var foodWeight = $('#picWeight').val();
    var mealName = $('#picMealName').val();
    var foodID = $('#picID').val();

    if (foodWeight.length == 0 || mealName.length == 0)
    {
        //alert("Please fill in Meal name or Weight");
        $('#picError').empty();
        $('#picError').append('Please fill in Meal name or Weight');
    }
    else
    {
        $('#picError').empty();
        $("#confirmFoodPopUp").popup("close");
        pictureSubmit(foodID, mealName, foodWeight, foodName);
    }
});

//if the user clicks no then it will close the popup
$(document).on('click', '#picClose', function(){
    $('#confirmFoodPopUp').popup("close");
});

function pictureSubmit(foodID, mealName, foodWeight, foodName){
    //var foodDetails = array('id' => foodID, 'weight' => foodWeight, 'name' => foodName);
    var foodDetails = {};
    foodDetails.id = foodID;
    foodDetails.weight = foodWeight;
    foodDetails.name = foodName;

    var polyphenolCount = {};
    //get the phenol information from the php script
    $.post('get_food_polyphenol_count.php', {food_id: foodID }, function(response){
            var response_data = $.parseJSON(response);
            var isoflavones_count = 0;
            var lignans_count = 0;

            if(response_data.success === true){
                if(foodWeight !== ''){
                    var isosflavones = response_data.isoflavones_total;
                    var lignans = response_data.lignans_total;

                    // responses will be figures in mg/100g so divide by 100 and multiply by weight
                    // to get total content in mg rounded to 2 decimal places.
                    isoflavones_count = ((isosflavones/100)*foodWeight).toFixed(2);
                    lignans_count = ((lignans/100)*foodWeight).toFixed(2);

                }
            }

            polyphenolCount.isoflavones_count = isoflavones_count;
            polyphenolCount.lignans_count = lignans_count;

            //add the meal with all the phenol and food data
            $.post('add_pic_meal.php', {food_details: foodDetails, meal_name: mealName, user_id: user_data.user_id, polyphenol_count: polyphenolCount}, function(resp){
                var respData = $.parseJSON(resp);
                console.log(respData);
                if(respData.success === true){
                    addNewUserMealToTable(mealName, polyphenolCount, respData.meal_id);
                    $.mobile.changePage('#diet_page');
                }
                
            });
            
        });
}

//recommend a meal to the user
$('#recommendBtn').live('click', function(e){

    var total_isoflavones = 0; //this will be in mg -- need to multiply by 1000 to convert to micrograms
    var total_lignans = 0;
    var polyphenol_levels = {};
    
    var mealData = [];
    var total_lignans_recommended = 0;
    var total_isoflavones_recommended = 0;

    // console.log(current_diary_date);
    // console.log(user_data.user_id);

    polyphenol_levels.isoflavones_count = recommendTotalIso;
    polyphenol_levels.lignans_count = recommendTotalLig;
    // console.log(recommendTotalLig);
    // console.log(recommendTotalIso);
    //console.log(polyphenol_levels);
    
    //send all the data about phenol levels 
    $.post('recommend_user_meal.php', {polyphenol_levels:polyphenol_levels}, function(res){
        var resData = $.parseJSON(res);
        console.log(resData);
        if (resData.success == true){

            var mealName = resData.meal_name;
            var phenol = resData.phenol;
            $.each(phenol, function(key,val){
                var ingredients = {};
                total_isoflavones_recommended += parseFloat((val.weight * val.isoflavones_total)/100);
                total_lignans_recommended += parseFloat((val.weight * val.lignans_total)/100);

                ingredients.food_name = val.name;
                ingredients.food_weight = val.weight;
                ingredients.isoflavones_count = (val.weight * val.isoflavones_total)/100;
                ingredients.lignans_count = (val.weight * val.lignans_total)/100;

                ingredients.isoflavones_count = ingredients.isoflavones_count.toFixed(2);
                ingredients.lignans_count = ingredients.lignans_count.toFixed(2);
                //console.log(ingredients);
                ingredients.food_id = val.id;
                mealData.push(ingredients);
                //console.log(mealData);
            });

            total_isoflavones_recommended = total_isoflavones_recommended.toFixed(2);
            total_lignans_recommended = total_lignans_recommended.toFixed(2);

            recommendData = mealData;
            recommendData.meal_name = mealName;
            recommendDataMain.isoflavones_count = total_isoflavones_recommended;
            recommendDataMain.lignans_count = total_lignans_recommended;

            //open the recommend data in a popup
            $('#recommendContent').empty();
            $('#recommendContent').append('<p> <b>' + mealName + ' </b> </p>');
            $('#recommendContent').append('<p> Total isoflavone: ' + total_isoflavones_recommended + '<br /> Total lignans: ' + total_lignans_recommended);
            mealData.forEach(function(entry){
                $('#recommendContent').append('<p> Ingredient Name: ' + entry["food_name"] + '<br /> Ingredient weight: ' + entry["food_weight"] + '<br /> Isoflavone level: ' + entry["isoflavones_count"] + '<br /> Lignan level: ' + entry["lignans_count"] + '</p>');
            });
            $('#recommendContent').append('<input type="submit" id="recommendSubmit" value="Add to meal" /> <input type="button" value="Cancel" id="recommendCancel"/> ');
        }else{
            $('#recommendContent').empty();
            $('#recommendContent').append('<p style=color:red> Sorry we could not find an appropriate meal for you </p>');
        }
        
    });
});

//submit data to the database.
$(document).on('click', '#recommendSubmit', function(){
    if(recommendData !== null){
        //console.log(recommendData);
        var meal_name = recommendData.meal_name;
        var user_id = user_data.user_id;
        $.post('add_user_meal.php', {user_id:user_id, meal_name:meal_name, ingredients:recommendData, polyphenol_count:recommendDataMain}, function(response){
            var respData = $.parseJSON(response);
            if(respData.success === true){
                $('#recommendPopUp').popup("close");
                $.mobile.changePage('#diet_page');
            }
        });

    }
});

//close the popup
$(document).on('click', '#recommendCancel', function(){
    $('#recommendPopUp').popup("close");
});
//taken from http://stackoverflow.com/questions/7295843/allow-only-numbers-to-be-typed-in-a-textbox at 31/03/2014
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
