<?php


include "connect.php";

$email = $_REQUEST['login_email'];
$password = $_REQUEST['login_password'];

try {
    $sql = "SELECT * FROM users WHERE email = ?;";
    $statement = $db_handle->prepare($sql);
    $statement->execute(array($email));
    $statement->setFetchMode(PDO::FETCH_ASSOC);

    $result_set = $statement->fetchAll();
    $totalrows = count($result_set);
    //check if user email exists
    if ($totalrows == 0) {
        $response = array('success' => false, 'error' => 'no_user');
        echo(json_encode($response));
    } elseif ($totalrows == 1) {
        $result = $result_set[0];
        if ($result['pass_reset'] == 1) {
            $temp_pass_query = "SELECT temp_pass FROM temporary_passwords WHERE user_id = ?";
            $pass_statement = $db_handle->prepare($temp_pass_query);
            $pass_statement->setFetchMode(PDO::FETCH_ASSOC);
            $pass_statement->execute(array($result['id']));

            $pass_result = $pass_statement->fetchAll();
            $pass_count = count($pass_result);
            if ($pass_count == 1) {
                if ($password = $pass_result[0]['temp_pass']) {
                    //authentication successful
                    $user_data = array(
                        'user_name' => $result['name'],
                        'user_id' => $result['id'],
                        'questionnaire_complete' => $result['questionnaire_complete'],
                        'phyto_level' => $result['phyto_level']
                    );
                    $response = array('success' => true, 'user_data' => $user_data, 'change_pass' => true);
                    echo(json_encode($response));
                }
            } else if ($pass_count == 0) {
                //could not find temporary password.
            } else if ($pass_count > 1) {
                //more than one temporary password has been stored.
            }
        } else {
            $hash = $result['password'];
            //check if password is correct
            if (crypt($password, $hash) == $hash) {
                //prepare user data to return
                $user_data = array(
                    'user_name' => $result['name'],
                    'user_id' => $result['id'],
                    'questionnaire_complete' => $result['questionnaire_complete'],
                );
                //retrieve user meals if they exist
                $meal_query = "SELECT meal_id, meal_name, isoflavones_count, lignans_count FROM user_meals WHERE user_id = ?";
                $statement2 = $db_handle->prepare($meal_query);
                $statement2->setFetchMode(PDO::FETCH_ASSOC);
                $statement2->execute(array($result['id']));
                $meals_result_set = $statement2->fetchAll();
                $totalrows2 = count($meals_result_set);

                if ($totalrows2 > 0) {
                    $user_data['user_meals'] = $meals_result_set;
                } else {
                    $user_data['user_meals'] = 0;
                }
                $response = array('success' => true, 'user_data' => $user_data, 'change_pass' => false);
                echo json_encode($response);
            } else {
                $response = array('success' => false, 'error' => 'wrong_password');
                echo(json_encode($response));
            }
        }
    } else {
        $response = array('success' => false, 'error' => 'something_went_wrong');
        echo(json_encode($response));
    }

} catch (PDOException $e) {
    $response = array('success' => false, 'error' => 'PDO_exception', 'message' => $e->getMessage());
    echo(json_encode($response));
}

