<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 22/04/13
 * Time: 10:05
 * Description:
 */

include "connect.php";

$sql = "SELECT * FROM exercise_type";

try{
    $statement = $db_handle->prepare($sql);
    $statement->setFetchMode(PDO::FETCH_ASSOC);
    $statement->execute();
    $result_set = $statement->fetchAll();
    $totalrows = count($result_set);

    if($totalrows > 0){
        $response_data = array('success' => true, 'data' => $result_set);
        echo(json_encode($response_data));
    } else {
        $response_data = array('success' => false, 'error' => 'no data returned');
        echo(json_encode($response_data));
    }

} catch(PDOException $e){
    $response_data = array('success' => false, 'error' => $e->getMessage());
    echo(json_encode($response_data));
}