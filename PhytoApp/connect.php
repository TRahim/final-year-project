<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 07/01/13
 * Time: 20:28
 * To change this template use File | Settings | File Templates.
 */

/* This file connects to the PhytoApp MySQL database */

if(!isset($_SESSION)){
    session_start();
}

$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'root';
$db_name = 'phyto_app';

try {
    $db_handle = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
    $db_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
    echo $e->getMessage();
}
