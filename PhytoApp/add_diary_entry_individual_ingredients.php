<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 21/04/13
 * Time: 14:30
 * Description:
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$save_meal = $_REQUEST['save_meal'];
$ingredients = $_REQUEST['ingredients'];
$date = $_REQUEST['date'];
$meal_type = $_REQUEST['meal_type'];

if($meal_type == "breakfast"){
    $meal_type = 1;
} else if ($meal_type == "lunch"){
    $meal_type = 2;
} else if ($meal_type == "dinner"){
    $meal_type = 3;
} else if ($meal_type == "snacks_other"){
    $meal_type = 4;
}

$sql1 = "INSERT INTO user_diary_entry (user_id, date) VALUES (?,?)";
$statement1 = $db_handle->prepare($sql1);

if ($save_meal == "no"){

    $sql2 = "INSERT INTO diary_entry_individual_ingredients (diary_entry_id, ingredient_name, ingredient_phenoldb_id, ingredient_weight, meal_type, isoflavones_count, lignans_count) VALUES (?,?,?,?,?,?,?)";
    $statement2 = $db_handle->prepare($sql2);
    try{

        $statement1->execute(array($user_id, $date));
        $entry_id = $db_handle->lastInsertId();

        foreach($ingredients as $ingredient){
            $ingredient_id = $ingredient['food_id'];
            $ingredient_name = $ingredient['food_name'];
            $ingredient_weight = $ingredient['food_weight'];
            $isoflavones_count = $ingredient['isoflavones_count'];
            $lignans_count = $ingredient['lignans_count'];
            $statement2->execute(array($entry_id, $ingredient_name, $ingredient_id, $ingredient_weight, $meal_type, $isoflavones_count, $lignans_count));
        }
        $success_response = array('success' => true, 'meal_saved' => false);
        echo json_encode($success_response);
    } catch(PDOException $e){
        $fail_response = array('success' => false, 'error' => $e->getMessage());
        echo(json_encode($fail_response));
    }
} elseif($save_meal == "yes"){
    $meal_name = $_REQUEST['meal_name'];
    $total_isoflavones_count = 0;
    $total_lignans_count = 0;
    foreach($ingredients as $ingredient){
        $total_isoflavones_count += $ingredient['isoflavones_count'];
        $total_lignans_count += $ingredient['lignans_count'];
    }

    try{
        $statement1 = $db_handle->prepare($sql1);
        $statement1->execute(array($user_id, $date));

        $entry_id = $db_handle->lastInsertId();

        $sql3 = "INSERT INTO user_meals (user_id, meal_name, isoflavones_count, lignans_count) VALUES (?,?,?,?)";
        $statement2 = $db_handle -> prepare($sql3);
        $statement2 -> execute(array($user_id, $meal_name, $total_isoflavones_count, $total_lignans_count));
        $meal_id = $db_handle->lastInsertId();

        $sql4 = "INSERT INTO diary_entry_meals (entry_id, meal_type, meal_id) VALUES (?,?,?)";
        $statement3 = $db_handle->prepare($sql4);
        $statement3->execute(array($entry_id, $meal_type, $meal_id));

        $sql5 = "INSERT INTO user_meal_ingredients (meal_id, ingredient_name, ingredient_phenoldb_id, ingredient_weight, isoflavones_count, lignans_count) VALUES (?,?,?,?,?,?)";
        $statement = $db_handle -> prepare($sql5);

        foreach($ingredients as $ingredient){
            $ingredient_id = $ingredient['food_id'];
            $ingredient_name = $ingredient['food_name'];
            $ingredient_weight = $ingredient['food_weight'];
            $isoflavones_count = $ingredient['isoflavones_count'];
            $lignans_count = $ingredient['lignans_count'];

            $statement -> execute(array($meal_id, $ingredient_name, $ingredient_id, $ingredient_weight, $isoflavones_count, $lignans_count));
        }
        $success_response = array('success' => true, 'meal_saved' => true, 'meal_id' => $meal_id, 'isoflavones_count' => $total_isoflavones_count, 'lignans_count' => $total_lignans_count);
        echo json_encode($success_response);
    } catch(PDOException $e){
        $fail_response = array('success' => false, 'error' => $e->getMessage());
        echo(json_encode($fail_response));
    }
}