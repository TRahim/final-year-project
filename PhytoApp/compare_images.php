<?php

// $db_host = 'localhost';
// $db_user = 'root';
// $db_pass = 'root';
// $db_name = 'phyto_app';
// $success = FALSE;

// try {
//   $db_handle = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
//   $db_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// }
// catch(PDOException $e){
//   $e->getMessage();
// }
include "connect.php";

//limit extensions and file size to 5MB
$allowedExts = array("jpeg", "jpg", "png");
$temp = explode(".", $_FILES["file"]["name"]);
$extension = end($temp);
if ((($_FILES["file"]["type"] == "image/jpeg")
  || ($_FILES["file"]["type"] == "image/jpg")
  || ($_FILES["file"]["type"] == "image/pjpeg")
  || ($_FILES["file"]["type"] == "image/x-png")
  || ($_FILES["file"]["type"] == "image/png"))
  && ($_FILES["file"]["size"] < 5000000)
  && in_array($extension, $allowedExts))
{
  if ($_FILES["file"]["error"] > 0)
  {
    //echo "Error: " . $_FILES["file"]["error"] . "<br>";
  }
  else
  { 
    //change directory to executable location
    $dir = "/Users/taz-3456/Library/Developer/Xcode/DerivedData/OpenCV_Tutorials-bkcweuzhadfnhrhgvcpyvefssepm/Build/Products/Debug";
    chdir($dir);
    //run the command line application
    $argv = "./OpenCV\ Tutorials " . $_FILES["file"]["tmp_name"];
    $commandResult = shell_exec($argv);

    //remove the extension and any numbers in the image name
    $name = explode(".", $commandResult);
    $name = preg_replace('/[0-9]+/', '', $name);

    //search in the picture information with a similar name
    $sql = "SELECT * FROM picture_information WHERE name LIKE ?";
    try
    { 
      $query = $db_handle->prepare($sql);
      $query->setFetchMode(PDO::FETCH_ASSOC);
      $query->execute(array($name[0]));
      $results = $query->fetchAll();
      foreach($results as $row){
        $id = $row['phenol_ID'];
        $weight = $row['weight'];
      }
    }
    catch (PDOException $e) 
    {
      $e->getMessage();
    }

    $db_name = "phenol-explorer";
    $db_handle = null;
    try {
      $db_handle = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
      $db_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e){
      $e->getMessage();
    }

    //get the name of the food from the phenol id
    $sql2 = "SELECT * FROM FOODS WHERE FOOD_ID = ?";
    
    try{
      $query2 = $db_handle->prepare($sql2);
      $query2->setFetchMode(PDO::FETCH_ASSOC);
      $query2->execute(array($id));
      $results2 = $query2->fetchAll();
      foreach ($results2 as $row2) {
        $name = $row2['FOOD_NAME'];
        $success = TRUE;
      }
    }
    catch(PDOException $e){
      $e->getMessage();
    }
    $response = array("success" => $success, "weight" => $weight, "name" => $name, "id" => $id);
    echo json_encode($response);
  }
}
else
{
  $response = array("success" => $success, "path" => $_FILES["file"]["tmp_name"]);
  echo json_encode($response);
}
