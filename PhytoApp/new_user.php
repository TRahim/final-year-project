<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 07/01/13
 * Time: 20:27
 * To change this template use File | Settings | File Templates.
 */

include "connect.php";

$name = $_REQUEST['name'];
$surname = $_REQUEST['surname'];
$dob = $_REQUEST['dob'];
$email = $_REQUEST['email'];
$password = $_REQUEST['password'];

try {
    $sql = "SELECT COUNT(*) FROM users WHERE email = ?;";
    $statement = $db_handle->prepare($sql);
    $statement->execute(array($email));
    $statement->setFetchMode(PDO::FETCH_NUM);

    $result = $statement->fetch();

    if($result[0] != 0){
        $response = array('success' => false, 'error' => 'user_exists');
        echo(json_encode($response));
    } elseif ($result[0] == 0) {
        $hash = generate_hash($password);
        //create array for data to insert
        $data = array($name, $surname, $dob, $email, $hash);
        //create statement handle
        $statement_handle = $db_handle->prepare("INSERT INTO users (name, surname, dob, email, `password`) VALUES (?, ?, ?, ?, ?)");
        $statement_handle->execute($data); //run the query
        $user_id = $db_handle->lastInsertId();
        $user_data = array(
            'user_name' => $name,
            'user_id' => $user_id,
            'questionnaire_complete' => 0
        );
        $response = array('success' => true, 'user_data' => $user_data);
        echo(json_encode($response));
    } else {
        $response = array('success' => false, 'error' => 'Something went wrong');
        echo(json_encode($response));
    }

} catch (PDOException $e) {
    $response = array('success' => false, 'error' => $e->getMessage());
    echo(json_encode($response));
}

// generate_hash:
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.
function generate_hash($input, $rounds = 12)
{
    $salt = "";
    $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
    for($i=0; $i < 22; $i++) {
        $salt .= $salt_chars[array_rand($salt_chars)];
    }
    return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt);
}

?>
