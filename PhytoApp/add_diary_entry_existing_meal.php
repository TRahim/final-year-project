<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 20/04/13
 * Time: 20:03
 * Description:
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$date = $_REQUEST['date'];
$meal_id = $_REQUEST['meal_id'];
$meal_type = $_REQUEST['meal_type'];

if($meal_type == "breakfast"){
    $meal_type = 1;
} else if ($meal_type == "lunch"){
    $meal_type = 2;
} else if ($meal_type == "dinner"){
    $meal_type = 3;
} else if ($meal_type == "snacks_other"){
    $meal_type = 4;
}

$sql1 = "INSERT INTO user_diary_entry (user_id, date) VALUES (?,?)";
$sql2 = "INSERT INTO diary_entry_meals (entry_id, meal_type, meal_id) VALUES (?,?,?)";

try {
    $statement1 = $db_handle->prepare($sql1);
    $statement1->execute(array($user_id, $date));

    $entry_id = $db_handle->lastInsertId();

    $statement2 = $db_handle->prepare($sql2);
    $statement2->execute(array($entry_id, $meal_type, $meal_id));

    echo('success');
} catch(PDOException $e){
    echo($e->getMessage());
}