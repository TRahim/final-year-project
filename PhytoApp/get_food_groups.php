<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 15/03/13
 * Time: 16:02
 * Description: This script goes to the phenol-explorer database and extracts all the food data and returns what it finds
 * as JSON. The structure of the returned data is an array of food groups which contains arrays of each subgroup which
 * in turn contain an array of each type of food in that subgroup:
 *
 * [Food data array]
 *      [Food group 1]
 *          [Subgroup 1]
 *              [Food 1]
 *              [Food 2]
 *              ...
 *          [Subgroup 2]
 *          ...
 *      [Food group 2]
 *      ...
 * [end of array]
 */

include "phenol_connect.php";

try {
    $group_sql = "SELECT * FROM FOOD_GROUPS";
    $group_statement = $phenol_db->prepare($group_sql);
    $group_statement->setFetchMode(PDO::FETCH_ASSOC);
    $group_statement->execute();

    $group_result_set = $group_statement->fetchAll();
    $totalrows = count($group_result_set);
    $food_data = array(); //array for all the food data to be returned

    if ($totalrows > 0) {

       // print_r ($group_result_set);

        //echo json_encode($result_set);
        foreach($group_result_set as $food_group){


            $group_id = $food_group['FOOD_GROUP_ID'];
            //temporary array to hold all the data for each group which is added to the food_data array at the end
            $temp_group = array(
                "group_id" => $group_id,
                "group_name" => $food_group['FOOD_GROUP_NAME']
            );
            $temp_subgroups = array(); //holds all the subgroups and their data

            try {
                //get all the subgroup names and ids for the food group
                $subgroup_query = "SELECT * FROM FOOD_SUBGROUPS WHERE FOOD_GROUP_ID = ?";
                $subgroup_statement = $phenol_db->prepare($subgroup_query);
                $subgroup_statement->setFetchMode(PDO::FETCH_ASSOC);
                $subgroup_statement->execute(array($group_id));
                $subgroup_result_set = $subgroup_statement->fetchAll();
                $subgroup_totalrows = count($subgroup_result_set);

                if($subgroup_totalrows > 0){
                    foreach($subgroup_result_set as $subgroup){
                        $subgroup_id = $subgroup['FOOD_SUBGROUP_ID'];
                        $temp_subgroup = array(
                            "subgroup_id" => $subgroup_id,
                            "subgroup_name" => $subgroup['FOOD_SUBGROUP_NAME']
                        );

                        $temp_foods = array();
                        try{
                            //get all the foods of a subgroup
                            $food_query = "SELECT * FROM FOODS WHERE FOOD_SUBGROUP_ID = ?";
                            $food_statement = $phenol_db->prepare($food_query);
                            $food_statement->setFetchMode(PDO::FETCH_ASSOC);
                            $food_statement->execute(array($subgroup_id));
                            $food_result_set = $food_statement->fetchAll();
                            $food_result_totalrows = count($food_result_set);

                            if($food_result_totalrows > 0){
                                foreach($food_result_set as $food){
                                    $temp_food = array(
                                        "food_id" => $food['FOOD_ID'],
                                        "food_name" => $food['FOOD_NAME']
                                    );
                                    array_push($temp_foods, $temp_food);
                                }
                            } else {
                                echo ('food results empty');
                            }
                        } catch(PDOException $e){
                            echo $e->getMessage();
                        }
                        $temp_subgroup['subgroup_foods'] = $temp_foods;
                        array_push($temp_subgroups, $temp_subgroup);
                    }
                } else {
                    echo ('subgroup rows empty');
                }

            } catch (PDOException $e){
                echo $e->getMessage();
            }
            $temp_group['subgroups'] = $temp_subgroups;
            array_push($food_data, $temp_group);
        }
    } else {
        echo ('no data returned');
    }
    echo json_encode($food_data);
} catch (PDOException $e) {
    echo $e->getMessage();
}


