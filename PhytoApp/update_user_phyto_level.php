<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 22/04/13
 * Time: 16:15
 * Description:
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$phyto_level = $_REQUEST['phyto_level'];

$sql = "UPDATE users SET phyto_level = ? WHERE id = ?";

try{
    $statement = $db_handle->prepare($sql);
    $statement->execute(array($phyto_level, $user_id));
    echo("success");
} catch(PDOException $e){
    echo($e->getMessage());
}