<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 19/04/13
 * Time: 13:54
 * Description: This script takes a user id & a date and returns a multidimensional array of meals & ingredients for each meal.
 *              Also returns an array of any individual ingredients added to the diary which weren't saved as user meals
 *              And finally returns all exercise entered by the user for a particular day.
 *              This therefore gets all data for a particular day of the user's food diary which is stored in the database.
 *              The purpose of this is to allow the user to see previous food diary entries
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$date = $_REQUEST['date'];
//get all user meals for a diary entry
$sql1 = "SELECT * FROM user_diary_entry AS ude RIGHT JOIN diary_entry_meals AS dem ON (ude.entry_id = dem.entry_id)LEFT JOIN user_meals AS um ON (dem.meal_id = um.meal_id) WHERE ude.user_id = ? AND ude.date = ?";

try {
    $statement1 = $db_handle->prepare($sql1);
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    $statement1->execute(array($user_id, $date));

    $diary_entry_result_set = $statement1->fetchAll();
    $totalrows = count($diary_entry_result_set);

    $return_data = array();
    $meal_ids = array();
    $meal_ingredients = array();
    $response = array();
    $success = false;

    if ($totalrows > 0) {
        foreach ($diary_entry_result_set as $entry) {
            $meal_data = array(
                'meal_type' => $entry['meal_type'],
                'meal_id' => $entry['meal_id'],
                'meal_name' => $entry['meal_name'],
                'isoflavones_count' => $entry['isoflavones_count'],
                'lignans_count' => $entry['lignans_count']
            );
            array_push($return_data, $meal_data);
            //$return_data[$entry['meal_type']] = $meal_data;
            array_push($meal_ids, $entry['meal_id']);
        }
        //get all ingredients for the meals
        //because of using a variable number of integers in the IN() clause we must pad the sql with however many '?' as the size of the meal_id array
        $qMarks = str_repeat('?,', count($meal_ids) - 1) . '?';
        $sql2 = "SELECT * FROM user_meals AS um LEFT JOIN user_meal_ingredients AS umi ON (um.meal_id = umi.meal_id) WHERE umi.meal_id IN ($qMarks) ORDER BY umi.meal_id DESC";
        $statement2 = $db_handle->prepare($sql2);
        $statement2->setFetchMode(PDO::FETCH_ASSOC);
        $statement2->execute($meal_ids);
        $meal_ingredients_result_set = $statement2->fetchAll();
        //print_r($meal_ingredients_result_set);
        $totalrows2 = count($meal_ingredients_result_set);
        if ($totalrows2 > 0) {
            //print_r($meal_ingredients_result_set);
            $temp_meal_ingredients = array();
            $previous_meal_id = 0;
            $count = count($meal_ingredients_result_set);
            //build an array of meal ingredients indexed by meal_id
            foreach ($meal_ingredients_result_set as $ingredient) {
                if($count != 1){
                    if (empty($temp_meal_ingredients)) { //if temp array is empty add first ingredient (this should only happen once)
                        array_push($temp_meal_ingredients, $ingredient);
                        $previous_meal_id = $ingredient['meal_id'];
                    } else {
                        if ($previous_meal_id == $ingredient['meal_id']) { //is the current ingredient meal_id same as the previous one if yes, then add to the temp array
                            array_push($temp_meal_ingredients, $ingredient);
                        /*    if ($count == 1) { //if count == 1 this is the last element of the $ingredients_result_set array so we must add the temp ingredients to the $meal_ingredients array
                                $meal_ingredients[$previous_meal_id] = $temp_meal_ingredients;
                            }*/
                        } else if ($previous_meal_id != $ingredient['meal_id']) { //if not it means that the meal has all ingredients so add temp array to $meal_ingredients indexed by meal_id then clear temp array
                            $meal_ingredients[$previous_meal_id] = $temp_meal_ingredients;
                            $temp_meal_ingredients = array(); //reset temp array to be empty
                            array_push($temp_meal_ingredients, $ingredient);
                            $previous_meal_id = $ingredient['meal_id'];
                        } else if (isset($meal_ingredients[$ingredient['meal_id']])) {
                            //do nothing because ingredients are already in the list.
                        }
                    }
                } else {
                    $meal_ingredients[$previous_meal_id] = $temp_meal_ingredients;
                    $temp_meal_ingredients = array();
                    array_push($temp_meal_ingredients, $ingredient);
                    $meal_ingredients[$ingredient['meal_id']] = $temp_meal_ingredients;
                }
                $count--;
            }
            //print_r($meal_ingredients);
            // print_r($return_data);
            //print_r($return_data);
           // print_r($meal_ingredients);
            foreach ($return_data as $key => $meal) {
                $current_meal_id = $meal['meal_id'];
                $ingredients = array('ingredients' => $meal_ingredients[$current_meal_id]);
                $temp = array_merge($meal, $ingredients);
                $return_data[$key] = $temp;
            }
            $success = true;
            $response['meals'] = $return_data;
        } else {
            //$response = array('success' => false, 'error' => 'no_ingredients');
            //no data
        }
    } else {
        $response['meals'] = "none";
    }
    //get all individual ingredients not saved as meals
    $sql3 = "SELECT * FROM user_diary_entry AS ude RIGHT JOIN diary_entry_individual_ingredients AS deii ON (ude.entry_id = deii.diary_entry_id) WHERE ude.user_id = ? AND ude.date = ?";
    $statement3 = $db_handle->prepare($sql3);
    $statement3->setFetchMode(PDO::FETCH_ASSOC);
    $statement3->execute(array($user_id, $date));
    $individual_ingredients_result_set = $statement3->fetchAll();
    $totalrows3 = count($individual_ingredients_result_set);

    //$response = array('success' => true, 'data' => $return_data);

    if ($totalrows3 > 0) {
        $success = true;
        $response['individual_ingredients'] = $individual_ingredients_result_set;
    } else {
        $response['individual_ingredients'] = "none";
    }
    //get all exercise for a day.
    $sql4 = "SELECT * FROM user_diary_entry AS ude RIGHT JOIN diary_entry_exercise AS dee ON (ude.entry_id = dee.entry_id) LEFT JOIN exercise_type AS et ON (dee.type = et.id) WHERE ude.user_id = ? AND ude.date = ?";
    $statement4 = $db_handle->prepare($sql4);
    $statement4->setFetchMode(PDO::FETCH_ASSOC);
    $statement4->execute(array($user_id, $date));

    $exercise_result_set = $statement4->fetchAll();
    $totalrows4 = count($exercise_result_set);

    if($totalrows4 > 0){
        $success = true;
        $response['exercise'] = $exercise_result_set;
    } else {
        $response['exercise'] = "none";
    }
    $response['success'] = $success;
    echo(json_encode($response));

} catch (PDOException $e) {
    $response = array('success' => false, 'error' => $e->getMessage());
    echo(json_encode($response));
}
