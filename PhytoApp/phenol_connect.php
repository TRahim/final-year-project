<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 15/03/13
 * Time: 11:07
 * To change this template use File | Settings | File Templates.
 */

/* This file connects to the Phenol-Explorer database */

if(!isset($_SESSION)){
    session_start();
}

$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'root';
$db_name = 'phenol-explorer';

try {
    $phenol_db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
    $phenol_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch(PDOException $e){
    echo $e->getMessage();
}