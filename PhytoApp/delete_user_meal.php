<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 16/04/13
 * Time: 16:27
 * Description:
 */

include "connect.php";

$meal_id = $_REQUEST['meal_id'];

$meal_sql = "DELETE FROM user_meals WHERE meal_id = ?";
$ingredients_sql = "DELETE FROM user_meal_ingredients WHERE meal_id = ?";

try{
    $statement1 = $db_handle->prepare($meal_sql);
    $meal_delete_success = $statement1->execute(array($meal_id));
    $statement2 = $db_handle->prepare($ingredients_sql);
    $ingredients_delete_success = $statement2->execute(array($meal_id));

    if($meal_delete_success && $ingredients_delete_success){
        echo ('success');
    } else {
        echo ('fail');
    }
} catch (PDOException $e){
    echo ($e->getMessage());
}