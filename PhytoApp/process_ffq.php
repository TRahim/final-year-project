<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 20/01/13
 * Time: 13:19
 * Description: Inserts all food frequency questionnaire data into the database and then updates flag in users table to indicate that the user has completed the questionnaire.
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$data = $_REQUEST['data'];

$ffq_data_table = array('id' => 0, 'user_id ' => $user_id);
$ffq_other_foods_data = array();
$ffq_milk_breakfast_data = array();
$ffq_vitamins_supplements_data = array();
$ffq_food_preparation_data = array();


foreach($data as $key => $page_data){
    if($key == "ffq_page_12"){
        $ffq_other_foods_data = $page_data;
    } else if ($key == "ffq_page13"){
        if(!isset($page_data['cereal_brand_1'])){
            $page_data['cereal_brand_1'] = 'none';
        }
        if(!isset($page_data['cereal_type_1'])){
            $page_data['cereal_type_1'] = 'none';
        }
        if(!isset($page_data['cereal_brand_2'])){
            $page_data['cereal_brand_2'] = 'none';
        }
        if(!isset($page_data['cereal_type_2'])){
            $page_data['cereal_type_2'] = 'none';
        }
        $ffq_milk_breakfast_data = $page_data;
    } else if ( $key == "ffq_page14"){
        if($page_data['which_fat_frying'] == "vegetable_oil"){
            $ffq_food_preparation_data['which_fat_frying'] = $page_data['which_veg_oil'];
        } else {
            $ffq_food_preparation_data['which_fat_frying'] = $page_data['which_fat_frying'];
        }
        if($page_data['which_fat_baking'] == "margarine"){
            $ffq_food_preparation_data['which_fat_baking'] = $page_data['which_margarine_type'];
        } else {
            $ffq_food_preparation_data['which_fat_baking'] = $page_data['which_fat_frying'];
        }
        $ffq_food_preparation_data['how_often_fried_home'] = $page_data['how_often_fried_home'];
        $ffq_food_preparation_data['how_often_fried_away'] = $page_data['how_often_fried_away'];
        $ffq_food_preparation_data['visible_fat'] = $page_data['visible_fat'];
        $ffq_food_preparation_data['how_often_grilled_roast_meat'] = $page_data['how_often_grilled_roast_meat'];
        $ffq_food_preparation_data['grilled_roast_meat_how_well_cooked'] = $page_data['grilled_roast_meat_how_well_cooked'];
        $ffq_food_preparation_data['how_often_add_salt_cooking'] = $page_data['how_often_add_salt_cooking'];
        $ffq_food_preparation_data['how_often_add_salt_table'] = $page_data['how_often_add_salt_table'];
        if($page_data['salt_sub_yes_no'] == "yes"){
            $ffq_food_preparation_data['salt_sub_yes_no'] = $page_data['which_salt_sub'];
        } else {
            $ffq_food_preparation_data['salt_sub_yes_no'] = $page_data['salt_sub_yes_no'];
        }
    } else if ($key == "ffq_page15"){
        $ffq_vitamins_supplements_data = $page_data;
    } else {
        foreach($page_data as $key => $val){
            $ffq_data_table[$key] = $val;
        }

    }

}

try{

    $qMarks = str_repeat('?,', count($ffq_data_table) -1) . '?'; //creates a string of question marks equal to the number of input values for the query
    $ffq_data_sql = "INSERT INTO ffq_data VALUES ($qMarks)";
    $statement1 = $db_handle->prepare($ffq_data_sql);
    $statement1->execute(array_values($ffq_data_table));

    echo('statement 1 successful. ');

    $entry_id = $db_handle->lastInsertId();

    $ffq_milk_breakfast_data['ffq_entry_id'] = $entry_id;
    $ffq_milk_breakfast_data['id'] = 0;
    $ffq_food_preparation_data['ffq_entry_id'] = $entry_id;
    $ffq_food_preparation_data['id'] = 0;



    $milk_breakfast_sql = "INSERT INTO ffq_milk_breakfast (which_milk, how_much_milk, cereal_brand_1, cereal_type_1, cereal_brand_2, cereal_type_2, ffq_entry_id, id) VALUES (?,?,?,?,?,?,?,?)";
    $food_prep_sql = "INSERT INTO ffq_food_preparation (which_fat_frying, which_fat_baking, how_often_fried_home, how_often_fried_away, visible_fat, how_often_grilled_roast_meat, grilled_roast_meat_how_well_cooked, how_often_add_salt_cooking, how_often_add_salt_table, salt_sub_yes_no, ffq_entry_id, id) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

    $statement2 = $db_handle->prepare($milk_breakfast_sql);
    //print_r($ffq_milk_breakfast_data);
    $statement2->execute(array_values($ffq_milk_breakfast_data));

    echo('statement 2 successful. ');

    $statement3 = $db_handle->prepare($food_prep_sql);
    //print_r($ffq_food_preparation_data);
    $statement3->execute(array_values($ffq_food_preparation_data));

    echo('statement 3 successful. ');

    $other_foods_sql = "INSERT INTO ffq_other_foods (id, ffq_entry_id, food_name, serving_size, frequency) VALUES (?,?,?,?,?)";
    $statement4 = $db_handle->prepare($other_foods_sql);

    echo('statement 4 successful. ');

    foreach($ffq_other_foods_data as $food){
        $statement4->execute(array(0,$entry_id, $food['food_name'], $food['serving_size'], $food['frequency']));
    }

    $supplements_sql = "INSERT INTO ffq_vitamins_supplements (id, ffq_entry_id, supplement_name, supplement_brand, supplement_strength, supplement_dose, supplement_frequency) VALUES (?,?,?,?,?,?,?)";
    $statement5 = $db_handle->prepare($supplements_sql);

    echo('statement 5 successful. ');

    foreach($ffq_vitamins_supplements_data as $vitamin){
        $statement5->execute(array(0,$entry_id, $vitamin['supplement_name'], $vitamin['supplement_brand'], $vitamin['supplement_strength'], $vitamin['supplement_dose'], $vitamin['supplement_frequency']));
    }

    $update_user_table_sql = "UPDATE users SET questionnaire_complete = 1 WHERE user_id = ?";
    $statement6 = $db_handle->prepare($update_user_table_sql);
    $statement6->execute(array($user_id));

} catch(PDOException $e){
    echo $e->getMessage();
}
