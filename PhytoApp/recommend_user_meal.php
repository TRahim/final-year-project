<?php

include "connect.php";

date_default_timezone_set('Europe/London');
$polyphenol_levels = $_REQUEST['polyphenol_levels'];
//$polyphenol_levels = array("isoflavones_count" => 10, "lignans_count" => 20);
$time = date("H:i:s");
$today = strtotime($time);
$morningStart = strtotime("07:00:00");
$morningEnd = strtotime("10:00:00");
$lunchStart = strtotime("12:00:00");
$lunchEnd = strtotime("14:00:00");
$dinnerStart = strtotime("18:00:00");
$dinnerEnd = strtotime("22:00:00");
$type = "S";
//levels varying from 0-4 with 
//0 being 0 lignans and isoflavones
//1 being 0 - 0.15 lignans and 0 - 1 isoflavones
//2 being 0.15 - 0.3 lignans and 1 - 16.3 isoflavones
//3 being 0.3+ lignans and 16.3+ isoflavones
$lignanLevel = 3;
$isoLevel = 3;
$response = array();
$success = false; 

if($polyphenol_levels["isoflavones_count"] >= 16.3){
	$isoLevel = 3;
} else if ($polyphenol_levels["isoflavones_count"] < 16.3 && $polyphenol_levels["isoflavones_count"] > 1){
	$isoLevel = 2;
} else if ($polyphenol_levels["isoflavones_count"] <= 1 && $polyphenol_levels["isoflavones_count"] > 0){
	$isoLevel = 1;
} else {
	$isoLevel = 0;

}

if($polyphenol_levels['lignans_count'] >= 0.3){
	$lignanLevel  = 3;
} else if ($polyphenol_levels['lignans_count'] < 0.3 && $polyphenol_levels['lignans_count'] > 0.15){
	$lignanLevel  = 2;
} else if ($polyphenol_levels['lignans_count'] <= 0.15 && $polyphenol_levels['lignans_count'] > 0){
	$lignanLevel  = 1;
}else {
	$lignanLevel  = 0;
}

if($today >= $morningStart && $today <= $morningEnd){
	$type = "M";
}
if($today >= $lunchStart && $today <= $lunchEnd){
	$type = "L";
}
if($today >= $dinnerStart && $today <= $dinnerEnd){
	$type = "D";
}


if($isoLevel === 3 && $lignanLevel === 3){
	//if the users diet is fine then return false
	$response = array("success" => false);
	echo json_encode($response);
}else{
	//search for type and Phytoestrogen levels that match in the table 
	$sql1 = "SELECT * FROM recommended_meal_name WHERE meal_type = ? AND lignans_level = ? AND isoflavones_level = ?";
	$statement1 = $db_handle->prepare($sql1);
	$statement1->setFetchMode(PDO::FETCH_ASSOC);
	$statement1->execute(array($type, $lignanLevel, $isoLevel));

	$results1 = $statement1->fetchAll();

	foreach ($results1 as $row) {
		$id = $row['meal_id'];
		$name = $row['meal_name'];
	}

	//search the meal id through for all ingredients 
	$sql2 = "SELECT * FROM recommended_meal WHERE meal_id = ?";
	$statement2 = $db_handle->prepare($sql2);
	$statement2->setFetchMode(PDO::FETCH_ASSOC);
	$statement2->execute(array($id));

	$results2 = $statement2->fetchAll();


	foreach ($results2 as $row) {
    	//store the weights and the ids 
		$phenolDetails[] = array("id" => $row['phenol_id'], "weight" => $row['weight']);	
	}

	//change the database to the phenol-explorer
	$db_name = "phenol-explorer";
	$db_user = "root";
	$db_pass = 'root';
	$db_handle = null;
	try {
		$db_handle = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
		$db_handle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e){
		$e->getMessage();
	}

	$phenol = array();

	$sql3 = "SELECT MEAN FROM COMPOSITION_TABLE WHERE COMPOUND_ID IN (393, 394, 395, 396, 397, 398, 399, 400) AND FOOD_ID = ? ";
    //This query selects foods which contain lignans
	$sql4 = "SELECT MEAN FROM COMPOSITION_TABLE WHERE COMPOUND_ID IN (595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629) AND FOOD_ID = ?";
	$sql5 = "SELECT * FROM FOODS WHERE FOOD_ID = ?";

	$isoflavones_total = 0;
	$lignans_total = 0;

	//$ingredient_name = array();

	//add up all the mean values and save the food names and weight.
	foreach($phenolDetails as $id){
		//echo $id["id"];
		$statement3 = $db_handle->prepare($sql3);
		$statement3->setFetchMode(PDO::FETCH_ASSOC);
		$statement3->execute(array($id["id"]));
		$isoflavone_result_set = $statement3->fetchAll();
		$totalrows1 = count($isoflavone_result_set);
		

		$response_data = array();

		if ($totalrows1 > 0) {
			foreach ($isoflavone_result_set as $row) {
				$isoflavones_total += $row['MEAN'];
			}
			$response_data['isoflavones_total'] = $isoflavones_total;
		} else {
			$response_data['isoflavones_total'] = $isoflavones_total;
		}

		$statement4 = $db_handle->prepare($sql4);
		$statement4->setFetchMode(PDO::FETCH_ASSOC);
		$statement4->execute(array($id["id"]));

		$lignans_result_set = $statement4->fetchAll();
		$totalrows2 = count($lignans_result_set);
		

		if($totalrows2 > 0) {
			foreach($lignans_result_set as $row){
				$lignans_total += $row['MEAN'];
			}
			$response_data['lignans_total'] = $lignans_total;
		} else {
			$response_data['lignans_total'] = $lignans_total;
		}

		$statement5 = $db_handle->prepare($sql5);
		$statement5->setFetchMode(PDO::FETCH_ASSOC);
		$statement5->execute(array($id["id"]));
		$name_results = $statement5->fetchAll();

		foreach ($name_results as $row) {
			$response_data['name'] = $row['FOOD_NAME'];
		}

		$response_data["weight"] = $id["weight"];
		$response_data["id"] = $id["id"];
		$phenol[] = $response_data;
		$success = true;
	}
	
	$response = array("success" => $success, "phenol" => $phenol, "meal_name" => $name, "type" =>$type);
	//print_r($phenol);
	echo json_encode($response);
}
