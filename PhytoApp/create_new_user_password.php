<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 24/04/13
 * Time: 16:27
 * Description: When a user wants to reset their password this script will generate a new hash and update the users
 *              table with the new hash value and set the pass_reset flag to false. It will then remove the record from
 *              the temporary passwords table which was associated with this user.
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$new_pass = $_REQUEST['new_pass'];

$sql1 = "UPDATE users SET password = ?, pass_reset = 0  WHERE id = ?";
$statement1 = $db_handle->prepare($sql1);

$sql2 = "DELETE FROM temporary_passwords WHERE user_id = ?";
$statement2 = $db_handle->prepare($sql2);

$hash = generate_hash($new_pass);

try{
    $statement1->execute(array($hash , $user_id));
    $statement2->execute(array($user_id));
    echo('success');
} catch (PDOException $e){
    echo $e->getMessage();
}


// generate_hash:
// Original PHP code by Chirp Internet: www.chirp.com.au
// Please acknowledge use of this code by including this header.
function generate_hash($input, $rounds = 12)
{
    $salt = "";
    $salt_chars = array_merge(range('A','Z'), range('a','z'), range(0,9));
    for($i=0; $i < 22; $i++) {
        $salt .= $salt_chars[array_rand($salt_chars)];
    }
    return crypt($input, sprintf('$2a$%02d$', $rounds) . $salt);
}