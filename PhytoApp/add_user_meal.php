<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 12/04/13
 * Time: 14:15
 * Description:
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$meal_name = $_REQUEST['meal_name'];
$ingredients = $_REQUEST['ingredients'];
$polyphenol_count = $_REQUEST['polyphenol_count'];

try {
    $sql1 = "INSERT INTO user_meals (user_id, meal_name, isoflavones_count, lignans_count) VALUES (?,?,?,?)";
    $statement1 = $db_handle -> prepare($sql1);
    $statement1 -> execute(array($user_id, $meal_name, $polyphenol_count['isoflavones_count'], $polyphenol_count['lignans_count']));

    $meal_id = $db_handle->lastInsertId();
    $sql2 = "INSERT INTO user_meal_ingredients (meal_id, ingredient_name, ingredient_phenoldb_id, ingredient_weight, isoflavones_count, lignans_count) VALUES (?,?,?,?,?,?)";
    $statement2 = $db_handle -> prepare($sql2);
    foreach($ingredients as $ingredient){
        $ingredient_id = $ingredient['food_id'];
        $ingredient_name = $ingredient['food_name'];
        $ingredient_weight = $ingredient['food_weight'];
        $isoflavones_count = $ingredient['isoflavones_count'];
        $lignans_count = $ingredient['lignans_count'];

        $statement2 -> execute(array($meal_id, $ingredient_name, $ingredient_id, $ingredient_weight, $isoflavones_count, $lignans_count));
    }
    $success_response = array('success' => true, 'meal_id' => $meal_id);
    echo json_encode($success_response);
} catch(PDOException $e){
    $fail_response = array('success' => false, 'error' => $e -> getMessage());
    echo json_encode($fail_response);
}
