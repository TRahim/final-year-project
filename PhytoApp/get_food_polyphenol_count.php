<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 15/04/13
 * Time: 11:52
 * Description: Searches the database for the food id provided and returns a total polyphenol content count.
 *              Note that totals will be calculated using the mean content value for each polyphenol available.
 */

include "phenol_connect.php";

$food_id = $_REQUEST['food_id'];

try {
    //Search for ALL polyhpenol content - uncomment in necessary
    //$sql = "SELECT MEAN FROM COMPOSITION_TABLE WHERE FOOD_ID = ?";

    //The query below will search the table only for isoflavones.
    $sql1 = "SELECT MEAN FROM COMPOSITION_TABLE WHERE COMPOUND_ID IN (393, 394, 395, 396, 397, 398, 399, 400) AND FOOD_ID = ? ";
    //This query selects foods which contain lignans
    $sql2 = "SELECT MEAN FROM COMPOSITION_TABLE WHERE COMPOUND_ID IN (595,596,597,598,599,600,601,602,603,604,605,606,607,608,609,610,611,612,613,614,615,616,617,618,619,620,621,622,623,624,625,626,627,628,629) AND FOOD_ID = ?";

    $statement1 = $phenol_db->prepare($sql1);
    $statement1->setFetchMode(PDO::FETCH_ASSOC);
    $statement1->execute(array($food_id));

    $isoflavone_result_set = $statement1->fetchAll();
    $totalrows1 = count($isoflavone_result_set);
    $isoflavones_total = 0;

    $response_data = array();

    if ($totalrows1 > 0) {
        foreach ($isoflavone_result_set as $row) {
            $isoflavones_total += $row['MEAN'];
        }
        $response_data['isoflavones_total'] = $isoflavones_total;
    } else {
        $response_data['isoflavones_total'] = $isoflavones_total;
    }

    $statement2 = $phenol_db->prepare($sql2);
    $statement2->setFetchMode(PDO::FETCH_ASSOC);
    $statement2->execute(array($food_id));

    $lignans_result_set = $statement2->fetchAll();
    $totalrows2 = count($lignans_result_set);
    $lignans_total = 0;

    if($totalrows2 > 0) {
        foreach($lignans_result_set as $row){
            $lignans_total += $row['MEAN'];
        }
        $response_data['lignans_total'] = $lignans_total;
    } else {
        $response_data['lignans_total'] = $lignans_total;
    }
    $response_data['success'] = true;
    echo(json_encode($response_data));

} catch (PDOException $e) {
    $response_data['success'] = false;
    $response_data['error'] = $e->getMessage();
    echo(json_encode($response_data));
}


