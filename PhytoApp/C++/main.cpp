#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <string>

using namespace std;
using namespace cv;

/** @function main */
int main( int argc, char** argv )
{
    Mat src_base, hsv_base;
    
    /// Load at least one image
    if( argc < 2 )
    { printf("Error not enough images\n");
        return -1;
    }
    
    src_base = imread( argv[1], 1 );
    
    /// Convert to HSV
    cvtColor( src_base, hsv_base, CV_BGR2HSV );
    
    
    /// Using 50 bins for hue
    int h_bins = 50;
    int histSize[] = { h_bins};
    
    // hue varies from 0 to 256
    float h_ranges[] = { 0, 256 };

    const float* ranges[] = { h_ranges };
    
    // Use the o-th channel
    int channels[] = { 0 };
    
    /// Histograms
    MatND hist_base;
    
    /// Calculate the histograms for the HSV images
    calcHist( &hsv_base, 1, channels, Mat(), hist_base, 1, histSize, ranges, true, false );
    normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );
    
    Mat src_rest, hsv_rest;
    MatND hist_rest;
    
    /// create a vector to store all source paths
    vector<string> src;
    
    string line;
    //open up the foods file
    ifstream myfile ("/Users/taz-3456/Desktop/foods.txt");
    if (myfile.is_open())
    {
        /// for every line in the file push to the end of the vector
        while ( getline (myfile,line) )
        {
            src.push_back(line);
        }
        myfile.close();
    }
    else cout << "Unable to open file";
    
    /// work out the length of the vector
    long arrLength = src.size();

    float temp = 1;
    string best = "";
    
    //For every image compare with the user image
    for(int i = 0;i<arrLength;i++){
        
        src_rest = imread(src[i],1);
        cvtColor(src_rest, hsv_rest, COLOR_BGR2HSV);
        calcHist(&hsv_rest, 1, channels, Mat(), hist_rest, 1, histSize, ranges, true, false);
        normalize(hist_rest, hist_rest, 0, 1, NORM_MINMAX, -1, Mat());
        double baseComp = compareHist( hist_base, hist_rest, 3 );
        //cout<<"Main picture compared to "<<src[i]<<" bhattacharyya "<<baseComp<<'\n';
        
        //simple if condition to get the lowest positive integer
        if(baseComp >= 0 && baseComp<temp){
            temp = baseComp;
            best = src[i];
        }
    }
    
    //if the best is a a good enough match it is printed
    if (temp > 0.35){
        cout<<"No close matches found";
        printf("\n");
    }else{
        cout<<best;
        printf("\n");
    }
    return 0;
}