<?php 

include "connect.php";

$user_id = $_REQUEST['user_id'];
$meal_name = $_REQUEST['meal_name'];
$food_details = $_REQUEST['food_details'];
$polyphenol_count = $_REQUEST['polyphenol_count'];

try
{
    //add the data values into the user meals table
	$sql1 = "INSERT INTO user_meals (user_id, meal_name, isoflavones_count, lignans_count) VALUES (?,?,?,?)";
	$statement1 = $db_handle -> prepare($sql1);
    $statement1 -> execute(array($user_id, $meal_name, $polyphenol_count['isoflavones_count'], $polyphenol_count['lignans_count']));

    $meal_id = $db_handle->lastInsertId();
    //insert ingredients of the meal
    $sql2 = "INSERT INTO user_meal_ingredients (meal_id, ingredient_name, ingredient_phenoldb_id, ingredient_weight, isoflavones_count, lignans_count) VALUES (?,?,?,?,?,?)";
    $statement2 = $db_handle -> prepare($sql2);
    $statement2 -> execute(array($meal_id, $food_details['name'], $food_details['id'], $food_details['weight'], $polyphenol_count['isoflavones_count'], $polyphenol_count['lignans_count']));
    $success_response = array('success' => true, 'meal_id' => $meal_id);
    echo json_encode($success_response);
}
catch(PDOException $e){
    $fail_response = array('success' => false, 'error' => $e -> getMessage());
    echo json_encode($fail_response);
}