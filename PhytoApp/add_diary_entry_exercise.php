<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 22/04/13
 * Time: 11:12
 * Description:
 */

include "connect.php";

$user_id = $_REQUEST['user_id'];
$date = $_REQUEST['date'];
$type = $_REQUEST['type'];
$intensity = $_REQUEST['intensity'];
$duration = $_REQUEST['duration'];

$sql1 = "INSERT INTO user_diary_entry (user_id, date) VALUES (?,?)";
$sql2 = "INSERT INTO diary_entry_exercise (entry_id, type, duration, intensity) VALUES (?,?,?,?)";

try{
    $statement1 = $db_handle->prepare($sql1);
    $statement1->execute(array($user_id, $date));
    $entry_id = $db_handle->lastInsertId();

    $statement2 = $db_handle->prepare($sql2);
    $statement2->execute(array($entry_id, $type, $duration, $intensity));
    echo('success');
} catch(PDOException $e){
    echo($e->getMessage());
}