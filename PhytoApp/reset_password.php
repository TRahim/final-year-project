<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 24/04/13
 * Time: 14:50
 * Description: This script generates a hash out of the user's email address then sets a flag in the database to say theyre resetting their password
 *              It then sends the has to their email for them to input as their password. After that the user will be prompted to create a password
 *              The new password will then be crytographically hashed again properly.
 */

include "connect.php";

$email = $_REQUEST['email'];

$hash = sha1($email); //create random hash for user as new password
$hash = substr($hash, 1, 8);

//check for user in the database
$sql1 = "SELECT id FROM users WHERE email = ?";
$statement1 = $db_handle->prepare($sql1);
$statement1->setFetchMode(PDO::FETCH_ASSOC);

//set flag for password reset and replace old password hash with a blank string
$sql2 = "UPDATE users SET pass_reset = 1, password = '' WHERE id = ?";

//store hash value and user id in temporary passwords table so user can log in and create a new proper one.
$sql3 = "INSERT INTO temporary_passwords (user_id, temp_pass) VALUES (?,?)";
try{
    $statement1->execute(array($email));
    $result_set1 = $statement1->fetchAll();
    $totalrows = count($result_set1);

    if($totalrows == 1){
        $user_id = $result_set1[0]['id'];

        $statement2 = $db_handle->prepare($sql2);
        $statement2->execute(array($user_id));

        $statement3 = $db_handle->prepare($sql3);
        $statement3->execute(array($user_id, $hash));

        mail($email, 'Phyto-APP Password Reset', 'Hello, please log in to the application using this temporary password: ' . $hash);

        $response = array('success' => true);
        echo(json_encode($response));
    } else if ($totalrows > 1) {
        $response = array('success' => false, 'error' => 'duplicate_email');
        echo(json_encode($response));
    } else if ($totalrows == 0){
        $response = array('success' => false, 'error' => 'email_does_not_exist');
        echo(json_encode($response));
    }
} catch(PDOException $e){
    $response = array('success' => false, 'error' => $e->getMessage());
    echo(json_encode($response));
}

