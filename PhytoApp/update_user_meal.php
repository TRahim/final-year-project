<?php
/**
 * Created by JetBrains PhpStorm.
 * User: James
 * Date: 17/04/13
 * Time: 13:06
 * Description:
 */

include "connect.php";

$meal_id = $_REQUEST['meal_id'];
$meal_name = $_REQUEST['meal_name'];
if (isset($_REQUEST['new_ingredients'])) {
    $new_ingredients = $_REQUEST['new_ingredients'];
}
$poly_count = $_REQUEST['new_poly_count'];
$sql1 = "";
$sql2 = "INSERT INTO user_meal_ingredients (meal_id, ingredient_name, ingredient_phenoldb_id, ingredient_weight, isoflavones_count, lignans_count) VALUES (?,?,?,?,?, ?)";

try {

    if ($meal_name != "no_change") {
        $sql1 .= "UPDATE user_meals SET meal_name = ?, isoflavones_count = ?, lignans_count = ? WHERE meal_id = ?";
        $statement1 = $db_handle->prepare($sql1);
        $statement1->execute(array($meal_name, $poly_count['isoflavones_count'], $poly_count['lignans_count'], $meal_id));
    } else {
        $sql1 .= "UPDATE user_meals SET isoflavones_count = ?, lignans_count = ? WHERE meal_id = ?";
        $statement1 = $db_handle->prepare($sql1);
        $statement1->execute(array($poly_count['isoflavones_count'], $poly_count['lignans_count'], $meal_id));
    }
    if (!empty($new_ingredients)) {
        $statement2 = $db_handle->prepare($sql2);
        foreach ($new_ingredients as $ingredient) {
            $ingredient_id = $ingredient['food_id'];
            $ingredient_name = $ingredient['food_name'];
            $ingredient_weight = $ingredient['food_weight'];
            $isoflavones_count = $ingredient['isoflavones_count'];
            $lignans_count = $ingredient['lignans_count'];

            $statement2->execute(array($meal_id, $ingredient_name, $ingredient_id, $ingredient_weight, $isoflavones_count, $lignans_count));
        }
    }
    $success_response = array('success' => true);
    echo json_encode($success_response);
} catch (PDOException $e) {
    $fail_response = array('success' => false, 'error' => $e->getMessage());
    echo json_encode($fail_response);
}


