<?php

include "connect.php";

$user_id = $_REQUEST['user_id'];
$date = $_REQUEST['date'];

// $user_id = "7";
// $date = "2014-05-18";

//change to unix timestamp
$currentDay = strtotime($date);
$dayArray = array();
$secDayArray = array();
$val = "";
for ($i = 0 ; $i < 7 ; $i++) {
	$val = date('Y-m-d', $currentDay);
	$currentDay -= 24 * 3600;
	$dayArray[] = $val; 
	//$secDayArray[] = 
}
$in  = str_repeat('?,', count($dayArray) - 1) . '?';
$secDayArray = $dayArray;
/*
 SQL alternative: SELECT SUM(isoflavones_count) AS isoflavones_count, SUM(lignans_count) AS lignans_count, `date` FROM user_diary_entry AS ude RIGHT JOIN diary_entry_meals AS dem ON (ude.entry_id = dem.entry_id)LEFT JOIN user_meals AS um ON (dem.meal_id = um.meal_id) WHERE ude.date IN ("2014-02-27","2014-02-26","2014-02-25") AND ude.user_id = 1 GROUP BY date
*/

 $sql1 = "SELECT * FROM user_diary_entry AS ude RIGHT JOIN diary_entry_meals AS dem ON (ude.entry_id = dem.entry_id)LEFT JOIN user_meals AS um ON (dem.meal_id = um.meal_id) WHERE ude.date IN ($in) AND ude.user_id = ?";

 try{
 	$statement1 = $db_handle->prepare($sql1);
 	$dayArray[] = $user_id;
 	$statement1->setFetchMode(PDO::FETCH_ASSOC);
 	$statement1->execute($dayArray);

 	$diaryResults = $statement1->fetchAll();

 	$rows = count($diaryResults);
 	$response = array();
 	$success = false;

 	$grouped = array();
        //Loop created to add up everything for 1 day for every day.
 	foreach($diaryResults as $row) 
    {
        if(!array_key_exists($row['date'], $grouped))
        {   // create array key if it doesnt exist
            $grouped[$row['date']] = array('isoflavones_count' => 0, 'lignans_count' => 0);
        }
   	    $grouped[$row['date']]['isoflavones_count'] += $row['isoflavones_count']; // sum for each date
	    $grouped[$row['date']]['lignans_count'] += $row['lignans_count'];
    }

    $newArray = "";
    foreach ($secDayArray as $array) 
    {
        if (key_exists($array, $grouped)) 
        {
            //check if iso and/or lig is set else set the value to 0 or something else
            if (key_exists("isoflavones_count", $grouped[$array])) 
            {
                $iso = $grouped[$array]["isoflavones_count"];
            } 
            else 
            {
                $iso = 0;
            }

            if (key_exists("lignans_count", $grouped[$array])) 
            {
                $lig = $grouped[$array]["lignans_count"];
            } 
            else 
            {
                $lig = 0;
            }
            $newArray[$array] = array("isoflavones_count" => $iso, "lignans_count" => $lig);
        } 
        else 
        {
            $newArray[$array] = array("isoflavones_count" => 0, "lignans_count" => 0);
        }
    }

  $sql2 = "SELECT * FROM user_diary_entry AS ude RIGHT JOIN diary_entry_individual_ingredients as dei ON (ude.entry_id = dei.diary_entry_id) WHERE ude.date IN ($in) and ude.user_id = ?";

  $statement2 =$db_handle->prepare($sql2);
  $statement2->setFetchMode(PDO::FETCH_ASSOC);
  $statement2->execute($dayArray);

  $individualResults = $statement2->fetchAll();

  $repl = array_replace($newArray, $grouped);
  $keys = array();
  foreach($repl as $key=>$val){
    $keys[]=array("date"=>$key,
      "isoflavones_count"=>$val['isoflavones_count'],
      "lignans_count"=>$val['lignans_count']);
}

//add individual results to data so not only meal data is calculated
foreach($individualResults as $row2)
{   
    foreach($keys as $key2=>$val2)
    {   
        if($row2["date"] === $val2["date"])
        {   
            $val2["isoflavones_count"] += $row2['isoflavones_count'];
            $val2["lignans_count"] += $row2['lignans_count'];
            $keys[$key2]["isoflavones_count"] = $val2["isoflavones_count"];
            $keys[$key2]["lignans_count"] = $val2["lignans_count"]; 
        }
    }
}

array_push($response, $keys);
$success = true;

$falseArray = array();
for($i = 0; $i < 7; $i++){
    $falseArray[] = array("date"=>$secDayArray[$i],
        "isoflavones_count"=>0,
        "lignans_count"=>0);
}
if($success === false){
    array_push($response, $falseArray);
}
$response['success'] = $success;
    // var_dump($response);
echo(json_encode($response));
}catch (PDOException $e) {
	$response = array('success' => false, 'error' => $e->getMessage());
	echo(json_encode($response));
}

