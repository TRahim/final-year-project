<!DOCTYPE html>
<html>
<head>
    <title>PhytoApp</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" href="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.css" rel="stylesheet"/>
    <link type="text/css" href="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.min.css" rel="stylesheet" />
    <link type="text/css" href="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.css" rel="stylesheet" />
    <link type="text/css" href="css/mobile_styles.css" rel="stylesheet"/>
</head>
<body class="mobile_body">
<div class="overlay"></div>
<!-- HOME PAGE -->
<div data-role="page" id="home_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li data-theme="b"><a href="#home_page">Home</a></li>
            <li><a href="#register_page">Register</a></li>
            <li><a href="#login_page">Login</a></li>
            <li><a href="#contact_page">Contact</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <!-- PAGE CONTENT -->
    <div data-role="content">
        <h1 class="centre">PhytoApp</h1>

        <h2 class="centre">About</h2>

        <p>Modification of behavioural factors such as alcohol consumption, diet and exercise may offer potential for
            improving long-term outcomes after breast cancer.</p>

        <p>Data from the DietCompLyf Study, comprising of 3,400 breast cancer patients recruited from 56 hospitals
            across the UK has shown that approximately half of all breast cancer patients replace, or omit, foodstuffs
            that they had previously consumed pre-diagnosis.</p>

        <p>This project aims to record and modulate breast cancer patient behaviour in terms of their patterns of
            exercise and their consumption of Phytoestrogens.</p>
        <br>

        <h2 class="centre">Learn More</h2>

        <div class="centre">
            <iframe width=300 height=200 src="http://www.youtube.com/embed/ckKzbg5ddmY?rel=0" frameborder="0"
                    allowfullscreen></iframe>
        </div>
    </div>
</div>
<!-- END HOMEPAGE -->

<!-- REGISTER -->
<div data-role="page" id="register_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu2">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#home_page">Home</a></li>
            <li data-theme="b"><a href="#register_page">Register</a></li>
            <li><a href="#login_page">Login</a></li>
            <li><a href="#contact_page">Contact</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu2" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <!-- PAGE CONTENT -->
    <div data-role="content">
        <h1 class="centre">Registration</h1>

        <p>Your personal information will be kept completely confidential.</p>
        <div id="register_validation_error" class="validation_error"></div>

        <form>
            <fieldset data-role="fieldcontain">
                <legend><h3>Personal Information</h3></legend>
                <label for="name">Name: </label>
                <input id="name" placeholder="Name" name="name" type="text"/>
                <label for="surname">Surname: </label>
                <input id="surname" placeholder="Surname" name="surname" type="text"/>
                <label for="dob_day">Birth Day: </label>
                <input id="dob_day" placeholder="Day" type="text" maxlength="2" />
                <label for="dob_month">Birth Month: </label>
                <input id="dob_month" placeholder="Month" type="text" maxlength="2" />
                <label for="dob_year">Birth Year: </label>
                <input id="dob_year" placeholder="Year" type="text" maxlength="4" />
            </fieldset>

            <fieldset data-role="fieldcontain">
                <legend><h3>User Account Details</h3></legend>
                <label for="email">E-mail: </label>
                <input id="email" placeholder="Email" name="email" type="email"/>
                <label for="password">Password: </label>
                <input id="password" placeholder="Password" name="password" type="password"/>
                <label for="password2">Confirm Password: </label>
                <input id="password2" placeholder="Confirm Password" name="password2" type="password"/>
            </fieldset>
        </form>
        <input id="register_submit" type="submit" value="Submit" data-theme="b" data-icon="check"/>
    </div>
</div>
<!-- END REGISTER PAGE -->

<div data-role="page" id="change_password_page">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <div data-role="content">
        <h1>Create New Password</h1>
        <form>
            <div id="new_password_error_message"></div>
            <fieldset data-role="fieldcontain">
                <label for="new_password">New Password: </label>
                <input type="text" placeholder="New Password" id="new_password" />
                <label for="new_password_confirm">Confirm New Password: </label>
                <input type="text" placeholder="Confirm New Password" id="new_password_confirm" />
            </fieldset>
            <div data-role="button" data-icon="check" data-theme="b" id="new_password_submit">Confirm</div>
        </form>
    </div>
</div>

<div data-role="page" id="questionnaire_dialog">

    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu2">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#home_page">Home</a></li>
            <li data-theme="b"><a href="#register_page">Register</a></li>
            <li><a href="#login_page">Login</a></li>
            <li><a href="#contact_page">Contact</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu2" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <div data-role="content">
        <p>Would you like to complete the Food Frequency Questionnaire which will allow us to recommend you meals
            tailored to your eating habits?</p>
        <a href="#questionnaire_page" data-role="button" data-theme="b">Yes Please</a>
        <a href="#dashboard_page" data-role="button" data-theme="a">Later</a>
    </div>
</div>

<!-- LOGIN PAGE -->
<div data-role="page" id="login_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu3">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#home_page">Home</a></li>
            <li><a href="#register_page">Register</a></li>
            <li data-theme="b"><a href="#login_page">Login</a></li>
            <li><a href="#contact_page">Contact</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu3" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <!-- PAGE CONTENT -->
    <div data-role="content">
        <h1 class="centre">Login</h1>
        <form>
            <fieldset data-role="fieldcontain">
                <legend><h3>Enter email and password</h3></legend>
                <div class="validation_error" id="login_error_message"></div>
                <label for="login_email">Email: </label>
                <input type="text" placeholder="Email" id="login_email" name="login_email"/>
                <label for="login_password">Password: </label>
                <input type="password" placeholder="Password" id="login_password" name="login_password"/>
            </fieldset>
        </form>
        <input id="login_submit" type="submit" value="Submit" data-theme="b" data-icon="check"/>

        <div id="password_reset_error_message"></div>
        <p>If you have forgotten your password please type your email in above press the button below to reset it.</p>

        <div data-role="button" data-theme="e" data-icon="refresh" id="password_reset_btn">Reset Password</div>
    </div>
</div>
<!-- END LOGIN PAGE -->

<!-- CONTACT PAGE -->
<div data-role="page" id="contact_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu4">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#home_page">Home</a></li>
            <li><a href="#register_page">Register</a></li>
            <li><a href="#login_page">Login</a></li>
            <li data-theme="b"><a href="#contact_page">Contact</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu4" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <!-- PAGE CONTENT -->
    <div data-role="content">
        <h1 class="centre">Contact Us</h1>

        <p>If you have any queries regarding this research application then please do not hesitate to contact us.</p>

        <p>You can send us an E-mail directly with the form below or if you would prefer another method a list of other
            contact details are provided for your convenience</p>

        <form action="contact.php" method="POST">
            <fieldset data-role="fieldcontain">
                <legend><h3>Please enter your message and contact email</h3></legend>
                <label for="contact_us_message">Your Message: </label>
                <textarea id="contact_us_message" placeholder="Your Message" name="contact_us_message"></textarea>
                <label for="contact_us_user_email">Your contact E-mail: </label>
                <input type="text" id="contact_us_user_email" placeholder="Your Email" name="contact_us_user_email"/>
            </fieldset>
            <input id="contact_submit" type="submit" value="Submit" data-theme="b" data-icon="check"/>
        </form>

        <div id="contact_details">
            <p>

            <h2 class="centre">University of Westmisnter School Of Life Sciences</h2></p>
            <div id="map_canvas">Map goes here</div>
            <h3>Address: </h3>

            <p>University Of Wesminster</br>115 New Cavendish Street</br>London</br>W1W 6XH</p>
        </div>
    </div>
</div>
<!-- END CONTACT PAGE -->

<!-- /////////////// THESE PAGES ONLY ACCESSIBLE AFTER LOGIN \\\\\\\\\\\\\\\\\\ -->

<!-- DASHBOARD PAGE -->
<div data-role="page" id="dashboard_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu5">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li data-theme="b"><a href="#dashboard_page">Dashboard</a></li>
            <li><a href="#diet_page" class="diet_btn">Diet</a></li>
            <li><a href="#diary_page">Diary</a></li>
            <li><a href="#upload_pic_page">Upload Picture</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu5" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout" data-icon="gear">Log Out</a>
    </div>

    <!-- CONTENT -->
    <div data-role="content">
        <h1 class="centre">Dashboard</h1>
        <hr>

        <div id="dashboard_greeting"></div>

        <div class="centre">
            <div id="dashboard_total_isoflavone_count"><p>Total Isoflavones consumption on this day: </p><span></span></div>
            <div id="dashboard_total_lignans_count"><p>Total Lignans consumption on this day: </p><span></span></div>
        </div>
        <hr>
        <p>Create & customize meals for your diet in the 'Diet' section.</p>
        <p>Use the 'Diary' to input your meals and exercise on a daily basis.</p>

        <div class="dashboard-nav">
            <ul data-role="listview" data-inset="true">
                <li><a href="#diet_page">Your Diet</a></li>
                <li><a href="#diary_page">Your Diary</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- END DASHBOARD PAGE -->

<!-- Diary PAGE -->
<div data-role="page" id="diary_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu7">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#dashboard_page">Dashboard</a></li>
            <li><a href="#diet_page" class="diet_btn">Diet</a></li>
            <li data-theme="b"><a href="#diary_page">Diary</a></li>
            <li><a href="#upload_pic_page">Upload Picture</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu7" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout" data-icon="gear">Log Out</a>
    </div>

    <!-- CONTENT -->
    <div data-role="content">
        <h1 class="centre">Diary</h1>
        <div id="calendar_stuff">
            <div data-role="controlgroup" data-type="horizontal" id="calendar_controls" class="centre">
                <div data-role="button" data-icon="arrow-l" data-iconpos="notext" data-theme="c" data-inline="true" id="calendar_back_btn"></div>
                <div data-role="button" data-icon="grid" data-theme="c" data-inline="true" id="open_calendar_btn">Open Calendar</div>
                <div data-role="button" data-icon="arrow-r" data-iconpos="notext" data-theme="c" data-inline="true" id="calendar_forward_btn"></div>
            </div>
            <input name="date_box_input" style="display:none" id="date_box_input" type="text" data-role="datebox" data-options='{"mode":"calbox", "themeDateToday": "b", "centerHoriz": true, "useFocus":true, "enablePopup":true, "useNewStyle":true}' />
            <div id="diary_total_isoflavone_count"><p>Total Isoflavones consumption on this day: </p><span></span></div>
            <div id="diary_total_lignans_count"><p>Total Lignans consumption on this day: </p><span></span></div>
            <a href="#isoChart" data-rel="popup" data-role="button" data-inline="true" id="isoPopUp">View Isoflavone graph</a>
            <div id="isoChart" style="margin:0 auto; margin-left:auto; margin-right:auto; align:center; text-align:center;" data-role="popup" data-overlay-theme="a"></div>
            <a href="#ligChart" data-rel="popup" data-role="button" data-inline="true" id="ligPopUp">View Lignan graph</a>
            <div id="ligChart" style="margin:0 auto; margin-left:auto; margin-right:auto; align:center; text-align:center;" data-role="popup" data-overlay-theme="a"></div>
            <a href="#recommendPopUp" data-rel="popup" data-role="button" data-inline="true" id="recommendBtn">Recommend me a meal</a>
            <div id="recommendPopUp" data-role="popup" data-overlay-theme="a">
                <div class="ui-corner-top ui-header ui-bar-a" data-theme="b" data-role="header" role="banner">
                    <h1 class="ui-title" role="heading" aria-level="1">Recommended food</h1>
                </div>
                <div id="recommendContent"></div>
            </div>
        </div>

        </div>
        <div id="diary_food_list">
            <div id="diary_breakfast">
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider">Breakfast</li>
                    <li class="diary_nothing_selected" id="diary_breakfast_list_empty">You currently have nothing listed.</li>
                </ul>
                <div data-role="button" id="diary_breakfast_add_item" data-entry-type="breakfast" data-icon="plus" data-mini="true" class="diary_add_item">Add Breakfast Item</div>
            </div>
            <div id="diary_lunch">
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider">Lunch</li>
                    <li class="diary_nothing_selected" id="diary_lunch_list_empty">You currently have nothing listed.</li>
                </ul>
                <div data-role="button" id="diary_lunch_add_item" data-entry-type="lunch" data-icon="plus" data-mini="true" class="diary_add_item">Add Lunch Item</div>
            </div>
            <div id="diary_dinner">
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider">Dinner</li>
                    <li class="diary_nothing_selected" id="diary_dinner_list_empty">You currently have nothing listed.</li>
                </ul>
                <div data-role="button" id="diary_dinner_add_item" data-entry-type="dinner" data-icon="plus" data-mini="true" class="diary_add_item">Add Dinner Item</div>
            </div>
            <div id="diary_snacks_other">
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider">Snacks / anything else</li>
                    <li class="diary_nothing_selected" id="diary_snacks_list_empty">You currently have nothing listed.</li>
                </ul>
                <div data-role="button" id="diary_snacks_add_item" data-entry-type="snacks_other" data-icon="plus" data-mini="true" class="diary_add_item">Add Snack Item</div>
            </div>
            <div id="diary_exercise">
                <ul data-role="listview" data-inset="true">
                    <li data-role="list-divider">Exercise</li>
                    <li class="diary_nothing_selected" id="diary_exercise_list_empty">You currently have nothing listed.</li>
                </ul>
                <div data-role="button" id="diary_exercise_add_item" data-entry-type="exercise" data-icon="plus" data-mini="true">Add Exercise</div>
            </div>
        </div>
    </div>
</div>

<!-- DIARY ADD FOOD ENTRY PAGE -->
<div data-role="page" id="add_diary_entry_page" data-add-back-btn="true">

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <div data-role="content">
        <h1 class="centre">Add To Diary</h1>
        <p>Choose either from a predefined meal or individual ingredients</p>
        <div>
            <h3 class="centre">Predefined meals</h3>
            <ul id="diary_meal_list" data-role="listview" data-inset="true">
                <li data-role="list-divider">Your Meals</li>
                <li class="list_empty" id="diary_meal_list_empty">You currently have no custom meals.</li>
            </ul>
        </div>
        <div id="diary_entry_polyphenol_count"></div>
        <div id="diary_ingredients_to_add">
            <ul data-role="listview" data-inset="true">
                <li data-role="list-divider">Items to add</li>
                <li class="list_empty" id="diary_ingredients_list_empty">You currently have no items selected</li>
            </ul>
            <label for="diary_save_meal_yes_no">Save these ingredients as a meal for later use?</label>
            <div>
                <div id="diary_save_meal_error_message"></div>
                <select name="diary_save_meal_yes_no" id="diary_save_meal_yes_no" data-role="slider">
                    <option value="no">No</option>
                    <option value="yes">Yes</option>
                </select>
                <label for="diary_save_meal_name">Meal Name: </label>
                <input type="text" placeholder="Meal Name" id="diary_save_meal_name" disabled="true" />
            </div>
            <div data-role="button" id="diary_add_ingredients_btn" data-icon="check" data-theme="b">Confirm items to add</div>
        </div>

        <h3 class="centre">Select Ingredients</h3>
        <p>Firstly select a weight then choose an ingredient either by searching or from the list of categories.</p>
        <div id="diary_entry_select_quantities" data-role="collapsible" data-inset="false">
            <h4>Weight</h4>
            <p>Please input the precise figure in the text field in grams or mililitres.</p>
            <div id="diary_ingredients_weight_error_message"></div>
            <label for="diary_entry_ingredient_weight">Weight <strong>(g / ml)</strong>: </label>
            <input type="text" placeholder="Weight" id="diary_entry_ingredient_weight" />
        </div>
        <div>
            <div data-role="collapsible" id="diary_entry_search_collapsible" data-inset="false">
                <h3>Search</h3>
                <label for="diary_entry_search"><h4>Search by name:</h4></label>
                <input type="text" placeholder="Search" id="diary_entry_search"/>

                <div id="diary_entry_search_btn" data-role="button" data-icon="search" data-theme="b">Search</div>

                <div id="diary_search_results"></div>
            </div>

            <div id="diary_food_selection_groups" data-role="collapsible" data-inset="false">
                <h3>Categories</h3>

                <div id="diary_food_groups" data-role="collapsible-set">
                </div>
            </div>
        </div>
    </div>
</div>

<!-- DIARY ADD EXERCISE PAGE -->
<div data-role="page" id="diary_add_exercise_page" data-add-back-btn="true">
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <div data-role="content">
        <h1 class="centre">Exercise</h1>
        <div id="exercise_validation_message"></div>
        <fieldset data-role="fieldcontain">
            <label for="select_exercise_list">Select exercise type:</label>
            <select id="select_exercise_list"></select>
            <label for="exercise_intensity">Intesity: </label>
            <select id="exercise_intensity">
                <option value="no_choice"></option>
                <option value="Low">Low</option>
                <option value="Moderate">Moderate</option>
                <option value="High">High</option>
            </select>
            <label for="exercise_duration">Duration in minutes: </label>
            <input type="text" placeholder="Duration" id="exercise_duration" />
            <p>(eg. 1 minute and 30 seconds = 1.30)</p>
        </fieldset>
        <div data-role="button" data-icon="check" data-theme="b" id="diary_add_exercise_confirm_btn">Confirm</div>
        <div data-role="button" data-icon="delete" data-theme="a" id="diary_add_exercise_cancel_btn">Cancel</div>
    </div>
</div>

<!-- DIET PAGE -->
<div data-role="page" id="diet_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu6">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#dashboard_page">Dashboard</a></li>
            <li data-theme="b"><a href="#diet_page" class="diet_btn">Diet</a></li>
            <li><a href="#diary_page">Diary</a></li>
            <li><a href="#upload_pic_page">Upload Picture</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu6" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout" data-icon="gear">Log Out</a>
    </div>

    <!-- CONTENT -->
    <div data-role="content">
        <h1 class="centre">Diet</h1>
        <div id="diet_questionnaire_message"></div>

            <div>
                <ul id="user_meal_list" data-role="listview" data-inset="true">
                    <li data-role="list-divider">Your Meals</li>
                    <li id="user_meal_list_empty">You currently have no custom meals.</li>
                </ul>
            </div>
            <a href="#" id="add_meal_btn" data-role="button" data-icon="plus">Add Meal</a>
    </div>
</div>
<!-- END DIET PAGE -->

<!-- CREATE MEAL PAGE -->
<div data-role="page" id="create_meal_page" data-add-back-btn="true">

    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    </div>

    <div data-role="content">
        <h1 class="centre">Create Meal</h1>

        <p>To add ingredients to a meal first input the weight and then you can either search by name or select manually from the list
            of categories beneath.</p>

        <div id="meal_isoflavone_count"><p>Total meal Isoflavones content: </p><span></span></div>
        <div id="meal_lignans_count"><p>Total meal Lignans content: </p><span></span></div>

        <div id="custom_meal_ingredients">
            <ul data-role="listview" data-inset="true" divider-theme="b" data-split-icon="red-cross"  data-split-theme="a">
                <li data-role="list-divider">Ingredients</li>
                <li id="add_meal_list_empty" class="none_selected">You have no ingredients selected</li>
            </ul>
        </div>

        <fieldset data-role="fieldcontain">
            <label for="create_meal_name_input">Meal Name: </label>
            <input type="text" placeholder="Meal Name" id="create_meal_name_input" />
            <div data-role="button" id="create_meal_save_btn" data-icon="plus" data-theme="b">Save Meal</div>
            <div data-role="button" id="update_meal_btn" data-icon="check" data-theme="e" data-meal-to-update="">Update Meal</div>
            <div data-role="button" id="delete_meal_btn" data-icon="delete" data-theme="a" data-meal-to-delete="">Delete Meal</div>
        </fieldset>

        <h3 class="centre">Select Ingredients</h3>
        <div id="create_meal_select_quantities" data-role="collapsible" data-inset="false">
            <h4>Weight</h4>
            <div id="add_meal_weight_error_message"></div>
            <p>Please input the precise figure in the text field in grams or mililitres.</p>
            <label for="create_meal_ingredient_weight">Weight <strong>(g / ml)</strong>: </label>
            <input type="text" placeholder="Weight" id="create_meal_ingredient_weight" />
        </div>
        <div>
            <div data-role="collapsible" id="create_meal_search_collapsible" data-inset="false">
                <h3>Search</h3>
                <label for="create_meal_search"><h4>Search by name:</h4></label>
                <input type="text" placeholder="Search" id="create_meal_search"/>

                <div id="create_meal_search_btn" data-role="button" data-icon="search" data-theme="b">Search</div>

                <div id="meal_search_results"></div>
            </div>

            <div id="food_selection_groups" data-role="collapsible" data-inset="false">
                <h3>Categories</h3>

                <div id="food_groups" data-role="collapsible-set">
                </div>
            </div>
        </div>

    </div>
</div>

<div data-role="page" id="confirm_remove_ingredient_dialog">
    <div data-role="header"><h1>Are you sure?</h1></div>

    <div data-role="content">
        <h1>Remove Item?</h1>
        <p>Are you sure you wish to remove this ingredient from the list?</p>
        <div class="centre">
            <a id="ingredients_confirm_delete" data-role="button" data-theme="b" data-icon="check" data-inline="true" data-mini="true">Yes</a>
            <a id="ingredients_cancel_delete" data-role="button" data-theme="a" data-icon="delete" data-inline="true" data-mini="true">No</a>
        </div>
    </div>
</div>


<!-- FOOD FREQUENCY QUESTIONNAIRE PAGE -->
<div data-role="page" id="questionnaire_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu5">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li data-theme="b"><a href="#dashboard">Dashboard</a></li>
            <li><a href="#diet_page">Diet</a></li>
            <li><a href="#diary_page">Diary</a></li>
            <li><a href="#upload_pic_page">Upload Picture</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-fixed="true">
        <a href="#panel_menu5" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Food Frequency Questionnaire</h1>
        <p>Please complete each section listed below and then press submit.</p>
        <div class="ffq_validation_message"></div>

        <div id="ffq_section_list">
            <ul data-role="listview" data-inset="true">
                <li><a href="#ffq_page1">Meat & Fish</a></li>
                <li><a href="#ffq_page2">Bread & Savoury Biscuits</a></li>
                <li><a href="#ffq_page3">Cereals</a></li>
                <li><a href="#ffq_page4">Potatoes, Rice & Pasta</a></li>
                <li><a href="#ffq_page5">Dairy Products & Fats</a></li>
                <li><a href="#ffq_page6">Butter & Margarine</a></li>
                <li><a href="#ffq_page7">Sweets & Snacks</a></li>
                <li><a href="#ffq_page8">Soups, Sauces & Spreads</a></li>
                <li><a href="#ffq_page9">Drinks</a></li>
                <li><a href="#ffq_page10">Fruit</a></li>
                <li><a href="#ffq_page11">Vegetables</a></li>
                <li><a href="#ffq_page12">Other Foods</a></li>
                <li><a href="#ffq_page13">Milk & Breakfast</a></li>
                <li><a href="#ffq_page14">Food Preparation</a></li>
                <li><a href="#ffq_page15">Vitamins & Supplements</a></li>
            </ul>
        </div>

        <div data-role="button" id="ffq_complete_submit" data-icon="check" data-theme="b">Submit</div>
    </div>
</div>
<!-- END FOOD FREQUENCY QUESTIONNAIRE PAGE -->

<div data-role="page" id="ffq_page1" data-add-back-btn="true">
<!-- HEADER -->
<div data-role="header" class="header" data-id="header" data-position="fixed">
    <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
</div>

<div data-role="content">
<h1 class="centre">Meat & Fish</h1>

<p>All are medium sized portions unless stated otherwise</p>

<p>All fields are required unless stated otherwise.</p>

<p><strong>When all fields are complete please press the "confirm" button</strong></p>

<div class="ffq_validation_message"></div>

<form class="ffq_form">
<fieldset data-role="fieldcontain"> <!-- MEAT & FISH -->
<label for="beef_roast_etc">Beef: roast, steak, mince, stew or casserole</label>
<select id="beef_roast_etc">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="beefburgers">Beefburgers</label>
<select id="beefburgers">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="pork_roast_etc">Pork: roast, chops, stew or slices</label>
<select id="pork_roast_etc">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="lamb_roast_etc">Lamb: roast, chops, or stew</label>
<select id="lamb_roast_etc">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="chicken_other_poultry">Chicken or other poultry (eg. Turkey)</label>
<select id="chicken_other_poultry">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="bacon">Bacon</label>
<select id="bacon">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="ham">Ham</label>
<select id="ham">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="cornedbeef_etc">Corned Beef, Spam, luncheon meats</label>
<select id="cornedbeef_etc">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sausages">Sausages</label>
<select id="sausages">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="savoury_pies">Savoury Pies (eg. meat pie, pork pie, pasties, sausages rolls)</label>
<select id="savoury_pies">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="liver">Liver, liver pate, liver sausage</label>
<select id="liver">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="fried_fish_batter">Fried Fish in batter</label>
<select id="fried_fish_batter">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="fish_fingers">Fish fingers, Fish cakes</label>
<select id="fish_fingers">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="other_white_fish">Other white fish, fresh or frozen (eg. Cod, Haddock, etc)</label>
<select id="other_white_fish">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="oily_fish">Oily fish, fresh or canned (eg. Mackerel, kippers, tuna, etc)</label>
<select id="oily_fish">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="shellfish">Shellfish</label>
<select id="shellfish">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="fish_roe_taramasalata">Fish roe, Taramasalata</label>
<select id="fish_roe_taramasalata">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>
</fieldset>
<div class="ffq_confirm" data-icon="check" id="ffq1_confirm" data-page-id="ffq_page1" data-role="button" data-theme="b">Confirm</div>
<div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page1" data-theme="a">Cancel</div>
</form>
</div>
</div>

<div data-role="page" id="ffq_page2" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Bread & Savoury Biscuits</h1>

        <p>All portions are one slice or biscuit unless stated otherwise</p>

        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>

        <div class="ffq_validation_message"></div>

        <form class="ffq_form">
            <fieldset data-role="fieldcontain">
                <label for="white_bread">White bread & rolls</label>
                <select id="white_bread">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="brown_bread">Brown bread & rolls</label>
                <select id="brown_bread">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="wholemeal_bread">Wholemeal bread & rolls</label>
                <select id="wholemeal_bread">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="cream_crackers">Cream crackers, cheese biscuits</label>
                <select id="cream_crackers">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="crispbread">Crispbread (eg. Ryvita)</label>
                <select id="crispbread">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>
            </fieldset>


            <div class="ffq_confirm" data-icon="check" id="ffq2_confirm" data-page-id="ffq_page2" data-role="button" data-theme="b">Confirm</div>
            <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page2" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page3" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">

        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Cereals</h1>

        <p>All portions are one bowl unless stated otherwise.</p>

        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>

        <div class="ffq_validation_message"></div>

        <fieldset data-role="fieldcontain">
            <label for="porridge">Porridge, Readybrek</label>
            <select id="porridge">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="cereal">Breakfast Cereal</label>
            <select id="cereal">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>
        </fieldset>
        <form class="ffq_form">
            <div class="ffq_confirm" data-icon="check" id="ffq3_confirm" data-page-id="ffq_page3" data-role="button" data-theme="b">Confirm</div>
            <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page3" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page4" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Potatoes, Rice & Pasta</h1>

        <p>All portions are a medium serving unless stated otherwise.</p>

        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>

        <div class="ffq_validation_message"></div>

        <form class="ffq_form">

            <fieldset data-role="fieldcontain">

                <label for="boiled_mashed_potatoes">Boiled, mashed, instant or jacket potatoes</label>
                <select id="boiled_mashed_potatoes">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="chips">Chips</label>
                <select id="chips">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="roast_potatoes">Roast Potatoes</label>
                <select id="roast_potatoes">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="potato_salad">Potato salad</label>
                <select id="potato_salad">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="white_rice">White Rice</label>
                <select id="white_rice">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="brown_rice">Brown Rice</label>
                <select id="brown_rice">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="white_green_pasta">White or green Pasta (eg. Spaghetti, macaroni, etc)</label>
                <select id="white_green_pasta">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="wholemeal_pasta">Wholemeal Pasta</label>
                <select id="wholemeal_pasta">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="lasagna">Lasagna, Moussaka</label>
                <select id="lasagna">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="pizza">Pizza</label>
                <select id="pizza">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>
            </fieldset>
            <div class="ffq_confirm" data-icon="check" id="ffq4_confirm" data-page-id="ffq_page4" data-role="button" data-theme="b">Confirm</div>
            <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page4" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page5" data-add-back-btn="true">
<!-- HEADER -->
<div data-role="header" class="header" data-id="header" data-position="fixed">
    <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
</div>

<div data-role="content">
    <h1 class="centre">Dairy Prodcuts & Fats</h1>

    <p>All fields are required unless stated otherwise.</p>

    <p><strong>When all fields are complete please press the "confirm" button</strong></p>

    <div class="ffq_validation_message"></div>

    <form class="ffq_form">
        <fieldset data-role="fieldcontain">

            <label for="single_sour_cream">Single or Sour Cream (tablespoon)</label>
            <select id="single_sour_cream">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="double_clotted_cream">Double or Clotted Cream (tablespoon)</label>
            <select id="double_clotted_cream">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="low_fat_yoghurt">Low fat Yoghurt, Fromage Frais (125g carton)</label>
            <select id="low_fat_yoghurt">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="full_fat_yoghurt">Full fat or Greek Yoghurt (125g carton)</label>
            <select id="full_fat_yoghurt">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="dairy_desserts">Dairy Desserts (125g carton)</label>
            <select id="dairy_desserts">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="cheese">Cheese eg. Cheddar, Brie (medium serving)</label>
            <select id="cheese">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="cottage_cheese">Cottage Cheese, low fat soft cheese (medium serving)</label>
            <select id="cottage_cheese">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="eggs">Eggs, boiled, fried, scrambled, etc (one)</label>
            <select id="eggs">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="quiche">Quiche (medium serving)</label>
            <select id="quiche">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="low_salad_cream">Low Calorie/Fat Salad Cream (tablespoon)</label>
            <select id="low_salad_cream">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="salad_cream">Salad Cream, Mayonnaise (tablespoon)</label>
            <select id="salad_cream">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="french_dressing">French Dressing (tablespoon)</label>
            <select id="french_dressing">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>

            <label for="other_dressing">Other Salad Dressing(tablespoon)</label>
            <select id="other_dressing">
                <option value="no_choice"></option>
                <option value="0">Never/less than once a month</option>
                <option value="1">1-3 per month</option>
                <option value="2">Once a week</option>
                <option value="3">2-4 per week</option>
                <option value="4">5-6 per week</option>
                <option value="5">Once a day</option>
                <option value="6">2-3 per day</option>
                <option value="7">4-5 per day</option>
                <option value="8">6+ per day</option>
            </select>
        </fieldset>
        <div class="ffq_confirm" data-icon="check" id="ffq5_confirm" data-page-id="ffq_page5" data-role="button" data-theme="b">Confirm</div>
        <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page5" data-theme="a">Cancel</div>
    </form>
</div>
</div>

<div data-role="page" id="ffq_page6" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Butter & Margarine</h1>

        <p>The following items on bread or vegetables.</p>

        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button.</strong></p>

        <div class="ffq_validation_message"></div>

        <form class="ffq_form">
            <fieldset data-role="fieldcontain">

                <label for="butter">Butter (teaspoon)</label>
                <select id="butter">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="block_margarine">Block Margarine (teaspoon)</label>
                <select id="block_margarine">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="polyunsaturated_margarine">Polyunsaturated Margarine (tub) eg. Flora, sunflower
                    (teaspoon)</label>
                <select id="polyunsaturated_margarine">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="other_margarine">Other Soft Margarine Spreads (tub) eg. Blue Band, Clover
                    (teaspoon)</label>
                <select id="other_margarine">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="low_fat_spread">Low Fat Spread (tub) eg. Outline, Gold (teaspoon)</label>
                <select id="low_fat_spread">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="very_low_fat_spread">Very Low Fat Spread (tub)(teaspoon)</label>
                <select id="very_low_fat_spread">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>
            </fieldset>
            <div class="ffq_confirm" data-icon="check" id="ffq6_confirm" data-page-id="ffq_page6" data-role="button" data-theme="b">Confirm</div>
            <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page6" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page7" data-add-back-btn="true">
<!-- HEADER -->
<div data-role="header" class="header" data-id="header" data-position="fixed">
    <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
</div>

<div data-role="content">
<h1 class="centre">Sweets & Snacks</h1>

<p>All portions are a medium serving unless stated otherwise</p>

<p>All fields are required unless stated otherwise.</p>

<p><strong>When all fields are complete please press the "confirm" button</strong></p>

<div class="ffq_validation_message"></div>

<form class="ffq_form">
<fieldset data-role="fieldcontain">

<label for="biscuits_chocolate">Sweet biscuits - Chocolate eg. Digestives (one)</label>
<select id="biscuits_chocolate">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="biscuits_plain">Sweet biscuits - Plain eg. Nice, ginger, etc (one)</label>
<select id="biscuits_plain">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="cakes_homemade">Cakes eg. Fruit, Sponge - Home Baked</label>
<select id="cakes_homemade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="cakes_readymade">Cakes eg. Fruit, Sponge - Ready Made</label>
<select id="cakes_readymade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="buns_homemade">Buns, Pastries eg. Scones, Flapjacks - Home Baked</label>
<select id="buns_homemade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="buns_readymade">Buns, Pastries eg. Scones, Flapjacks - Ready Made</label>
<select id="buns_readymade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="fruit_pie_homemade">Fruit Pies, Tarts, Crumbles - Home Made</label>
<select id="fruit_pie_homemade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="fruit_pie_readymade">Fruit Pies, Tarts, Crumbles - Ready Made</label>
<select id="fruit_pie_readymade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sponge_puddings_homemade">Sponge Puddings - Home Made</label>
<select id="sponge_puddings_homemade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sponge_puddings_readymade">Sponge Puddings - Ready Made</label>
<select id="sponge_puddings_readymade">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="milk_puddings">Milk Puddings eg. Rice, Custard, Trifle</label>
<select id="milk_puddings">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="ice_cream">Ice Cream, Choc Ices</label>
<select id="ice_cream">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="chocolates_single">Chocolates - Single or Squares</label>
<select id="chocolates_single">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="chocolate_bars">Chocolate Bars eg. Mars, Crunchie</label>
<select id="chocolate_bars">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sweets">Sweets, Toffees, Mints</label>
<select id="sweets">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sugar_added">Sugar Added to Tea, Coffee or Cereal (teaspoon)</label>
<select id="sugar_added">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="crisps">Crisps or Other Packet Snacks eg. Wotsits</label>
<select id="crisps">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="nuts">Peanuts or Other Nuts</label>
<select id="nuts">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>
</fieldset>
<div class="ffq_confirm" data-icon="check" id="ffq7_confirm" data-page-id="ffq_page7" data-role="button" data-theme="b">Confirm</div>
<div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page7" data-theme="a">Cancel</div>
</form>
</div>
</div>

<div data-role="page" id="ffq_page8" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Soups, Sauces & Spreads</h1>

        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>

        <div class="ffq_validation_message"></div>

        <form class="ffq_form">
            <fieldset data-role="fieldcontain">

                <label for="vegetable_soup">Vegetable Soups (bowl)</label>
                <select id="vegetable_soup">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="meat_soup">Meat Soups (bowl)</label>
                <select id="meat_soup">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="sauces">Sauces eg White Sauce, Cheese Sauce, Gravy (tablespoon)</label>
                <select id="sauces">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="tomato_ketchup">Tomato Ketchup (tablespoon)</label>
                <select id="tomato_ketchup">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="pickles_chutney">Pickles, Chutney (tablespoon)</label>
                <select id="pickles_chutney">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="marmite_bovril">Marmite, Bovril (teaspoon)</label>
                <select id="marmite_bovril">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="jam_marmalade_honey">Jam, Marmalade, Honey (teaspoon)</label>
                <select id="jam_marmalade_honey">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="peanut_butter">Peanut Butter (teaspoon)</label>
                <select id="peanut_butter">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>
            </fieldset>
            <div class="ffq_confirm" data-icon="check" id="ffq8_confirm" data-page-id="ffq_page8" data-role="button" data-theme="b">Confirm</div>
            <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page8" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page9" data-add-back-btn="true">
<!-- HEADER -->
<div data-role="header" class="header" data-id="header" data-position="fixed">
    <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
</div>

<div data-role="content">
<h1 class="centre">Drinks</h1>

<p>All fields are required unless stated otherwise.</p>

<p><strong>When all fields are complete please press the "confirm" button.</strong></p>

<div class="ffq_validation_message"></div>

<form class="ffq_form">
    <fieldset data-role="fieldcontain">
        <label for="tea">Tea (cup)</label>
        <select id="tea">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="coffee_instant">Coffee - Instant or Ground (cup)</label>
        <select id="coffee_instant">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="coffee_decaf">Coffee - Decaffeinated (cup)</label>
        <select id="coffee_decaf">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="coffee_whitener">Coffee Whitener eg. Coffee-mate (teaspoon)</label>
        <select id="coffee_whitener">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="cocoa">Cocoa, Hot Chocolate (cup)</label>
        <select id="cocoa">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="horlicks">Horlicks, Ovaltine (cup)</label>
        <select id="horlicks">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="wine">Wine (glass)</label>
        <select id="wine">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="beer">Beer, Lager, Cider (half pint)</label>
        <select id="beer">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="port_sherry_vermouth">Port, Sherry, Vermouth, Liqueurs (glass)</label>
        <select id="port_sherry_vermouth">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="spirits">Spirits eg. Gin, Brandy, Whiskey, Vodka (single)</label>
        <select id="spirits">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="diet_fizzy_drinks">Low Calorie or Diet Fizzy Soft Drinks (glass)</label>
        <select id="diet_fizzy_drinks">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="fizzy_drinks">Fizzy Soft Drinks eg. Cola, Lemonade etc. (glass)</label>
        <select id="fizzy_drinks">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="pure_fruit_juice">Pure Fruit Juice (glass)</label>
        <select id="pure_fruit_juice">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>

        <label for="squash">Fruit Squash or Cordial (glass)</label>
        <select id="squash">
            <option value="no_choice"></option>
            <option value="0">Never/less than once a month</option>
            <option value="1">1-3 per month</option>
            <option value="2">Once a week</option>
            <option value="3">2-4 per week</option>
            <option value="4">5-6 per week</option>
            <option value="5">Once a day</option>
            <option value="6">2-3 per day</option>
            <option value="7">4-5 per day</option>
            <option value="8">6+ per day</option>
        </select>
    </fieldset>
    <div class="ffq_confirm" data-icon="check" id="ffq9_confirm" data-page-id="ffq_page9" data-role="button" data-theme="b">Confirm</div>
    <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page9" data-theme="a">Cancel</div>
</form>
</div>
</div>

<div data-role="page" id="ffq_page10" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Fruit</h1>

        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>

        <div class="ffq_validation_message"></div>

        <form class="ffq_form">
            <fieldset data-role="fieldcontain">

                <label for="apples">Apples (1 fruit)</label>
                <select id="apples">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="pears">Pears (1 fruit)</label>
                <select id="pears">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="oranges">Oranges, Satsumas, Mandarins (1 fruit)</label>
                <select id="oranges">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="grapefruit">Grapefruit (half)</label>
                <select id="grapefruit">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="bananas">Bananas (1 fruit)</label>
                <select id="bananas">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="grapes">Grapes (medium serving)</label>
                <select id="grapes">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="melons">Melons (1 slice)</label>
                <select id="melons">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="peaches_plums_apricots">Peaches, Plums, Apricots (1 fruit)</label>
                <select id="peaches_plums_apricots">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="strawberries_raspberries_kiwis">Strawberries, Raspberries, Kiwi Fruit (medium
                    serving)</label>
                <select id="strawberries_raspberries_kiwis">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="tinned_fruit">Tinned Fruit (medium serving)</label>
                <select id="tinned_fruit">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>

                <label for="dried_fruit">Dried Fruit eg. Raisins, Prunes (medium serving)</label>
                <select id="dried_fruit">
                    <option value="no_choice"></option>
                    <option value="0">Never/less than once a month</option>
                    <option value="1">1-3 per month</option>
                    <option value="2">Once a week</option>
                    <option value="3">2-4 per week</option>
                    <option value="4">5-6 per week</option>
                    <option value="5">Once a day</option>
                    <option value="6">2-3 per day</option>
                    <option value="7">4-5 per day</option>
                    <option value="8">6+ per day</option>
                </select>
            </fieldset>
            <div class="ffq_confirm" data-icon="check" id="ffq10_confirm" data-page-id="ffq_page10" data-role="button" data-theme="b">Confirm</div>
            <div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page10" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page11" data-add-back-btn="true">
<!-- HEADER -->
<div data-role="header" class="header" data-id="header" data-position="fixed">
    <img class="mobile_logo" src="img/westminster-logo-white.png"/>
    <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
</div>

<div data-role="content">
<h1 class="centre">Vegetables - Fresh, Frozen or Tinned</h1>

<p>All portions are a medium serving unless stated otherwise.</p>

<p>All fields are required unless stated otherwise.</p>

<p><strong>When all fields are complete please press the "confirm" button</strong></p>

<div class="ffq_validation_message"></div>

<form class="ffq_form">
<fieldset data-role="fieldcontain">

<label for="carrots">Carrots</label>
<select id="carrots">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="spinach">Spinach</label>
<select id="spinach">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="broccoli">Broccoli, Spring Greens, Kale</label>
<select id="broccoli">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sprouts">Brussels Sprouts</label>
<select id="sprouts">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="cabbage">Cabbage</label>
<select id="cabbage">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="peas">Peas</label>
<select id="peas">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="beans">Green Beans, Broad Beans, Runner Beans</label>
<select id="beans">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="marrow">Marrow, Courgettes</label>
<select id="marrow">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="cauliflower">Cauliflower</label>
<select id="cauliflower">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="parsnips">Parsnips, Turnips, Swedes</label>
<select id="parsnips">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="leeks">Leeks</label>
<select id="leeks">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="onions">Onions</label>
<select id="onions">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="garlic">Garlic</label>
<select id="garlic">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="mushrooms">Mushrooms</label>
<select id="mushrooms">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sweet_peppers">Sweet Peppers</label>
<select id="sweet_peppers">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="beansprouts">Beansprouts</label>
<select id="beansprouts">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="salad">Green Salad, Lettuce, Cucumber, Celery</label>
<select id="salad">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="watercress">Watercress</label>
<select id="watercress">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="tomatoes">Tomatoes</label>
<select id="tomatoes">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="sweetcorn">Sweetcorn</label>
<select id="sweetcorn">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="beetroot">Beetroot</label>
<select id="beetroot">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="coleslaw">Coleslaw</label>
<select id="coleslaw">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="avocado">Avocado</label>
<select id="avocado">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="baked_beans">Baked Beans</label>
<select id="baked_beans">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="dried_lentils">Dried Lentils, Beans, Peas</label>
<select id="dried_lentils">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>

<label for="tofu_soya">Tofu, Soya Meat, TVP, Vegeburger</label>
<select id="tofu_soya">
    <option value="no_choice"></option>
    <option value="0">Never/less than once a month</option>
    <option value="1">1-3 per month</option>
    <option value="2">Once a week</option>
    <option value="3">2-4 per week</option>
    <option value="4">5-6 per week</option>
    <option value="5">Once a day</option>
    <option value="6">2-3 per day</option>
    <option value="7">4-5 per day</option>
    <option value="8">6+ per day</option>
</select>
</fieldset>
<div class="ffq_confirm" data-icon="check" id="ffq11_confirm" data-page-id="ffq_page11" data-role="button" data-theme="b">Confirm</div>
<div class="ffq_cancel" data-icon="delete" data-role="button" data-page-id="ffq_page11" data-theme="a">Cancel</div>
</form>
</div>
</div>
<!-- Other foods -->
<div data-role="page" id="ffq_page12" data-add-back-btn="true">

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Other Foods</h1>
        <form>
            <label for="other_foods_yes_no">Are there any other foods which you ate more than once a week?</label>
            <select id="other_foods_yes_no">
                <option value="no_choice"></option>
                <option value="yes">Yes</option>
                <option value="no">No</option>
            </select>
            <p>If yes, please add them in the fields below otherwise leave them blank.</p>
            <p><strong>When all fields are complete please press the "confirm" button.</strong></p>
            <div>
                <ul id="other_foods_list" data-role="listview" data-inset="true" data-split-icon="red-cross" data-split-theme="a">
                    <li data-role="list-divider">Other foods</li>
                    <li id="other_foods_list_empty">You currently have no new foods added.</li>
                </ul>
            </div>
            <div id="other_foods_validation_message" class="validation_error"></div>
            <fieldset data-role="fieldcontain">
                <label for="other_foods_food_name">Food name: </label>
                <input type="text" placeholder="Food Name" id="other_foods_food_name" disabled="true"/>
                <label for="other_foods_serving_size">Usual serving size: </label>
                <input type="text" placeholder="Serving Size" id="other_foods_serving_size" disabled="true" />
                <label for="other_foods_frequency">Number of times eaten each week</label>
                <select id="other_foods_frequency" disabled="true">
                    <option value="no_choice"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8+">8 or more</option>
                </select>
                <input  type="button" data-icon="plus" id="other_foods_add_btn" data-theme="e" value="Add Food" disabled="true"/>
            </fieldset>

            <div data-role="button" data-icon="check" id="other_foods_submit" data-page-id="ffq_page12" data-theme="b">Confirm</div>
            <div data-role="button" data-icon="delete" id="other_foods_cancel" data-page-id="ffq_page12" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="confirm_remove_food_dialog">
    <div data-role="header"><h1>Are you sure?</h1></div>

    <div data-role="content">
        <h1>Remove Item?</h1>
        <p>Are you sure you wish to remove this food item from the list?</p>
        <div class="centre">
            <a id="other_foods_confirm_delete" data-role="button" data-theme="b" data-icon="check" data-inline="true" data-mini="true">Yes</a>
            <a id="other_foods_cancel_delete" data-role="button" data-theme="a" data-icon="delete" data-inline="true" data-mini="true">No</a>
        </div>
    </div>
</div>

<div data-role="page" id="ffq_page13" data-add-back-btn="true">

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Milk & Breakfast</h1>
        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>
        <form>
            <fieldset data-role="fieldcontain">
                <label for="which_milk">What type of milk did you most often use?</label>
                <select id="which_milk">
                    <option value="no_choice"></option>
                    <option value="full_fat">Full fat</option>
                    <option value="semi_skimmed">Semi-skimmed</option>
                    <option value="skimmed">Skimmed</option>
                    <option value="channel_islands">Channel Islands</option>
                    <option value="dried">Dried Milk</option>
                    <option value="soya">Soya</option>
                    <option value="none">None</option>
                    <option value="other">Other (please specify)</option>
                </select>
                <label for="milk_specify_other">Other: </label>
                <input type="text" placeholder="Please Specify" id="milk_specify_other"/>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <label for="how_much_milk">How much milk did you drink each day, including milk with tea, coffee, cereals etc?</label>
                <select id="how_much_milk">
                    <option value="no_choice"></option>
                    <option value="none">None</option>
                    <option value="quarter_pint">Quarter of a pint</option>
                    <option value="half_pint">Half a pint</option>
                    <option value="3_quarter_pint">Three quarters of a pint</option>
                    <option value="pint">One pint</option>
                    <option value="more_than_pint">More than one pint</option>
                </select>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <label for="cereal_yes_no">Did you usually eat breakfast cereal (excluding porridge and Ready Brek mentioned earlier?</label>
                <select id="cereal_yes_no">
                    <option value="no_choice"></option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <p>If yes, which brand and type of breakfast cereal, including muesli, did you usually eat? If no just leave the rest blank.</p>
                <p><strong>Please list 1 or 2:</strong> </p>
                <label for="cereal_brand_1">Brand 1 (eg. Kellogg's): </label>
                <input type="text" placeholder="Brand 1" id="cereal_brand_1" />
                <label for="cereal_type_1">Type 1 (eg. cornflakes): </label>
                <input type="text" placeholder="Type 1" id="cereal_type_1" />
                <label for="cereal_brand_2">Brand 2: </label>
                <input type="text" placeholder="Brand 2" id="cereal_brand_2" />
                <label for="cereal_type_2">Type 2: </label>
                <input type="text" placeholder="Type 2" id="cereal_type_2" />
            </fieldset>
            <div data-role="button" id="cereal_breakfast_submit" data-page-id="ffq_page13" data-icon="check" data-theme="b">Confirm</div>
            <div data-role="button" id="cereal_breakfast_cancel" data-page-id="ffq_page13" data-icon="delete" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="ffq_page14" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Food Preparation</h1>
        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>
        <form>
            <fieldset data-role="fieldcontain">
                <label for="which_fat_frying">Which type of fat did you most often use for, frying, roasting, grilling, etc?</label>
                <select id="which_fat_frying">
                    <option value="no_choice"></option>
                    <option value="butter">Butter</option>
                    <option value="lard">Lard/Dripping</option>
                    <option value="vegetable_oil">Vegetable Oil</option>
                    <option value="solid_vegetable_fat">Solid Vegetable Fat</option>
                    <option value="margarine">Margarine</option>
                    <option value="none">None</option>
                </select>
                <label for="which_veg_oil">If you used vegetable oil, please specify which type: </label>
                <input type="text" placeholder="Specify Vegetable Oil" id="which_veg_oil" />

                <label for="which_fat_baking">Which type of fat did you most often use for baking cakes etc?</label>
                <select id="which_fat_baking">
                    <option value="no_choice"></option>
                    <option value="butter">Butter</option>
                    <option value="lard">Lard/Dripping</option>
                    <option value="vegetable_oil">Vegetable Oil</option>
                    <option value="solid_vegetable_fat">Solid Vegetable Fat</option>
                    <option value="margarine">Margarine</option>
                    <option value="none">None</option>
                </select>
                <label for="which_margarine_type">If you used margarine, please specify which type (eg. Flora, Stork): </label>
                <input type="text" placeholder="Specify Margarine" id="which_margarine_type" />
            </fieldset>
            <fieldset data-role="fieldcontain">
                <label for="how_often_fried_home">How often did you eat food that was fried at home?</label>
                <select id="how_often_fried_home">
                    <option value="no_choice"></option>
                    <option value="daily">Daily</option>
                    <option value="4-6_a_week">4-6 times a week</option>
                    <option value="1-3_a_week">1-3 times a week</option>
                    <option value="less_than_once_a_week">Less than once a week</option>
                    <option value="never">Never</option>
                </select>
                <label for="how_often_fried_away">How often did you eat food that was fried away from home?</label>
                <select id="how_often_fried_away">
                    <option value="no_choice"></option>
                    <option value="daily">Daily</option>
                    <option value="4-6_a_week">4-6 times a week</option>
                    <option value="1-3_a_week">1-3 times a week</option>
                    <option value="less_than_once_a_week">Less than once a week</option>
                    <option value="never">Never</option>
                </select>
                <label for="visible_fat">What did you do with the visible fat on your meat?</label>
                <select id="visible_fat">
                    <option value="no_choice"></option>
                    <option value="ate_most">Ate most of the fat</option>
                    <option value="ate_some">Ate some of the fat</option>
                    <option value="ate_little">Ate as little as possible</option>
                    <option value="no_meat">Did not eat meat</option>
                </select>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <label for="how_often_grilled_roast_meat">How many times did you eat grilled or roast meat each week?</label>
                <select id="how_often_grilled_roast_meat">
                    <option value="no_choice"></option>
                    <option value="0">None</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8+">8+</option>
                </select>
                <label for="grilled_roast_meat_how_well_cooked">How well cooked did you usually have grilled or roast meat?</label>
                <select id="grilled_roast_meat_how_well_cooked">
                    <option value="no_choice"></option>
                    <option value="well_done">Well done</option>
                    <option value="medium">Medium</option>
                    <option value="rare">Lightly cooked/rare</option>
                    <option value="no_meat">Did not eat meat</option>
                </select>
            </fieldset>
            <fieldset data-role="fieldcontain">
                <label for="how_often_add_salt_cooking">How often did you add salt to food while cooking?</label>
                <select id="how_often_add_salt_cooking">
                    <option value="no_choice"></option>
                    <option value="always">Always</option>
                    <option value="usually">Usually</option>
                    <option value="sometimes">Sometimes</option>
                    <option value="rarely">Rarely</option>
                    <option value="never">Never</option>
                </select>
                <label for="how_often_add_salt_table">How often did you add salt to food at the table?</label>
                <select id="how_often_add_salt_table">
                    <option value="no_choice"></option>
                    <option value="always">Always</option>
                    <option value="usually">Usually</option>
                    <option value="sometimes">Sometimes</option>
                    <option value="rarely">Rarely</option>
                    <option value="never">Never</option>
                </select>
                <label for="salt_sub_yes_no">Did you regularly use a salt substitute (eg. LoSalt)?</label>
                <select id="salt_sub_yes_no">
                    <option value="no_choice"></option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <label for="which_salt_sub">If <strong>yes</strong> which brand?</label>
                <input type="text" placeholder="Salt Substitute Brand" id="which_salt_sub" />
            </fieldset>
            <div data-role="button" data-icon="check" data-theme="b" data-page-id="ffq_page14" id="food_preparation_confirm">Confirm</div>
            <div data-role="button" data-icon="delete" data-theme="a" data-page-id="ffq_page14" id="food_preparation_cancel">Cancel</div>
        </form>
    </div>
</div>
<!-- Supplements -->
<div data-role="page" id="ffq_page15" data-add-back-btn="true">
    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout ui-btn-right" data-icon="gear">Log Out</a>
    </div>

    <div data-role="content">
        <h1 class="centre">Vitamins & Supplements</h1>
        <p>All fields are required unless stated otherwise.</p>

        <p><strong>When all fields are complete please press the "confirm" button</strong></p>
        <div id="supplements_validation_message"></div>

        <form>
            <fieldset data-role="fieldcontain">
                <label for="vitamins_yes_no">Have you taken any vitamins, minerals, fish oils, fibre or other food supplements during the past year?</label>
                <select id="vitamins_yes_no">
                    <option value="no_choice"></option>
                    <option value="yes">Yes</option>
                    <option value="no">No</option>
                </select>
                <p>If <strong>yes</strong> please input the details of each below, otherwise leave it blank and press confirm.</p>
                <div>
                    <ul id="supplements_list" data-role="listview" data-inset="true" data-split-icon="red-cross" data-split-theme="a">
                        <li data-role="list-divider">Supplements</li>
                        <li id="supplements_list_empty">You currently have no supplements listed.</li>
                    </ul>
                </div>
                <label for="supplements_name">Name: </label>
                <input type="text" placeholder="Name" id="supplements_name" disabled="true" />
                <label for="supplements_brand">Brand: </label>
                <input type="text" placeholder="Brand" id="supplements_brand" disabled="true" />
                <label for="supplements_strength">Strength: </label>
                <input type="text" placeholder="Strength" id="supplements_strength" disabled="true" />
                <label for="supplements_dose">Dose (number of pills, capsules or teaspoons): </label>
                <select id="supplements_dose" disabled="true">
                    <option value="no_choice"></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6+">6+</option>
                </select>
                <label for="supplements_frequency">Average frequency: </label>
                <select id="supplements_frequency" disabled="true">
                    <option value="no_choice"></option>
                    <option value="less_once_a_month">Less than once a month</option>
                    <option value="1-3_per_month">1-3 per month</option>
                    <option value="once_a_week">Once a week</option>
                    <option value="2-4_per_week">2-4 per week</option>
                    <option value="5-6_per_week">5-6 per week</option>
                    <option value="once_a_day">Once a day</option>
                    <option value="2-3_per_day">2-3 per day</option>
                    <option value="4-5_per_day">4-5 per day</option>
                    <option value="6+_per_day">6+ per day</option>
                </select>
                <input type="button" id="add_supplement_btn" data-icon="plus" data-theme="e" value="Add supplement" disabled="true"/>
            </fieldset>
            <div data-role="button" id="supplements_confirm_btn" data-page-id="ffq_page15" data-icon="check" data-theme="b">Confirm</div>
            <div data-role="button" id="supplements_cancel_btn" data-page-id="ffq_page15" data-icon="delete" data-theme="a">Cancel</div>
        </form>
    </div>
</div>

<div data-role="page" id="confirm_remove_supplement_dialog">
    <div data-role="header"><h1>Are you sure?</h1></div>

    <div data-role="content">
        <h1>Remove Item?</h1>
        <p>Are you sure you wish to remove this food item from the list?</p>
        <div class="centre">
            <a id="supplements_confirm_delete" data-role="button" data-theme="b" data-icon="check" data-inline="true" data-mini="true">Yes</a>
            <a id="supplements_cancel_delete" data-role="button" data-theme="a" data-icon="delete" data-inline="true" data-mini="true">No</a>
        </div>
    </div>
</div>

<!-- UPLOAD PAGE -->
<div data-role="page" id="upload_pic_page">
    <!-- MENU PANEL -->
    <div data-role="panel" data-theme="a" id="panel_menu8">
        <h1>Menu</h1>
        <ul class="mobile_menu" data-role="listview" data-inset="true">
            <li><a href="#dashboard_page">Dashboard</a></li>
            <li><a href="#diet_page" class="diet_btn">Diet</a></li>
            <li><a href="#diary_page">Diary</a></li>
            <li data-theme="b"><a href="#upload_pic_page">Upload Picture</a></li>
            <li><a href="#header" data-rel="close">Close Menu</a></li>
        </ul>
    </div>

    <!-- HEADER -->
    <div data-role="header" class="header" data-id="header" data-position="fixed">
        <a href="#panel_menu8" data-icon="bars">Menu</a>
        <img class="mobile_logo" src="img/westminster-logo-white.png"/>
        <a class="logout" data-icon="gear">Log Out</a>
    </div>

    <!-- PAGE CONTENT -->
    <div data-role="content">
        <h1 class="centre">Upload Picture</h1>

        <form enctype="multipart/form-data">
            <label for="file">Filename:</label>
            <input type="file" name="file" id="file" accept="Image/*"><br>
        </form>
        <input id="upload_submit" type="submit" value="Submit" data-theme="b" data-icon="check" />
        <div id="confirmFoodPopUp" data-role="popup" data-overlay-theme="a">
            <div class="ui-corner-top ui-header ui-bar-a" data-theme="b" data-role="header" role="banner">
                <h1 class="ui-title" role="heading" aria-level="1">Confirm food</h1>
            </div>
            <span id="picError" style=color:red></span>
            <div id="picContent"></div>
        </div> 
        <div id="failPicPopup" data-role="popup" data-overlay-theme="a">
            <div class="ui-corner-top ui-header ui-bar-a" data-theme="b" data-role="header" role="banner">
                <h1 class="ui-title" role="heading" aria-level="1">Error</h1>
            </div>
            <div>
                <p style=color:red>   Sorry we couldn't find your picture in our database, please take   <br/ >    another picture and make sure the file is less than 2mb   </p>
            </div>
        </div>
    </div>
</div>
<!-- END UPLOAD PAGE -->

<!-- JAVASCRIPT IMPORTS -->
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.core.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/datebox/latest/jqm-datebox.mode.calbox.min.js"></script>
<script type="text/javascript" src="http://dev.jtsage.com/cdn/simpledialog/latest/jquery.mobile.simpledialog.min.js"></script>
<script type="text/javascript" src="js/jquery.ui.map.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/highcharts.js"></script>
<script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript" src="js/mobile.js"></script>

</body>

</html>